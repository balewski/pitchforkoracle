#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;  #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

i=0
outPath=out_all # all

cellL="bbp001 bbp002 bbp003 bbp004 bbp005 bbp006 bbp007 bbp008 bbp009 bbp010 bbp011 bbp012 bbp013 bbp014 bbp017 bbp018 bbp019 bbp020 bbp023 bbp024 bbp025 bbp026 bbp027 bbp028 bbp031 bbp032 bbp034 bbp035 bbp036 bbp037 bbp038 bbp040 bbp041 bbp042 bbp043 bbp045 bbp046 bbp047 bbp048 bbp049 bbp050 bbp051 bbp052 bbp053 bbp054 bbp055 bbp056 bbp057 bbp060 bbp061 bbp062 bbp063 bbp066 bbp067 bbp068 bbp069 bbp070 bbp071 bbp072 bbp075 bbp076 bbp077 bbp078 bbp079 bbp080 bbp081 bbp082 bbp083 bbp084 bbp086 bbp087 bbp088 bbp089 bbp090 bbp091 bbp092 bbp093 bbp094 bbp095 bbp096 bbp097 bbp098 bbp099 bbp100 bbp101 bbp102 bbp106 bbp107 bbp108 bbp109 bbp111 bbp112 bbp113 bbp114 bbp115 bbp118 bbp119 bbp120 bbp121 bbp122 bbp124 bbp125 bbp126 bbp127 bbp128 bbp129 bbp132 bbp133 bbp134 bbp135 bbp136 bbp139 bbp140 bbp141 bbp142 bbp143 bbp144 bbp145 bbp146 bbp147 bbp148 bbp149 bbp150 bbp151 bbp152 bbp153 bbp154 bbp155 bbp156 bbp159 bbp160 bbp161 bbp162 bbp164 bbp165 bbp166 bbp167 bbp168 bbp171 bbp172 bbp173 bbp174 bbp175 bbp176 bbp179 bbp180 bbp181 bbp182 bbp185 bbp186 bbp187 bbp188 bbp189 bbp192 bbp193 bbp194 bbp195 bbp196 bbp197 bbp198 bbp199 bbp200 bbp201 bbp202 bbp203 bbp204 bbp205 bbp206 bbp207"

#outPath=out_cNAC #inhibit
#for name in  bbp002 bbp003 bbp006 bbp009 bbp011 bbp014 bbp019 bbp025 bbp027 bbp036 bbp042 bbp048 bbp052 bbp062 bbp068 bbp071 bbp079 bbp081 bbp088 bbp092 bbp096 bbp108 bbp112 bbp114 bbp122 bbp126 bbp134 bbp142 bbp147 bbp161 bbp165 bbp167 bbp175 bbp181 bbp189 bbp195 bbp200  ;  do 
 

#outPath=out_cAD # excite
#for name in  bbp102 bbp054 bbp098 bbp152 bbp153 bbp154 bbp155 bbp156 bbp176 bbp205 bbp206 bbp207  ;  do  

for name in  $cellL ;  do  
    thr=0.99
    i=$[ $i + 1 ]
    #echo
    #echo "********  $i $thr $name *******"
    #time ./rdFrames2Pca.py --pcaThr $thr  --outPath $outPath  --cellName $name
    #date
    #./pcaZipUnzip.py  --pcaThr $thr  --cellName $name -X  --pcaPath pca/$outPath   2>/dev/null  |grep '\#'
    #exit
    #break
    ./rdPcaInfo.py  --cellName $name 
done
echo done $i

# April23:
#for name in   bbp102 bbp153  bbp027 bbp121 bbp091 bbp129 bbp006   bbp019 bbp020  ;  do
