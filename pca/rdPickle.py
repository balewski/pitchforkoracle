#!/usr/bin/env python3
import sys
import pickle


#............................
def read_pickle(fname):
    print('read_pickle:',fname)
    fd=open(fname,'rb')
    blob=pickle.load(fd)
    fd.close()
    print('got %d keys '%(len(blob)),blob.keys())
    return blob


#=================================
#=================================
#  M A I N 
#=================================
#=================================

varFracThr=0.99
inpF='final_eignevalues_2.pickle'
blob=read_pickle(inpF)

cellName=['bbp153', 'bbp102', 'bbp018', 'bbp043'][2] #<-- change cell here
eigVal,eigVec=blob[cellName]
print(cellName,eigVal.shape,eigVec.shape)

npr=eigVal.shape[0]
se2=0
for i in range(npr):
    eig=eigVal[i]
    se2+=eig*eig

su2=0
for i in range(npr):
    eig=eigVal[i]
    su2+=eig*eig
    vf=su2/se2
    print('i=%2d eig=%.1f cumFrac=%.3f'%(i,eig,vf))
    if vf>varFracThr: break
print('cell %s K=%d for %.2f varFract'%(cellName,i,varFracThr))
