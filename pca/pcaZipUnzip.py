#!/usr/bin/env python3
""" 
reduce data size by applying PCA, precomputed
Optional: check inverse PCA transform and make few plots
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Util_IOfunc import write_yaml, read_yaml,write_data_hdf5,read_data_hdf5
from Plotter_CellSpike import Plotter_CellSpike
from pprint import pprint
import numpy as np
from numpy import linalg as LA

import argparse, os
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")
    parser.add_argument("-i","--inpPath",help="oryginal data",  default='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr')
    parser.add_argument("-o","--outPath",default='/global/cscratch1/sd/balewski/neuronBBP_pcaAna',help="output for plots and Hd5")
    parser.add_argument("-p","--pcaPath",default='/global/cfs/cdirs/m2043/balewski/neuronBBP-pca_analysis/04_23',help="precomputed PCA")
    parser.add_argument("-N", "--cellName", type=str, default='bbp102',help="cell shortName")
    parser.add_argument("-t", "--pcaThr", default=0.99, type=float,
                        help="PCA explained variance")

    parser.add_argument("-s","--shardId",help="particular HD5", type=int,  default=22)
    

    args = parser.parse_args()
    args.prjName='pcaFilter_%s_pca%.0f'%(args.cellName,args.pcaThr*100)
    args.formatVenue='prod'
    args.verb=1
    args.inpPath+='/'
    args.pcaPath+='/'
    args.outPath+='/'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#...!...!..................
def probes_by_name(probesL):
    #print(probesL)
    armL=['soma','dend','axon','apic']
    morphD={ x:{} for x in armL}
    for i,x in enumerate(probesL):
        xL=x.split('_')
        j = 0 if len(xL)==1 else int(xL[1])
        morphD[xL[0]][j]=(x,i)        
        #print(i,x)
    #pprint(morphD)
    morphIdxL=[]
    morphNameL=[]
    morphArmL=[]
    for arm in armL:
        armD=morphD[arm]
        kL=sorted(armD)
        #print(arm, kL)
        if len(kL)>0:
            morphArmL.append((arm,len(morphIdxL)))
        for k in kL:
            name,idx=armD[k]
            morphIdxL.append(idx)
            morphNameL.append(name)

    print('\n morphpologically sorted %d probes'%len(morphIdxL))
    i=0
    for idx,name  in zip( morphIdxL,morphNameL):
        #print(i,idx,name)
        i+=1
    #print('morphArmL',morphArmL)                   
    return morphIdxL,morphNameL,morphArmL
#............................
#............................
#............................
class Plotter_pcaReduce(Plotter_CellSpike):
    def __init__(self, args,metaD):
        Plotter_CellSpike.__init__(self,args,metaD)
        #pprint(self.metaD)

#...!...!..................
    def L2forKris(self,val2D,tit='',figId=100):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(14,5))
        ax=self.plt.subplot(1,1,1)

        # reorder probes by morphology
        val2D=val2D[:,self.metaD['morphIdx']]
        
        probeNameL=self.metaD['probeName']
        print('probeNameL=',probeNameL)
        numProb=val2D.shape[1]
        #binsX= np.linspace(-0.4, numProb-.4,numProb+1)
        binsX= np.linspace(0.5, numProb+.5,numProb+1)
        #print('bx',binsX)
        yLo=np.amin(val2D);   yHi=np.amax(val2D)     
        print('yLo/Hi=',yLo,yHi)
        yLo=0; yHi/=2.
        binsY= np.linspace(yLo,yHi,50)

        xx=[i+1 for i in range(numProb)]*int(val2D.shape[0])
        xData=np.array(xx)
        yData=val2D.reshape(-1)
        print('xd',xData.shape,val2D.shape, binsX.shape, binsY.shape)

        cmap=self.plt.cm.get_cmap('PuRd', 6)
        zsum,xbins,ybins,img =ax.hist2d(xData,yData,bins=(binsX,binsY), cmin=1,cmap=cmap)
        self.plt.colorbar(img, ax=ax,label='number of  frames')
        
        # add box-plot : labels=xlab,vert=False ,labels=binsX[:-1]
        ax.boxplot(val2D, showfliers=False,whis=(1,99)) # last is no-outliers
        
        ax.set(xlabel='probe idx', ylabel='avr L2 (mV/timeBin)',title=tit)
        
        #ax.grid()
        armL=self.metaD['morphArm']
        for name,idx in armL:
            ax.axvline(idx+0.6,c='b',linestyle='--',lw=0.7)
            ax.text(idx+0.6,yHi*0.8,name,rotation=80, c='b')

        # We want to show all ticks...
        ax.set_xticks(np.arange(numProb)+1)
        ax.set_xticklabels(self.metaD['morphName'],rotation=80, fontsize=8)    
  
        
        
#...!...!..................
    def VoltsforKris(self,F1,F2,frId,figId=100, stim=[], titL=['','']):
        tit1,tit2=titL
        tit1+=' frame=%d'%ifr
        nrow,ncol=4,2
        figId=self.smart_append(figId)
        # Remove horizontal space between axes
        fig,axA = self.plt.subplots(nrow, ncol, sharex=True, sharey=True,num=figId,facecolor='white', figsize=(16,8))
        #fig.subplots_adjust(,hspace=0,wspace=0) ??

        #print('eee',F1.shape)
        mxPr=F1.shape[2]
        kk=7 if mxPr>=64 else mxPr//8
        metaD=self.metaD
        npr=nrow*ncol
        prL=[ kk*i for i in range(npr)]
        #print('prL',prL)
        
        nBin=F1.shape[1]
        maxX=nBin
        xLab='time bins'; yLab='ampl (mV)'
        binsX=np.linspace(0,maxX,nBin)
        probeNameL=metaD['probeName']
        
        for ipr in range(npr):
            prId=prL[ipr]
            ax=axA[ipr%nrow,ipr//nrow]
            V1=F1[frId,:,prId]/metaD['voltsScale']
            V2=F2[frId,:,prId]/metaD['voltsScale']
            
            ax.plot(binsX,V1,label='pr%d:%s'%(prId,probeNameL[prId]),c='b')
            ax.plot(binsX,V2,label='invPC(PC)',linewidth=0.9,c='r')
            if len(stim)>0:
                ax.plot(stim*10, label='stim',color='black', linestyle='--',linewidth=0.5)
            ax.grid(linestyle=':')
            ax.legend(loc='upper right', title='probe  idx:name')
            if ipr//nrow==0: ax.set_ylabel(yLab)
            if ipr%nrow==3: ax.set_xlabel(xLab)
            if ipr==0: ax.set_title(tit1)
            if ipr==nrow: ax.set_title(tit2)

#...!...!..................
def apply_PCA_transform(F,pca):
    X=F.reshape(-1,F.shape[-1])
    print('X.shape=',X.shape,X.dtype)
    print('\n........... step-1 Normalize data')
    Xnor=(X-pca['z_mean'])/pca['z_std']
    print('Xnor.shape=',Xnor.shape,Xnor.dtype)
    
    print('\n........... step-2 apply PC transformation')
    Xpc = np.dot(Xnor, pca['pc_componentsT'])
    print('Xpc.shape=',Xpc.shape,Xpc.dtype,num_pcs)
    return Xpc

#...!...!..................
def apply_invPCA_transform(Xpc,pca,num_tbin):
    print('\n........... step-5 apply invPC transformation')
    Xnor2 = np.dot(Xpc, pca['pc_componentsT'].T)
    print('Xnor2.shape=',Xnor2.shape,Xnor2.dtype)

    print('\n........... step-6 De-Normalize data')
    X2=Xnor2*pca['z_std'] + pca['z_mean']
    print('X2.shape=',X2.shape,X2.dtype)

    num_prob=pca['num_probes'][0]
    F2=X2.reshape(-1,num_tbin,num_prob)
    print('F2.shape=',F2.shape,F2.dtype)
    return F2

def ampl_f32_to_i16(Xpc,factV):
    pca_int16_clip=32000
    print('\n........... step-3  convert PCA ampl to int16, fact=',factV)
    Xpc*=factV    
    # count over/undeflow
    nUp=np.amax( Xpc,axis=(0,1))
    nDw=np.amin( Xpc,axis=(0,1))
    print('extreme: Up,Dw=',nUp,nDw,Xpc.shape)
    Xclip=np.clip(Xpc,-pca_int16_clip,pca_int16_clip)
    return Xclip.astype('int16')

def ampl_i16_to_f32(Xpc16,factV):
    print('\n........... step-4  convert PCA int16 to F32 fact len=',len(factV))
    Xpc=Xpc16/factV    
    return Xpc


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

pcaF=args.pcaPath+'pca.varThr%2.0f_%s.h5'%(args.pcaThr*100,args.cellName)
pcaD=read_data_hdf5(pcaF)
num_pcs=pcaD['num_pcs'][0]
num_probes=pcaD['num_probes'][0]
pcr=pcaD['pc_val_range']
pca_int16_fact=1500/np.sqrt(pcaD['pc_std'])
pcm=max(abs(pcr))
print('# ',num_pcs,args.cellName,num_probes,' %.1f , %.1f'%(pcr[0],pcr[1]),' max=%.1f'%pcm)

metaF=args.inpPath+args.cellName+"/meta.cellSpike.yaml"
metaD=read_yaml(metaF)
#pprint(metaD)

stimF=args.inpPath+args.cellName+"/stim.%s.yaml"%metaD['rawInfo']['stimName']
stimD=read_yaml(stimF); print(stimD.keys())
#pprint(stimD)
stim=stimD['stimFunc']

inpF=args.inpPath+args.cellName+"/%s.cellSpike.data_%d.h5"%(args.cellName,args.shardId)
inpD=read_data_hdf5(inpF)
U=inpD['unit_par']

plot=Plotter_pcaReduce(args,metaD['rawInfo'])
morphIdxL,morphNameL,morphArmL=probes_by_name(metaD['rawInfo']['probeName'] )
plot.metaD['morphIdx']=morphIdxL
plot.metaD['morphName']=morphNameL
plot.metaD['morphArm']=morphArmL
numFr=5000

F=inpD['frames'][:numFr]
num_frame,num_tbin,num_prob=F.shape

Xpc=apply_PCA_transform(F,pcaD)
Xpc16=ampl_f32_to_i16(Xpc,pca_int16_fact)
Xpc=ampl_i16_to_f32(Xpc16,pca_int16_fact)
F2=apply_invPCA_transform(Xpc,pcaD,num_tbin)

# compute L2 for all probes
F=F.astype('float32')
F2=F2.astype('float32')
D2=(F-F2)
print('D2', D2.shape)

l2_avr=LA.norm(D2, axis=1)/num_tbin
#mse_std=np.std(D2, axis=1)/np.sqrt(numFr)/num_tbin
print('L2',l2_avr.shape,l2_avr[0])


tit1='cell=%s/%d  probes=%d  PCA(%.2f)  K-rank=%d  usedFrames=%d  %s'%(args.cellName,args.shardId,num_probes,args.pcaThr,num_pcs,D2.shape[0],metaD['rawInfo']['bbpName'])
plot.L2forKris(l2_avr,tit=tit1)
plot.display_all(); exit(0)

# -----------------------------
#  just plotting

# - - - -  plot volts_vs_time before and after PCA
numFr=5
plot=Plotter_pcaReduce(args,metaD['rawInfo'])
print('voltScale:',plot.metaD['voltsScale'])

tit1='cell=%s/%d  probes=%d  PCA(%.2f)  K-rank=%d '%(args.cellName,args.shardId,num_probes,args.pcaThr,num_pcs)
tit2=metaD['rawInfo']['bbpName']
for ifr in range(numFr):
    plot.VoltsforKris(F,F2,ifr,figId=ifr,stim=stim,titL=[tit1,tit2])
plot.display_all()

exit(0)

#step2b: verify  we still have N(0,1)
xx=np.mean(Xpc,axis=0)
print('Mean:',xx.shape,xx)
xx=np.std(Xpc,axis=0)
print('Std:',xx.shape,xx)
sumVar=LA.norm(xx)**2
print('sumVar=%.2f expl_var=%.2f  num_pc=%d  num_prob=%d'%(sumVar,pcaD['expl_rel_var_sum'], num_pcs,num_prob ))

numP=9 # for plotting
fr=F[:10,:,:numP]
plot.frames_vsTime(fr,U,1,stim=stim)
fr2=F2[:10,:,:numP]
plot.frames_vsTime(fr2,U,1,stim=stim)

Fnor=Xnor.reshape(num_frame,num_tbin,num_prob)
frnor=Fnor[:10,:,:numP]
plot.metaD['voltsScale']=1.
plot.frames_vsTime(frnor,U,1)

Fnor2=Xnor2.reshape(num_frame,num_tbin,num_prob)
frnor2=Fnor2[:10,:,:numP]
plot.frames_vsTime(frnor2,U,1)

Fpc=Xpc.reshape(num_frame,num_tbin,-1)
frpc=Fpc[:10,:,:numP]
plot.metaD['voltsScale']=1.
plot.metaD['probeName']=[ 'PC_%d'%i for i in range(num_pcs)]
plot.frames_vsTime(frpc,U,1)

plot.display_all('pcaRed',pdf=2)

