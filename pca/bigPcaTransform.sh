#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;  #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value


function init_cell( ) {
    mkdir -p $path
    cp ${inpPath}/${cell}/stim.chaotic_2.yaml $path
}

inpPath=/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr
outPath=/global/cfs/cdirs/m2043/balewski/neuronBBP-packTest7
echo path=$outPath
cellL1="bbp205 bbp054 bbp189 bbp019 bbp006 bbp155"
cellL2="bbp153 bbp102 bbp019 bbp027 bbp006"
cellL="bbp054 bbp098 bbp152 bbp154 bbp155 bbp156 bbp176 bbp205 bbp206 bbp207" # bbp102 bbp153 

mkdir -p ${outPath}/scripts
for pt in pca99 quad octal orig ; do
    echo work on probeType=$pt
    path0=${outPath}/probe_$pt
    #echo $pt $path0
    for cell in $cellL ; do
	#echo cell=$cell
	path=${path0}/${cell}
	echo $path
	#init_cell ; continue  #only  when new dir-tree is created
	./pcaTransformMeta.py --cellName ${cell} --outPath $outPath; chmod a+x ${outPath}/scripts/repack_${cell}.sh 
       
	#exit
	#break
    done
    # end of probeType
    #break
done

