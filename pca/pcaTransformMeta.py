#!/usr/bin/env python3
""" 
master, dispatches pcaTransforOne tasks based on input metaData and produces all output meta data 

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Util_IOfunc import write_yaml, read_yaml,write_data_hdf5,read_data_hdf5
from pprint import pprint
import numpy as np

import random

import argparse, os
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i","--inpPath",help="oryginal data",  default='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr')
    parser.add_argument("-o","--outPath",default='/global/cfs/cdirs/m2043/balewski/neuronBBP-packTest5/',help="output for plots and Hd5")
    parser.add_argument("-p","--pcaPath",default='/global/cfs/cdirs/m2043/balewski/neuronBBP-pca_analysis/04_23',help="precomputed PCA")
    parser.add_argument("-N", "--cellName", type=str, default='bbp006',help="cell shortName")
    parser.add_argument("-t", "--pcaThr", default=0.99, type=float,
                        help="PCA explained variance")

    args = parser.parse_args()
    args.verb=1
    args.inpPath+='/'
    args.pcaPath+='/'
    args.outPath+='/'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

#..... open command file
cmdF=args.outPath+"/scripts/repack_%s.sh"%args.cellName
cmdFd = open(cmdF, 'w')
cmdFd.write("#!/bin/bash\n")
cmdFd.write("set -u ;  # exit  if you try to use an uninitialized variable\n")
cmdFd.write("set -e ;  #  bash exits if any statement returns a non-true return value\n")
cmdFd.write("set -o errexit ;  # exit if any statement returns a non-true return value\n")

pcaF='pca.varThr%2.0f_%s.h5'%(args.pcaThr*100,args.cellName)
pcaD=read_data_hdf5(args.pcaPath+pcaF)
num_pcs=int(pcaD['num_pcs'][0])

npack=2
print('# ',num_pcs,args.cellName,npack)

metaF=args.inpPath+args.cellName+"/meta.cellSpike.yaml"
metaD=read_yaml(metaF)
#pprint(metaD)

print('metaD primary keys',list(metaD.keys()))

metaRaw=metaD['rawInfo']
metaInp=metaD['dataInfo']
outMeta={'rawInfo':metaRaw, 'dataInfo':metaInp}

#...... cleanup Inp
for x in ['seenBadFrames','totalGoodFrames','usedRawInpFiles']:
    metaInp.pop(x)
print('inp keys:',list(metaInp.keys()))

#....... cleanup raw
for x in ['digiNoise', 'maxOutFiles', 'rawPath0', 'simuJobId', 'useQA','shuffle' ]:
    metaRaw.pop(x)
print('raw keys:',list(metaRaw.keys()))

#... output oryginal meta (w/ changed fields)metaInp['featureType']='probes'
numProbe=metaRaw['num_probes']
assert numProbe==len(metaRaw['probeName'])
metaInp['featureType']='probes_orig'
metaInp['numFeature']=numProbe
metaInp['featureName']=metaRaw['probeName']
metaInp['feature_mean']=[ float("%.4e"%pcaD['z_mean'][i]) for i in range(numProbe)]
metaInp['feature_std']=[ float("%.4e"%pcaD['z_std'][i]) for i in range(numProbe)]
metaF=args.outPath+"probe_orig/%s/meta.cellSpike_orig.yaml"%args.cellName
write_yaml(outMeta,metaF)


# ..... NEW META files ......
h5nameTemplate=metaInp.pop('h5nameTemplate')
metaRaw['numDataFiles']=metaInp.pop('numDataFiles')
splitIdx=metaInp.pop('splitIdx')

# ........  repack splitIdx
inpIdxL=[]
for dom in splitIdx:
    inpIdxL+=splitIdx[dom]
print('see inpIdxL:',len(inpIdxL))

m=len(inpIdxL)//npack
assert m>0
for i in range(m):
    i2=i+1
    idxL=inpIdxL[i*npack:i2*npack]
    #print(i,idxL)
    cmdFd.write('time ./pcaTransformOne.py --cellName %s --outShardId %d --inpShardId '%(args.cellName,i))
    [ cmdFd.write('%d '%i) for i in idxL]
    cmdFd.write(' --outPath %s \n'%(args.outPath))
cmdFd.write('\n date \n echo done with all shards\n')

# generate new idxL
allL=[i for i in range(m) ]
random.shuffle(allL) # works in place
frac=0.10
nFrac=max(1,int(m*frac))
if 2*nFrac+1>m:
    print('\n\nWARN,  sometning went wrong, only %d output hd files was created. It is not enough to assigne 1 for test & validation. All is saved but training will most likely crash. Fix it\n\n'%m)
    exit(111)

splitIdxD={'test': allL[:nFrac]}
splitIdxD['val']=allL[nFrac:2*nFrac]
splitIdxD['train']=allL[2*nFrac:]

#.... common for skim* & pca
metaInp['numDataFiles']=m
metaInp['splitIdx']=splitIdxD
metaInp['numFramesPerFile']=npack*6144
metaInp['totalFrameCount']=m*metaInp['numFramesPerFile']

skimL=[ ('quad',11), ('octal',19) ]
# ..... skim output
for skimStr,num_skim in skimL:
    assert len(metaRaw['probeName']) >=num_skim
    metaInp['h5nameTemplate']=h5nameTemplate.replace('cellSpike','cellSpike_'+skimStr)
    metaInp['featureType']='probes_'+skimStr
    metaInp['numFeature']=num_skim
    metaInp['featureName']=metaRaw['probeName'][:num_skim]
    metaInp['feature_mean']=[ float("%.4e"%pcaD['z_mean'][i]) for i in range(num_skim)]
    metaInp['feature_std']=[ float("%.4e"%pcaD['z_std'][i]) for i in range(num_skim)]
    #print(metaInp['feature_std'],len(metaInp['feature_std']))
    #pprint(metaInp)
    metaF=args.outPath+"probe_%s/%s/meta.cellSpike_%s.yaml"%(skimStr,args.cellName,skimStr)
    write_yaml(outMeta,metaF)


# ..... PCA output
rel_var=pcaD['expl_rel_var']
pcaStr='pca%.0f'%(100*args.pcaThr)
metaInp['h5nameTemplate']=h5nameTemplate.replace('cellSpike','cellSpike_'+pcaStr)
metaInp['featureType']='pca_%.2f'%args.pcaThr
metaInp['numFeature']=num_pcs
metaInp['featureName']=[ 'pc%d_%.3f'%(i,rel_var[i]) for i in range(num_pcs)]
metaInp['feature_mean']=[0]*num_pcs
metaInp['feature_std']=[ float("%.4e"%pcaD['pc_std'][i]) for i in range(num_pcs)]
metaInp['pcaAnaName']=pcaF
# heuristic used to convert float32 to int16, first attentuate each PC/sqrt(std)
factV=1500/np.sqrt(pcaD['pc_std'])  # this constant was chosen to fit int16 range
metaInp['pca_ampl_fact']=[ float("%.4e"%factV[i]) for i in range(num_pcs)]
#print(metaInp['feature_std'],len(metaInp['feature_std']))
#print(metaInp['featureName'])
print('PCA: sum expl var',sum(pcaD['expl_rel_var'][:num_pcs]))
metaF=args.outPath+"probe_%s/%s/meta.cellSpike_%s.yaml"%(pcaStr,args.cellName,pcaStr)
write_yaml(outMeta,metaF)

