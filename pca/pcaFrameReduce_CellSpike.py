#!/usr/bin/env python3
""" 
reduce data size by applying PCA, precomputed
Optional: check inverse PCA transform and make few plots
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Util_Func import write_yaml, read_yaml,write_data_hdf5,read_data_hdf5
from Plotter_CellSpike import Plotter_CellSpike
from pprint import pprint
import numpy as np
from numpy import linalg as LA

import argparse, os
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")
    parser.add_argument("-i","--inpPath",help="oryginal data",  default='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr')
    parser.add_argument("-o","--outPath",default='/global/cscratch1/sd/balewski/neuronBBP_pca1',help="output for plots and Hd5")
    parser.add_argument("-p","--pcaPath",default='pca/dataApr19',help="precomputed PCA")
    parser.add_argument("-N", "--cellName", type=str, default='bbp006',help="cell shortName")
    parser.add_argument("-s","--shardId",help="particular HD5", type=int,  default=33)
    
    parser.add_argument("-n", "--events", type=int, default=30, help="num frames to process")
    parser.add_argument("--checkInverse", type=int, default=1, help="flag, makes plots")

    args = parser.parse_args()
    args.prjName='pcaReduce'
    args.formatVenue='prod'
    args.verb=1
    args.inpPath+='/'
    args.pcaPath+='/'
    args.outPath+='/'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
#............................
#............................
class Plotter_pcaReduce(Plotter_CellSpike):
    def __init__(self, args,metaD):
        Plotter_CellSpike.__init__(self,args,metaD)
        #pprint(self.metaD)

#...!...!..................
    def XXframes_vsTime(self,X,Y,nFr,figId=7,metaD=None, stim=[]):
        if metaD==None:  metaD=self.metaD


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

pcaF=args.pcaPath+'pcaInfo_%s.h5'%args.cellName
pcaD=read_data_hdf5(pcaF)
sc_s=pcaD['sc_s']
sc_m=pcaD['sc_m']
pca_rot=pcaD['pca_components'].T
pca_mean=pcaD['pca_mean']
noise_var,num_pc,expl_var=pcaD['pca_misc']
num_pc=int(num_pc)
#print('pca_mean:', pca_mean)


metaF=args.inpPath+args.cellName+"/meta.cellSpike.yaml"
metaD=read_yaml(metaF)
#pprint(metaD)

stimF=args.inpPath+args.cellName+"/stim.%s.yaml"%metaD['rawInfo']['stimName']
stimD=read_yaml(stimF); print(stimD.keys())
#pprint(stimD)
stim=stimD['stimFunc']

inpF=args.inpPath+args.cellName+"/%s.cellSpike.data_%d.h5"%(args.cellName,args.shardId)
inpD=read_data_hdf5(inpF)

plot=Plotter_pcaReduce(args,metaD['rawInfo'])
print('voltScale:',plot.metaD['voltsScale'])
F=inpD['frames'][:]
num_frame,num_tbin,num_prob=F.shape
X=F.reshape(-1,num_prob)
U=inpD['unit_par']
print('X.shape=',X.shape,X.dtype)

print('\n........... step-1 Normalize data')
Xnor=(X-sc_m) /sc_s 
print('pr0 m,s:',sc_m[0],sc_s[0])
print('Xnor.shape=',Xnor.shape,Xnor.dtype)
'''
#step1b: verify now we have N(0,1)
xx=np.mean(Xnor,axis=0)
print('Mean:',xx.shape,xx)
xx=np.std(Xnor,axis=0)
print('Std:',xx.shape,xx)
'''

print('\n........... step-2 apply PC transformation')
Xpc = np.dot(Xnor, pca_rot)
print('Xpc.shape=',Xpc.shape,Xpc.dtype,num_pc)

#step2b: verify  we still have N(0,1)
xx=np.mean(Xpc,axis=0)
print('Mean:',xx.shape,xx)
xx=np.std(Xpc,axis=0)
print('Std:',xx.shape,xx)
sumVar=LA.norm(xx)**2
print('sumVar=%.2f expl_var=%.2f  num_pc=%d  num_prob=%d'%(sumVar,expl_var, num_pc, pca_rot.shape[0] ))

numP=9 # for plotting
fr=F[:10,:,:numP]
plot.frames_vsTime(fr,U,1,stim=stim)

Fnor=Xnor.reshape(num_frame,num_tbin,num_prob)
frnor=Fnor[:10,:,:numP]
plot.metaD['voltsScale']=1.
plot.frames_vsTime(frnor,U,1)#,stim=stim)

Fpc=Xpc.reshape(num_frame,num_tbin,-1)
frpc=Fpc[:10,:,:numP]
plot.metaD['voltsScale']=1.
plot.metaD['probeName']=[ 'PC_%d'%i for i in range(num_pc)]

plot.frames_vsTime(frpc,U,1)#,stim=stim)

plot.display_all('pcaRed',pdf=2)

