#!/usr/bin/env python3
import sys,os

# modiffied Shivam analysis of PCA

sys.path.append(os.path.abspath("../"))

from Util_Func import write_yaml, read_yaml, write_data_hdf5, read_data_hdf5

import numpy as np
from sklearn.preprocessing import StandardScaler
#from sklearn.decomposition import IncrementalPCA as PCA
from sklearn.decomposition import  PCA
from numpy import linalg as LA
np.random.seed(5)

#=================================
#=================================
#  M A I N 
#=================================
#=================================

shortN='bbp153' # excit-complx
#shortN='bbp102' # excit-simple
#shortN='bbp027' # inhib-complx
#shortN='bbp019' # inhib-simple
#shortN='bbp006' # used for 2020-gold ML

dataPath='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr/'
shard=22
inpF=dataPath+'%s/%s.cellSpike.data_%d.h5'%(shortN,shortN,shard)
bulk=read_data_hdf5(inpF)

frames=bulk['frames'][::50]
print('inp data',frames.shape)

#flatten the data:
X=frames.reshape(-1,frames.shape[2])
num_samp=X.shape[0]
print('flattened data',X.shape,'total pts=%.3e, num_samp/probe=%2e'%(np.prod(X.shape),num_samp))


print("Standardize the Data: z = (x - u) / s")
scaler = StandardScaler() # copy=False should save 2x RAM but I see no impact during Fit
# Fit on training set only.
scaler.fit(X)
sc_s=scaler.scale_
sc_m=scaler.mean_
sc_v=scaler.var_
print('scaler scale:',sc_s.shape,sc_s[::40])
print('scaler mean:',sc_m.shape,sc_m[::40])
print('scaler variance:',sc_v.shape,sc_v[::40])
print('scaler n_samples_seen_:',scaler.n_samples_seen_)

print(' Apply scaler transform to all frames')
# do manual transformation
Xnor= (X-sc_m)/sc_s

''' # out of the box transformation
Xnor2 = scaler.transform(X)
print('Xnor:',Xnor[:5,:3])
print('Xnor2:',Xnor2[:5,:3])
D=Xnor-Xnor2
DN=LA.norm(D, axis=1)
print('DN:',DN[:5],DN.shape,sum(DN))
'''

print('\n Fit PCA')
pca = PCA(.99)
#pca = PCA(n_components=Xnor.shape[1])
pca.fit(Xnor)

b=pca.explained_variance_
print('explained_variance:',b.shape,b[:5],sum(b))

c=pca.explained_variance_ratio_
expl_var=sum(c)
print('explained_variance_ratio',c.shape,c[:5],expl_var)

d=pca.singular_values_
print('singular_values',d.shape,d[:5])
pc_std= pca.singular_values_/np.sqrt(num_samp)
print('pc_std^2/expl_var',pc_std**2/b)

# meanig:   sing_val/sqrt(N samples) = std(PC)
trans_mean=pca.mean_
print('mean',trans_mean.shape, trans_mean[:5])
print('n_components:',pca.n_components_)
print('noise_variance:',pca.noise_variance_)

# transform data using PC
Xpc = np.dot(Xnor-pca.mean_, pca.components_.T) 
#step2b: verify  we still have N(0,1)
xx=np.mean(Xpc,axis=0)
print('Mean:',xx.shape,xx)
xx=np.std(Xpc,axis=0)
print('Std:',xx.shape,xx)
print('Var:',xx**2)
sumVar=LA.norm(xx)**2
print('sumVar=%.2f expl_var=%.2f num_pc=%d  num_prob=%d'%(sumVar,expl_var,pca.n_components_, pca.mean_.shape[0] ))
print('sing_val/std/sqrt(N):',pc_std/xx)


# save PCA for Jan START
misc=[pca.noise_variance_, pca.n_components_, sum(pca.explained_variance_ratio_)]
outD={'sc_s':scaler.scale_,'sc_m':scaler.mean_,'sc_v':scaler.var_,'sc_inp_shape':np.array(X.shape),'raw_inp_shape':np.array(frames.shape)}
outD.update( {'pca_mean':pca.mean_,'pca_components':pca.components_,'pca_explained_variance':pca.explained_variance_,'pca_misc':np.array(misc)})
outF='out/pcaInfo_%s.h5'%shortN
write_data_hdf5(outD,outF)
# save PCA for Jan END

print(shortN ,'end: pca_misc:',outD['pca_misc'],' PCA rotArr:',outD['pca_components'].shape)


