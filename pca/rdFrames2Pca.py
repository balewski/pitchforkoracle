#!/usr/bin/env python3
import sys,os

# compact Z+PCA  for production
'''
'bbp006' # used for 2020-gold ML
'''

sys.path.append(os.path.abspath("../"))
from Util_IOfunc import write_yaml, read_yaml, write_data_hdf5, read_data_hdf5

import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import  PCA
import time
np.random.seed(5)

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--pcaThr", default=0.99, type=float,
                        help="PCA explained variance")
    parser.add_argument("-o","--outPath",
                        default='out/',help="output path for plots and tables")

    parser.add_argument("-N", "--cellName", type=str, default='bbp153',
                        help="cell shortName")

    args = parser.parse_args()
    args.dataPath='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr/'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
shortN=args.cellName
shard=7
inpF='%s/%s.cellSpike.data_%d.h5'%(shortN,shortN,shard)
pcaD={}

startT0 = time.time()
bulk=read_data_hdf5(args.dataPath+inpF)
pca = PCA(args.pcaThr)
frames=bulk['frames']#[::50]
print('inp data',frames.shape)

# make sure all objects are regular np-arrays
num_probes=frames.shape[2]
pcaD['num_probes']=np.array(num_probes).reshape(-1).astype('int32')
pcaD['num_frames']=np.array(frames.shape[0]).reshape(-1).astype('int32')


#flatten the data:
T0 = time.time()
X=frames.reshape(-1,num_probes)
print('reshape  time=%.4f(sec) shape'%(time.time()-T0),X.shape,X.dtype)

num_samp=X.shape[0]
pcaD['num_samples']=np.array(num_samp).reshape(-1).astype('int32')
print('flattened data',X.shape,'total pts=%.3e, num_samp/probe=%2e'%(np.prod(X.shape),num_samp))

print("Standardize the Data: z = (x - u) / s")
scaler = StandardScaler() # copy=False should save 2x RAM but I see no impact during Fit
# Fit on training set only.
scaler.fit(X)

pcaD['z_mean']=scaler.mean_
pcaD['z_std']=scaler.scale_

print(' Apply scaler transform to all frames')
# do manual transformation
Xnor= (X-scaler.mean_)/scaler.scale_

print('\n Fit PCA')
pca.fit(Xnor)
print('explained_variance',pca.explained_variance_)

# make sure all objects are regular np-arrays
pcaD['expl_rel_var']=pca.explained_variance_ratio_
pcaD['pc_std']=np.sqrt(pca.explained_variance_)
pcaD['num_pcs']=pca.n_components_.reshape(-1).astype('int32')
pcaD['noise_var']=pca.noise_variance_.reshape(-1)
pcaD['expl_rel_var_sum']=np.sum(pca.explained_variance_ratio_).reshape(-1)
pcaD['pc_componentsT']=pca.components_.T

# note : PC transformation uses compenetsT, the inversePC uses compenents
Xpc = np.dot(Xnor, pca.components_.T)
# note2, the mean of input to PCA is already 0 because z-transform was applied
xmin=np.amin(Xpc)
xmax=np.amax(Xpc)
print('Xpc range:',xmin,xmax)
pcaD['pc_val_range']=np.array([xmin,xmax])

print('pcaD:',pcaD.keys())
outF=args.outPath+'/pca.varThr%2.0f_%s.h5'%(args.pcaThr*100,shortN)
write_data_hdf5(pcaD,outF)
# save PCA for Jan END

print(shortN ,'end: expl_var=%.2f  num_PCs=%d'%(pcaD['expl_rel_var_sum'],pcaD['num_pcs']),'total time=%.1f min'%((time.time()-startT0)/60.))

T0 = time.time()
Xpc=Xpc.astype('float32')
print('to float32  time=%.4f(sec) shape'%(time.time()-T0),Xpc.shape,Xpc.dtype)


