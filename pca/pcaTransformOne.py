#!/usr/bin/env python3
""" 
reduce data size by applying  precomputed PCA
For mass proecessing use pcaTransformMeta.py to generate the repack_bbp006.sh script
which will call this python script with appropriate arguments

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Util_IOfunc import write_yaml, read_yaml,write_data_hdf5,read_data_hdf5
from pprint import pprint
import numpy as np
from numpy import linalg as LA

import argparse, os
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i","--inpPath",help="oryginal data",  default='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr')
    parser.add_argument("-o","--outPath",default='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_packTest2/',help="output for plots and Hd5")
    parser.add_argument("-p","--pcaPath",default='/global/cfs/cdirs/m2043/balewski/neuronBBP-pca_analysis/04_23',help="precomputed PCA")
    parser.add_argument("-N", "--cellName", type=str, default='bbp102',help="cell shortName")
    parser.add_argument("-t", "--pcaThr", default=0.99, type=float,
                        help="PCA explained variance")

    parser.add_argument("--outShardId",help=" target id", type=int,  default=22)
    parser.add_argument("--inpShardId",help=" sorce ids",  nargs='+', type=int,  default=[10,11])
    

    args = parser.parse_args()
    args.verb=1
    args.inpPath+='/'
    args.pcaPath+='/'
    args.outPath+='/'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    for sub in ["quad","octal","pca99"]:
        path1=args.outPath+"probe_%s/%s"%(sub,args.cellName)
        if not os.path.exists(path1):
            print('missing out dir:',path1)
            exit(55)
    return args


#...!...!..................
def apply_PCA_transform(F,pca):
    nFr=F.shape[0]
    X=F.reshape(-1,F.shape[-1])
    print('X.shape=',X.shape,X.dtype)
    print('\n........... step-1 Normalize data')
    Xnor=(X-pca['z_mean'])/pca['z_std']
    print('Xnor.shape=',Xnor.shape,Xnor.dtype)
    
    print('\n........... step-2 apply PC transformation')
    Xpc = np.dot(Xnor, pca['pc_componentsT'])

    #brake it again per-frame
    Xpc=Xpc.reshape(nFr,-1,Xpc.shape[-1])
    print('Xpc.shape=',Xpc.shape,Xpc.dtype,num_pcs)
    return Xpc

#...!...!..................
def apply_invPCA_transform(Xpc,pca,num_tbin):
    print('\n........... step-3 apply invPC transformation')
    Xnor2 = np.dot(Xpc, pca['pc_componentsT'].T)
    print('Xnor2.shape=',Xnor2.shape,Xnor2.dtype)

    print('\n........... step-4 De-Normalize data')
    X2=Xnor2*pca['z_std'] + pca['z_mean']
    print('X2.shape=',X2.shape,X2.dtype)

    num_prob=pca['num_probes'][0]
    F2=X2.reshape(-1,num_tbin,num_prob)
    print('F2.shape=',F2.shape,F2.dtype)
    return F2

#...!...!..................
def ampl_f32_to_i16(Xpc,factV):
    pca_int16_clip=32000
    print('\n........... step-3  convert PCA ampl to int16, factV=',factV)
    Xpc*=factV    
    # count over/undeflow
    nUp=np.sum( Xpc >pca_int16_clip)
    nDw=np.sum( Xpc <-pca_int16_clip)
    print('nUp,nDw=',nUp,nDw,Xpc.shape)
    assert nUp<Xpc.shape[0]  #too many some PCA amplitudes were clipped
    assert nDw<Xpc.shape[0]

    Xclip=np.clip(Xpc,-pca_int16_clip,pca_int16_clip)
    return Xclip.astype('int16')

    
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

pcaF=args.pcaPath+'pca.varThr%2.0f_%s.h5'%(args.pcaThr*100,args.cellName)
pcaD=read_data_hdf5(pcaF)
num_pcs=pcaD['num_pcs'][0]
num_probes=pcaD['num_probes'][0]
print('# ',num_pcs,args.cellName,num_probes)
pcaStr='pca%.0f'%(100*args.pcaThr)


metaF=args.outPath+"probe_%s/%s/meta.cellSpike_pca99.yaml"%(pcaStr,args.cellName)
metaD=read_yaml(metaF)
#pprint(metaD)

num_skim11=11 # hardcoded, quad
num_skim19=19 # hardcoded, octal
frame_per_inp=6144  # hardcoded

numFramesPerFile= metaD['dataInfo']['numFramesPerFile']
num_tbin=metaD['dataInfo']['numTimeBin']
num_par=metaD['dataInfo']['numPar']
pca_int16_fact=metaD['dataInfo']['pca_ampl_fact']

Fskim11=np.zeros((numFramesPerFile,num_tbin,num_skim11),dtype='int16')
Fskim19=np.zeros((numFramesPerFile,num_tbin,num_skim19),dtype='int16')
Fpca=np.zeros((numFramesPerFile,num_tbin,num_pcs),dtype='int16')
Uall=np.zeros((numFramesPerFile,num_par),dtype='float32')
Pall=np.zeros((numFramesPerFile,num_par),dtype='float32')
print(' created output storage:',Fpca.shape,Fskim11.shape,Fskim19.shape,Uall.shape)

iOff=0
for shardId in args.inpShardId:
    inpF=args.inpPath+args.cellName+"/%s.cellSpike.data_%d.h5"%(args.cellName,shardId)
    inpD=read_data_hdf5(inpF)
    assert frame_per_inp<= inpD['frames'].shape[0]
    assert num_skim19<=inpD['frames'].shape[2]

    Uall[iOff:iOff+frame_per_inp]=inpD['unit_par'][:frame_per_inp]
    Pall[iOff:iOff+frame_per_inp]=inpD['phys_par'][:frame_per_inp]
    # just copy skimmed data
    Fskim11[iOff:iOff+frame_per_inp]=inpD['frames'][:frame_per_inp,:,:num_skim11]
    Fskim19[iOff:iOff+frame_per_inp]=inpD['frames'][:frame_per_inp,:,:num_skim19]

    # Apply PCA transformation    
    F=inpD['frames'][:frame_per_inp]
    Xpc=apply_PCA_transform(F,pcaD)
    
    Xpc_i16=ampl_f32_to_i16(Xpc,pca_int16_fact)
    Fpca[iOff:iOff+frame_per_inp]=Xpc_i16
    iOff+=frame_per_inp

print('repacking done')

# save skim11 data (quad)
outD={'unit_par':Uall, 'phys_par':Pall, 'frames':Fskim11}
outF=args.outPath+"probe_quad/"+args.cellName+"/%s.cellSpike_quad.data_%d.h5"%(args.cellName,args.outShardId)
write_data_hdf5(outD,outF)

# save skim19 data (octal)
outD['frames']=Fskim19
outF=args.outPath+"probe_octal/"+args.cellName+"/%s.cellSpike_octal.data_%d.h5"%(args.cellName,args.outShardId)
write_data_hdf5(outD,outF)

# save PCA data
outD['frames']=Fpca

outF=args.outPath+"probe_%s/%s/%s.cellSpike_%s.data_%d.h5"%(pcaStr,args.cellName,args.cellName,pcaStr,args.outShardId)
write_data_hdf5(outD,outF)

print('done transform  %s  for outShard: %d\n\n'%(args.cellName,args.outShardId))


