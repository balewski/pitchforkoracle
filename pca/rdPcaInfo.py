#!/usr/bin/env python
""" 
 inspect PCA transformation info
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np
import matplotlib.pyplot as plt
from numpy import linalg as LA

import sys,os
sys.path.append(os.path.abspath("../toolbox"))
#from Util_IOfunc import write_yaml, read_yaml, write_data_hdf5, 
from Util_H5io3 import read3_data_hdf5

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("-i", "--inpPath",
                        default='out/',help="path to input/output")
    parser.add_argument("-o","--outPath",
                        default='out/',help="output path for plots and tables")

    parser.add_argument("-N", "--cellName", type=str, default='bbp153',
                        help="cell shortName")

    args = parser.parse_args()
    args.inpPath='/global/cfs/cdirs/m2043/balewski/neuronBBP-pca_analysis/May1/'
    
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args



#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
inpF=args.inpPath+'/pca.varThr99_%s.h5'%args.cellName
blob,_=read3_data_hdf5(inpF)

print('\n PCA summary for cell=',args.cellName)
for k in ['num_probes','num_frames','num_samples','num_pcs','expl_rel_var_sum','noise_var','pc_val_range']:
    print(k,blob[k])

# for CSV table: short_name,num_pca99,num_prob
print("#,%s,%d,%d"%(args.cellName,blob['num_pcs'],blob['num_probes'])) #; exit(0)

      
tot_var=blob['num_probes'] # since input was scaled to N(0,1)

cum_rvar=np.cumsum(blob['expl_rel_var'])
krank=blob['num_pcs'][0]
varThr=blob['expl_rel_var_sum']
print('K-rank=%d compare cum_expl_rvar: %.3f %.3f'%(krank,cum_rvar[-1], varThr))

# test norm of rotM rows
pcaRot=blob['pc_componentsT'].T
mR=LA.norm(pcaRot, axis=1)
print('L2 on rotM rows:',mR)
smR=sum(mR)
assert abs((smR - mR.shape[0]) <1.e-3)

#---------- just plotting
num_frames=blob['num_frames']

plt.figure(1,figsize=(6,4),facecolor='white')
ax = plt.subplot(1,1,1)
binsY=np.linspace(1,krank,krank)
ax.plot(cum_rvar,binsY,'*')
ax.grid()
ax.set_title('PCA:%s, numFr=%d, cumVar=%.3f, K-rank=%d'%(args.cellName,num_frames, varThr,krank))
ax.set_xlabel('cumulative realtive variance')
ax.set_ylabel('num PCs')
ax.set_xlim(0.9,1.)
ax.axvline(varThr,c='r')


# - - -  2D signifficance of probes
plt.figure(2,figsize=(12,4),facecolor='white')
ax = plt.subplot(1,1,1)
num_prob=blob['num_probes'][0]
colMap=plt.cm.bwr
img=ax.imshow(pcaRot, interpolation='nearest', cmap=colMap,origin='lower', vmin=-0.5,vmax=0.5)
plt.colorbar(img, ax=ax)
ax.set_title('cell=%s, deconstruction of PCs to probes, cumVarThr=%.3f'%(args.cellName,varThr))
ax.set_xlabel('measured probe index')
ax.set_ylabel('PC index')
ax.grid()

# - - -  1D signifficance of probes
plt.figure(3,figsize=(10,4),facecolor='white')
ax = plt.subplot(1,1,1)
m2C=LA.norm(pcaRot, axis=0)
mC=np.sqrt(m2C)
width = 0.65
binsX=np.linspace(0,num_prob-1,num_prob)
ax.bar(binsX,mC,width,fc='g')

ax.set_title('cell=%s, imporatnce of %d probes, cumVarThr=%.3f'%(args.cellName,num_prob,varThr))
ax.set_xlabel('measured probe index')
ax.set_ylabel('sqrt(L2(PC_pr))')
ax.grid()

plt.tight_layout()
plt.show()
