#!/usr/bin/env python3
import sys,os
'''
definition :https://en.wikipedia.org/wiki/Approximate_entropy
but the 'Nolds' implementation is much faster than the oryginal Pincus code.

git clone https://github.com/CSchoel/nolds.git
pip-installed on Cori with: module load tensorflow/gpu-2.1.0-py37

Aprox. entropy is computed by
  nolds.sampen(X, emb_dim=emb_len, tolerance=r_dist)

I assumed:  1mV as tolerance for matching  and emb_len of 3 (as suggested)
'''

import numpy as np
import time
import nolds

sys.path.append(os.path.abspath("/global/homes/b/balewski/pitchforkOracle/"))
from Util_IOfunc import write_yaml, read_yaml, write_data_hdf5, read_data_hdf5
from Plotter_Backbone import Plotter_Backbone
from matplotlib import cm as cmap

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")
    parser.add_argument('-ns',"--numShard", default=1, type=int, help="number of data shards to be used")
    parser.add_argument('-nf',"--numFrame", default=400, type=int, help="number of frames, or all is -1")
    parser.add_argument("--numSample", default=2, type=int, help="number of windows sampled from the same frame")
    parser.add_argument("--probeIndex", default=0, type=int, help="selects soma or other probe by index")
    parser.add_argument('-wl',"--windowLength", default=1000, type=int, help="number of consecutive time-bins from full time-line")
    
    parser.add_argument("-o","--outPath",
                        default='out/',help="output path for plots and tables")

    parser.add_argument("-N", "--cellName", type=str, default='bbp153',
                        help="cell shortName")

    args = parser.parse_args()
    args.dataPath='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr/'
    args.prjName='cellEntropy'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
#............................
#............................
class Plotter_Entropy(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)

#...!...!..................
    def entropy_list(self,aprEntV,tit,figId=101):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(10,5))
        ax = self.plt.subplot(1,1,1)
        nChunk=len(aprEntV)
        binsX=[i+1 for i in range(nChunk)]
        ax.plot(binsX,aprEntV)
        ax.set(title=tit,xlabel='frame chunk',ylabel='apr entropy')
        ax.grid()
        
#...!...!..................
    def entropy_hist1D(self,aprEntV,tit,figId=101):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(5,4))
        ax = self.plt.subplot(1,1,1)
        
        zmu=np.array(aprEntV)
        resM=float(zmu.mean())
        resS=float(zmu.std())
        txt2=' avr:%.1e rms:%.1e '%(resM,resS)
        xLo=max(0,resM-5*resS)
        binsX=np.linspace(xLo,resM+5*resS, 60)
        
        ax.hist(aprEntV,bins=binsX)
        ax.set(title=tit,ylabel='num samples',xlabel='aprox. entropy')
        ax.grid()

        ax.axvline(resM, color='k', linestyle='--')
        ax.axvline(resM-resS, color='k', linestyle=':')
        ax.axvline(resM+resS, color='k', linestyle=':')
        ax.text(0.6,0.65,txt2,transform=ax.transAxes)
        
#...!...!..................
    def entropy_hist2D(self,tOff,aprEntV,metaD,tit,figId=101):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(7,4))
        ax = self.plt.subplot(1,1,1)
        
        
        resM=metaD['ent_avr']
        resS=metaD['ent_std']
        txt2=' avr:%.1e rms:%.1e '%(resM,resS)
        print('AprEnt: %s'%(txt2))
        xLo=max(0,resM-5*resS)
        binsY=np.linspace(xLo,resM+5*resS, 40)
        binsX=np.linspace(min(tOff),max(tOff), 60)

        colMap=cmap.rainbow
        zsum,xbins,ybins,img =ax.hist2d(tOff,aprEntV,bins=(binsX,binsY), cmin=1,cmap = colMap)
        self.plt.colorbar(img, ax=ax)
        ax.set(title=tit,xlabel='window time offset',ylabel='aprox. entropy')
        ax.grid()

        ax.axhline(resM, color='k', linestyle='--')
        ax.axhline(resM-resS, color='k', linestyle=':')
        ax.axhline(resM+resS, color='k', linestyle=':')
        ax.text(0.3,0.85,txt2,transform=ax.transAxes)
        
#...!...!..................
    def VoltsSoma(self,F1,figId=100, stim=[], titL=['','']):
        tit1,tit2=titL
        nrow,ncol=4,2
        figId=self.smart_append(figId)
        # Remove horizontal space between axes
        fig,axA = self.plt.subplots(nrow, ncol, sharex=True, sharey=True,num=figId,facecolor='white', figsize=(16,8))
        #fig.subplots_adjust(,hspace=0,wspace=0) ??

        #print('eee',F1.shape)
        mxFr=F1.shape[0]
        nFr=nrow*ncol        
        frL=[ i for i in range(nFr)]
        #print('prL',prL)

        nBin=F1.shape[1]
        maxX=nBin
        xLab='time bins'; yLab='ampl (mV)'
        binsX=np.linspace(0,maxX,nBin)

        for ifr in range(nFr):
            frId=frL[ifr]
            ax=axA[ifr%nrow,ifr//nrow]
            V1=F1[frId]
            # np.random.shuffle(V1) 
            ax.plot(binsX,V1,label='fr%d'%(frId),c='b')
            if len(stim)>0:
                ax.plot(stim*10, label='stim',color='black', linestyle='--',linewidth=0.5)
            ax.grid(linestyle=':')
            ax.legend(loc='upper right', title='probe  idx:name')
            if ifr//nrow==0: ax.set_ylabel(yLab)
            if ifr%nrow==3: ax.set_xlabel(xLab)
            if ifr==0: ax.set_title(tit1)
            #if ifr==nrow: ax.set_title(tit2)


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
plot=Plotter_Entropy(args)

shortN=args.cellName

frame_per_hd5=6144 # exactly 6k
num_tbin=8000
numFrame=args.numShard * frame_per_hd5
Fall=np.zeros((numFrame,num_tbin),dtype='int16')

iOff=0
for shardId in range(args.numShard):
    inpF='%s/%s.cellSpike.data_%d.h5'%(shortN,shortN,shardId)
    inpD=read_data_hdf5(args.dataPath+inpF)
    assert frame_per_hd5<= inpD['frames'].shape[0]
    assert args.probeIndex < inpD['frames'].shape[2]
    
    Fall[iOff:iOff+frame_per_hd5]=inpD['frames'][:frame_per_hd5,:,args.probeIndex]
    iOff+=frame_per_hd5
    if iOff/args.numFrame >1.0:  break  # enough was collected

startT0 = time.time()
frames=Fall/150.
if args.numFrame>0 : frames=frames[:args.numFrame]
numFrame=frames.shape[0]
print('inp data',frames.shape)
#1plot.VoltsSoma(frames); plot.display_all()

T0 = time.time()
es = []
es2=[]
jo=[]
emb_len=2 #  using 3 gives similar results and runs 20% slower
r_dist=1.  # (mV)
nNaN=0
for i in range(numFrame):
    for _ in range(args.numSample):
        j=np.random.randint(frames.shape[1]-args.windowLength-1)
        X=frames[i,j:j+args.windowLength]
        #np.random.shuffle(X) # destroy any information
        stddev=X.std()
        # print('i=%d, j=%d, std=%.1f mV, emb_len=%d, r_dist=%.1f mV'%(i,j,stddev,emb_len,r_dist))
        entropyi=nolds.sampen(X, emb_dim=emb_len, tolerance=r_dist)
        #print(i,'esi',entropyi,X.shape)
        if i%50==0:print(' frame %d  time=%.4f(sec) len2=%d entropy=%.2e'%(i,time.time()-T0,len(es2),entropyi))
        if not np.isfinite(entropyi):
            nNaN+=1
            continue
        es.append(entropyi)
        jo.append(j)
        if j>500 and j<5000 :     es2.append(entropyi)


#print('es:',es)
es2=np.array(es2)
metaD={'ent_avr':es2.mean(),'ent_std':es2.std(),'num_data':es2.shape[0]}
print('n=%d avr ES=%.2e std(ES)=%.2e, numNaN=%d'%(metaD['num_data'],metaD['ent_avr'],metaD['ent_std'],nNaN))

tit='window_len=%d, emb_len=%d, r_dist=%.1f mV'%(args.windowLength,emb_len,r_dist)
#1plot.entropy_list(es,tit=tit)
plot.entropy_hist2D(jo,es,tit=tit,metaD=metaD)
plot.entropy_hist1D(es2,tit=tit)
plot.display_all()
