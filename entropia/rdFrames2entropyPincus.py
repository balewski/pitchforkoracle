#!/usr/bin/env python3
import sys,os
'''
based on 
https://en.wikipedia.org/wiki/Approximate_entropy
ApEn was developed by Steve M. Pincus 

This implementation of ApEn (see ~/prosty-waz/entropy)  is easy to understand but very slow - it works on lists instead of np-arrays
'''

import numpy as np
import time

from AproxEntropy_Func import ApEn
sys.path.append(os.path.abspath("/global/homes/b/balewski/pitchforkOracle/"))
from Util_IOfunc import write_yaml, read_yaml, write_data_hdf5, read_data_hdf5
from Plotter_Backbone import Plotter_Backbone

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")
    parser.add_argument('-s',"--numShard", default=1, type=int, help="number of data shards to be used")
    parser.add_argument("-o","--outPath",
                        default='out/',help="output path for plots and tables")

    parser.add_argument("-N", "--cellName", type=str, default='bbp153',
                        help="cell shortName")

    args = parser.parse_args()
    args.dataPath='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr/'
    args.prjName='cellEntropy'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
#............................
#............................
class Plotter_Entropy(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)

#...!...!..................
    def entropy(self,aprEntV,tit,figId=101):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(10,5))
        ax = self.plt.subplot(1,1,1)
        nChunk=len(aprEntV)
        binsX=[i+1 for i in range(nChunk)]
        ax.plot(binsX,aprEntV)
        ax.set(title=tit,xlabel='frame chunk',ylabel='apr entropy')
        ax.grid()
        
#...!...!..................
    def VoltsSoma(self,F1,figId=100, stim=[], titL=['','']):
        tit1,tit2=titL
        nrow,ncol=4,2
        figId=self.smart_append(figId)
        # Remove horizontal space between axes
        fig,axA = self.plt.subplots(nrow, ncol, sharex=True, sharey=True,num=figId,facecolor='white', figsize=(16,8))
        #fig.subplots_adjust(,hspace=0,wspace=0) ??

        #print('eee',F1.shape)
        mxFr=F1.shape[0]
        nFr=nrow*ncol        
        frL=[ i for i in range(nFr)]
        #print('prL',prL)

        nBin=F1.shape[1]
        maxX=nBin
        xLab='time bins'; yLab='ampl (mV)'
        binsX=np.linspace(0,maxX,nBin)

        for ifr in range(nFr):
            frId=frL[ifr]
            ax=axA[ifr%nrow,ifr//nrow]
            V1=F1[frId]
            # np.random.shuffle(V1) 
            ax.plot(binsX,V1,label='fr%d'%(frId),c='b')
            if len(stim)>0:
                ax.plot(stim*10, label='stim',color='black', linestyle='--',linewidth=0.5)
            ax.grid(linestyle=':')
            ax.legend(loc='upper right', title='probe  idx:name')
            if ifr//nrow==0: ax.set_ylabel(yLab)
            if ifr%nrow==3: ax.set_xlabel(xLab)
            if ifr==0: ax.set_title(tit1)
            #if ifr==nrow: ax.set_title(tit2)


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
plot=Plotter_Entropy(args)

shortN=args.cellName

frame_per_inp=6144  
num_tbin=8000
numFramesPerFile=args.numShard * frame_per_inp
Fall=np.zeros((numFramesPerFile,num_tbin),dtype='int16')

iOff=0
for shardId in range(args.numShard):
    inpF='%s/%s.cellSpike.data_%d.h5'%(shortN,shortN,shardId)
    inpD=read_data_hdf5(args.dataPath+inpF)
    assert frame_per_inp<= inpD['frames'].shape[0]
    Fall[iOff:iOff+frame_per_inp]=inpD['frames'][:frame_per_inp,:,0]
    iOff+=frame_per_inp


startT0 = time.time()
frames=Fall/150.
frames=frames[:,500:5500]
print('inp data',frames.shape)
#1plot.VoltsSoma(frames); plot.display_all()

T0 = time.time()
es = []
emb_len=3
r_dist=0.5  # (mV)
tot_len=400
for i in range(150):
    j=np.random.randint(frames.shape[1]-tot_len-2)
    k=np.random.randint(frames.shape[0])
    X=frames[i,j:j+tot_len]
    #np.random.shuffle(X) # destroy any information
    stddev=X.std()
    print('i=%d, j=%d, std=%.1f mV, emb_len=%d, r_dist=%.1f mV'%(i,j,stddev,emb_len,r_dist))
    entropyi=ApEn(X, emb_len, r_dist)
    print(i,'esi',entropyi,X.shape)
    print(' cal %d entropy time=%.4f(sec) shape'%(len(es),time.time()-T0))
    es.append(entropyi)
    #if len(es)>10: break
#print('es:',es)
es=np.array(es)
print('n=%d avr ES=%.2e std(ES)=%.2e'%(es.shape[0],es.std(),es.mean()))

tit='emb_len=%d, tot_len=%d, r_dist=%.2f'%(emb_len,tot_len,r_dist)
plot.entropy(es,tit=tit)
plot.display_all()
