Instruction for describing how to run the ML-training on bbp8v4_1 data set
Tested on 2019-10-07

0) Pull this code from the Bitbucket:

ssh -Y cori   (you should have graphic display capability)
Try:  xterm  - does it pop-up on your laptop?

git clone https://balewski@bitbucket.org/balewski/pitchforkOracle 
cd pitchforkOracle

1) The hd5 files from Anand are re-packed in to larger chunks of few GB/file.
The repacking is controlled by : rawMeta.cellSpike.yaml

The command is:
$ ./format_CellSpike.py  --dataPath data_bbp8v4_1_5h

- - - 
 cat data_bbp8v4_1_5h//../rawMeta.cellSpike.yaml
rawPath: /global/cscratch1/sd/asranjan/bbp8v4_1/volts/
maxDataShards: 1000
numShardsPerOutput: 20
rawDataName: he_1_1_16_volts_*
varParL:  [ skip1, skip2, skip3, skip4, skip5, gNaTa_tbar_NaTa_t, gK_Tstbar_K_Tst, skip8, gNap_Et2bar_Nap_Et2, skip10, gCa_HVAbar_Ca_HVA,gK_Pstbar_K_Pst, skip13, skip14, gCa_LVAstbar_Ca_LVAst, skip16, gSKv3_1bar_SKv3_1, skip18, skip19, gNaTs2_tbar_NaTs2_t, skip21, skip22, skip23 ]
#
stimName: he_1_1_16
probeName: [ soma, axon_0, apic_66, apic_79, dend_16 ]
shuffle: True
# units definition
timeAxis: { step: 0.02, unit: (ms) }
- - - - 

For the input of 1000 hd5 x 512 traces each it generates:
a) 50 cellSpike.data_*.h5, 4 GB per file, 10k traces per file
b) meta-data file meta.cellSpike.yaml which contains the following information needed by training to interpret the content of created cellSpike.data_*.h5 files: ['physRange', 'parName', 'numPar', 'stimulus', 'numDataFiles', 'splitIdx', 'totalGoodFrames', 'seenBadFrames', 'numTimeBin']
 
The 'formating' takes about 20 minutes and the size of the dir is ~190 GB.

balewski@cori12:> ll -h data_bbp8v4_1_5h
total 19G
-rw-rw---- 1 balewski nstaff 3.8G Oct  7 11:16 cellSpike.data_0.h5
-rw-rw---- 1 balewski nstaff 3.8G Oct  7 11:16 cellSpike.data_1.h5
-rw-rw---- 1 balewski nstaff 3.8G Oct  7 11:16 cellSpike.data_2.h5
-rw-rw---- 1 balewski nstaff 3.8G Oct  7 11:17 cellSpike.data_3.h5
-rw-rw---- 1 balewski nstaff 3.5G Oct  7 11:17 cellSpike.data_4.h5
<snip>
-rw-rw---- 1 balewski nstaff 116K Oct  7 11:17 meta.cellSpike.yaml
 
You can use my cellSpike.data_*.h5 files for testing.
Just link this directory into your place, do :

cd ~/pitchforkOracle
ln -s  /global/homes/b/balewski/prj/roy-neuron-sim-data/bbp8v4_1/packed5h data_bbp8v4_1_5h

Check you see the files:

ls -lh  data_bbp8v4_1_5h/*

Also prepare output dir for the training
mkdir out

Do ***NOT*** train it on cori login node - it would take over whole node and they will kick you out.

Instead 

2) request 1 interactive Haswell node, then TF load module, set the enviromnet for HD5
- - - 
cd ~/pitchforkOracle
module load tensorflow/gpu-1.14.0-py37
export HDF5_USE_FILE_LOCKING=FALSE
salloc --qos interactive -C haswell -t 2:00:00

./train_CellSpike.py bhla bhla
-----

3) verify content of hype-param file controlling the ML model is waht you want?:
- - - - 
$> cat hpar_cellSpike_100165_693.yaml
# Hyper params for cellSpike regression
# You can set key value to None-string  and it will get auto-converted to None 
myId: id164466367032c

# Data pre-processing
#digiNoise: [22., 0.6]
digiNoise: None
voltsScale: 50.

# CNN params
conv_filter: [18, 25]
conv_kernel: 5
conv_repeat: 3
pool_len: 7

# FC params
dropFrac: 0.01
fc_dims: [148, 131, 114, 97, 80, 63, 46]
lastAct: tanh
outAmpl: 1.2

# Training
batch_size:  128
steps: None  # None: generator will estimate it
lossName: mse
optimizerName: adam
reduceLR_factor: 0.14
- - -

4a) *********  train the  model on CPU, interactively:
It is very slow, it takes about 22 minutes per epoch for batch size of 128, reading data from data_bbp8v4_1_5h . You need ~15-30 epochs for good convergence - so CPU are only good to test the code, you should run on GPUs , where 1 epoch takes about 5 minutes. GPU is 4x faster.

On the worker node execute:
./train_CellSpike.py  --dataPath data_bbp8v4_1_5h  -X  -b128 -t 3600

You can observe, the training input generator estimated there are 51k frames and it will produce  395 steps, delivering batches of 128 frames:
>>>  gen-cnst:val nFiles=5 nFrames=50595 nSteps=395 x BS=128 is set

The training will run for about an hour, the code will estimate how many epoch will fit in to this time based on the training duration for the first 2 epochs.

Here is output from the firts 2 epochs - we have decent convergence (MSE loss =0.045) but full trainimng will give us 10x smaller loss (for this data set)

Epoch 1/2 - 1st epoch took 22 minutes:
3173/3173 [=======] - 1298s 409ms/step - loss: 0.0823 - val_loss: 0.0448


4b) ***************  train the  model on GPU, interactively:
ssh cori
cd ~/pitchforkOracle
module load esslurm
module load tensorflow/gpu-1.14.0-py37
mkdir outGpu

salloc  -C gpu -n10 --gres=gpu:1 -Adasrepo -t4:00:00 --exclusive
srun -n1 -c10  bash -c '  nvidia-smi -l 3 >&L.smi & python -u ./train_CellSpike.py  --dataPath data_bbp8v4_1_5h --outPath outGpu --noXterm  --batch_size 128 --trainTime 3600'

Example:  1st epoch took 5 minutes
Epoch 1/2
3170/3170 [=======] - 305s 96ms/step - loss: 0.1082 - val_loss: 0.0574
Epoch 3/8
3170/3170 [======] - 287s 91ms/step - loss: 0.0358 - val_loss: 0.0116


The final accuracy after 8 epochs (~1 wall hour on GPU) is
Epoch 8/8
3170/3170 [=========] - 286s 90ms/step - loss: 0.0079 - val_loss: 0.0052


Use this command to see if GPU are visible
srun -n1 bash -c '  nvidia-smi '
Mon Oct  7 12:27:33 2019       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 418.67       Driver Version: 418.67       CUDA Version: 10.1     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  Tesla V100-SXM2...  On   | 00000000:1A:00.0 Off |                    0 |
| N/A   29C    P0    38W / 300W |      0MiB / 16130MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+


4c) ********** train using  batch queue on GPU - the recommened way
Inspect and adopt Slurm script: batchTrain.slr


5) ******** predictions , using GPU-computed model
$  ./predict_CellSpike.py --design 100165_693 -n 2000  --dataPath data_bbp8v4_1_5h --seedModel  outGpu

names     0:gNaTa_tbar_NaTa_t    1:gK_Tstbar_K_Tst    2:gNap_Et2bar_Nap_Et2    3:gCa_HVAbar_Ca_HVA    4:gK_Pstbar_K_Pst    5:gCa_LVAstbar_Ca_LVAst    6:gSKv3_1bar_SKv3_1    7:gNaTs2_tbar_NaTs2_t
residue: U0->  2.5%   U1->  4.0%   U2->  4.5%   U3-> 12.7%   U4->  3.4%   U5-> 12.5%   U6->  3.6%   U7->  3.2%  
 global lossMSE=0.0049 , lossMAE=0.035 (computed), dom=val


= = = = = = = = = = = = = = = = =

Below is the  old instruction , the names have changed, the concepts reamined.
 - - - 
names      gna_dend     gkv_axon     gkv_soma
residu: U0->  7.2%  U1-> 22.9%  U2-> 36.5%  
 global lossMSE=0.064 , lossMAE=0.222 (computed)

names     gna_dend    gkv_axon    gkv_soma 
corrM    __var0__   __var1__   __var2__ 
 var0       1.000
 var1       0.139      1.000
 var2      -0.068     -0.739      1.000
- - - - 

Inspect PNG plots saved in out/ dir.
You can re-display them later with command

display out/xxx.png

This is loss vs. epoch:  idx0_cellMainen_train_f10
This are predictions and residua: idx0_cellMainen_train_f9.png

Jan 