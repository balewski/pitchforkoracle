#!/usr/bin/env python3
""" 
predict neuron params from volts
read input hd5 tensors
read trained net : model+weights
read test data from HD5
evaluate test data 

Use case: 
 ./predict_CellSpike.py  -n 5000 --seedModel out_80cell_c
Or for fixed cell
./predict_CellSpike.py -n 5000 --seedModel out_90cell_bbp027 --cellName bbp179

This code can do cross-prediction now, just speciffy the target cell name with this param --inferCellName (default=None)

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np
import  time

import sys,os
sys.path.append(os.path.abspath("toolbox"))
from Plotter_CellSpike import Plotter_CellSpike
from Deep_CellSpike import Deep_CellSpike
from Util_IOfunc import  write_yaml, read_yaml, write_data_hdf5
from Util_CellSpike import     param_physRange, get_UmZ_correl
from InpGenOntra_CellSpike import  CellSpike_input_generator

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    
    parser.add_argument('--venue', dest='formatVenue', choices=['prod','poster'], default='prod',help=" output quality/arangement")
    parser.add_argument("--cellName", type=str, default='witness', help="cell shortName , or witness or practice")

    parser.add_argument("-d", "--dataPath",help="path to input",
                        default='/global/homes/b/balewski/prjn/neuronBBP-pack8kHzRam/probe_3prB8kHz/ontra3/etype_8inhib_v1')
    
    parser.add_argument("--probeType",default='8inhib157c_3prB8kHz',  help="probe partition ")
    parser.add_argument("--inferCellName", type=str, default=None,help="inference cell shortName, use only for cross-inference")
  
    parser.add_argument("--seedModel",default='out', help="trained model and weights")
    parser.add_argument("--dom",default='test', help="domain is the dataset for which predictions are made, typically: test")

    parser.add_argument("--seedWeights", default=None,help="seed weights only, after model is created")

    parser.add_argument("-o", "--outPath", default='out/',help="output path for plots and tables")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm', action='store_true', default=False, help="disable X-term for batch mode")

    parser.add_argument("-n", "--events", type=int, default=1024, help="events for training, use 0 for all")
    parser.add_argument("--numFeature",default=None, type=int, help="ignored, flag exist for backward compatibility")
    args = parser.parse_args()
    args.prjName='cellSpike'
    args.modelDesign='findMe'
    args.dataPath+='/'
    args.outPath+='/'
    args.seedModel+='/'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    
    return args


#...!...!..................
def export_param_pred_4_Roy(U,Z,lossMSE,dom):
    print('export_results_4_Roy()')
    dataPred={'unitPred2D':Z,'unitTruth2D':U,'avr_lossMSE':lossMSE}
    outF=args.outPath+'/'+args.prjName+'.Upar_pred_%s.h5'%dom
    write_data_hdf5(dataPred,outF)

#...!...!..................
def rmsUmZ_for_positiveU(U,Z,parNameL):
    # input U has range [-1,1]
    # data with U<0 are discarded, but only for given component
    # slow implementation
    nPar=U.shape[1]
    outL=[]
    for i in range(nPar):
        uV=U[:,i]
        zV=Z[:,i]
        uM=uV>0
        umz=(uV-zV)[uM]
        avr=umz.mean()
        rms=umz.std()
        print(parNameL[i],umz.shape,avr,rms)
        outL.append(umz)
    ok_for_kris

#...!...!..................
def debiased_MSEloss(U,Z,parNameL, active=True):
     parMask=[  'fixed' not in name and 'const' not in name for name in parNameL]
     nTot=Z.shape[1]
     U=U[:,parMask]
     Z=Z[:,parMask]
     avrZ=np.mean(Z,axis=0) - np.mean(U,axis=0)  # for Ontra avrU!=0 
     print('ccc avrZ=',avrZ)
     print('ccc stdZ-U=',np.std(Z-U,axis=0))
     if active: Z-=avrZ
     UmZ=U-Z
     fac=Z.shape[1]/nTot
     avrUmZ=np.mean(UmZ,axis=0)
     t1=np.power(UmZ,2); compMSE=t1.mean()*fac; stdMSE=t1.std()*fac
     print('\n M:computed: lossMSE= %.3g  std=%.3g  n=%.2g'%( compMSE,stdMSE,Z.shape[0]),' max bias=%.3g (corrected=%r) opaque=%s'%(np.max(avrUmZ),active,opaque))
     return compMSE

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

deep=Deep_CellSpike.predictor(args)
plot=Plotter_CellSpike(args,deep.metaD  )

if args.seedModel=='same/' :  args.seedModel=args.outPath
deep.load_model_full(args.seedModel)
if args.seedWeights:
    if args.seedWeights=='same' : args.seedWeights=args.seedModel
    deep.load_weights_only(path=args.seedWeights)

# instantiate input generator
inpF=args.seedModel+deep.prjName+'.sum_train.yaml'    
trainM=read_yaml(inpF)
#print('bbb', blob.keys())
genConf=trainM['inpGen']['train']

dom=args.dom
opaque=[1]

if args.cellName in ['practice','witness']:
    opaque=args.cellName
    genConf['cellList']=deep.metaD['cellSplit'][opaque]
else:
    print('M: 1-cell predict for',args.cellName)
    opaque=args.cellName
    genConf['cellList']=[args.cellName]
print('M:aaa',genConf['cellList'])


genConf['name']='IG-'+dom+'-'+opaque
genConf['domain']=dom
genConf['myRank']=0
genConf['numLocalSamples']=args.events
genConf['dataPath']=deep.dataPath
genConf['shuffle']=True  # use False  for reproducibility
genConf['x_y_aux']=True # it will produce 3 np-arrays per call

if args.inferCellName!=None:
    print('WARN: cross-inference train:',genConf['h5nameTemplate'],' infere:',args.inferCellName)
    genConf['h5nameTemplate']=args.inferCellName+genConf['h5nameTemplate'][6:]
    #genConf['numFeature']=21 # overwrites to make predictions possible, bbp153
    print('changed inputGen to:',genConf)

#.... creates data generator
inpGen=CellSpike_input_generator(genConf,verb=1)

steps=max(1,args.events//genConf['localBS'])
mxSteps=inpGen.__len__()
steps=min(mxSteps,steps)
num_data=steps*genConf['localBS']

print('start predicting for dom=',dom,' num data=',num_data,' steps=',steps, ' mxSteps=',mxSteps,'BS=',genConf['localBS'])
assert mxSteps >= steps # prevents re-use test data (beyond 1  true epoch)

startT0 = time.time()
uL=[]; zL=[]
for i in range(steps):
    x,u,aux =inpGen.__getitem__(i)
    z= deep.model.predict(x)
    if i%20==0 :  print('M:%d step of %d done, shapes u,z:'%(i,steps),u.shape,z.shape)
    assert z.shape[1]==u.shape[1]
    uL.append(u)
    zL.append(z)

predTime=time.time() - startT0
print('pred-done, eve=%d, elaT=%.1f sec, len(zL)=%d'%(args.events,predTime,len(zL)),i)

# concatenate all steps 
U=np.concatenate(tuple(uL),axis=0)
Z=np.concatenate(tuple(zL),axis=0)

print('got predicted Z, shape:',Z.shape)
xxlossMSE,lossAudit=get_UmZ_correl(U-Z,dom,deep.metaD['parName'])

# this is more accurate value
biasActive=False
#biasActive=True
if biasActive:
    avrZ=np.mean(Z,axis=0) - np.mean(U,axis=0)  # for Ontra avrU!=0 
    Z-=avrZ
    print('ccc avrZ=',avrZ)

#TOO-COMPLEX:lossMSE=debiased_MSEloss(U,Z,deep.metaD['parName'], active=biasActive)    
#rmsUmZ_for_positiveU(U,Z,deep.metaD['parName'])  #hack fro Kris
UmZ=U-Z
t1=np.power(UmZ,2); lossMSE=t1.mean()
print('\n M:computed: lossMSE= %.3g  n=%.2g'%( lossMSE,Z.shape[0]), 'bias corrected=%r opaque=%s'%(biasActive,opaque))

#1plot.frames_vsTime(x,u,2,metaD=metaD) #hack
#tit='dom=%s'%(dom)
#1plot.fparams(z,'pred Z',figId=8,tit2=tit)

sumRec={}
sumRec['domain']=dom
sumRec[dom+'LossMSE']=float(lossMSE)
sumRec['predTimeMinutes']=predTime/60.
sumRec['numSamples']=U.shape[0]
sumRec['modelDesign']=trainM['modelDesign']
sumRec['genConf']=genConf
sumRec['lossAudit']=lossAudit
sumRec['short_name']=deep.metaD['shortName']
sumRec['full_name']=deep.metaD['fullName']
sumRec['predDataList']=genConf['cellList']
sumRec['numFeature']=inpGen.XY_shapes()['X'][1]
sumRec['featureType']=deep.metaD['featureType']
sumRec['lossThrHi']=0.50  # for tagging plots???
sumRec['lossThrLo']=0.10


plot.fparam_residua_2D(U,Z,sumRec)

if args.formatVenue=='prod' and 0:
    plot.fparam_residua_summary(None,sumRec)

    
#plot.fparams(u,'true U',figId=6,tit2=tit)
#plot.plt.axis('off')

write_yaml(sumRec, deep.outPath+'/'+deep.prjName+'.sum_pred_%s_%s.yaml'%(args.cellName,dom))

#1export_param_pred_4_Roy(U,Z,lossMSE,dom)

plot.display_all('predict_%s_%s'%(args.cellName,dom),png=1)









