#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

echo 'D:train-driver start on '`hostname`' '`date`
procIdx=${SLURM_PROCID} 
runSec=${1-120}
shortName=$2
design=$3  
probeType=$4  
useDataFrac=$5
numFeature=$6

export CUDA_VISIBLE_DEVICES=$SLURM_LOCALID

echo D:my procIdx=$procIdx runSec=$runSec  shortName=$shortName "pwd: "`pwd`
echo D:RANK_`hostname` $SLURM_PROCID $SLURM_LOCALID numFeature=$numFeature

if [ $SLURM_LOCALID -eq 0 ] ; then
 nvidia-smi -l 10 >&L.smi_`hostname` &
fi

#2 this is for handling task-list - not used currently
#2maxProc=`cat $taskList |wc -l`
#2if [ $procIdx -ge $maxProc ]; then
#2   echo "D:rank $procIdx above maxProc=$maxProc, idle ..."; exit 0
#2fi
#2line=`head -n $[ $procIdx +1 ] $taskList |tail -n 1`
#2shortName=`echo $line | cut -f1 -d\ `
#batch_size=`echo $line | cut -f2 -d\ `
#dropFrac=`echo $line | cut -f3 -d\ `
#myName=${shortName}_pr${num_probes}_des${design}
myName=${shortName}

echo "D:myName=${myName}=  probeType=$probeType "
outDir=out/${procIdx}_$myName
dataPath=data/$shortName
logN=${procIdx}_${myName}

mkdir -p $outDir
pwd

# IF continuation activate those lines:
#inpPath0=/global/cscratch1/sd/balewski/neuronBBP-train7/444637/7/out
#seedWeights=${inpPath0}/${myName}
#echo ini design=$design seedWeights=$seedWeights 
#ls -l $seedWeights/cellSpike.weights_best.h5

# IF HPAR search activate the lines below, it overwrites $design from the input
#design=${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}_${procIdx}_$shortName
#./genHPar_CellSpike.py --numTimeBin 1500 --nOutFeat  15  --outPath ./  --design $design

# stagger starts on each node to ease the cold-start IO
sleep $(( 2 + ${SLURM_LOCALID}*4 ))
echo D:start train ${SLURM_PROCID} at $outDir GPU=${CUDA_VISIBLE_DEVICES}=
other_opt=""
if [[ $numFeature != "None" ]] ; then
    other_opt=" --numFeature $numFeature "
    echo "add =${other_opt}="
fi

python -u ./train_CellSpike.py -t $runSec   --verbosity 0  --noXterm --design $design  --outPath   $outDir --dataPath $dataPath --jobId ${SLURM_ARRAY_JOB_ID}/${SLURM_ARRAY_TASK_ID}/${procIdx}  --useDataFrac $useDataFrac --probeType $probeType  --maxEpochTime 4800 ${other_opt} >&log.train_$logN

echo D:train-done-`date`
# spare:  --seedWeights $seedWeights --batch_size $batch_size --dropFrac $dropFrac  

echo "D:start predictions at PWD  "`pwd`

sumF=${outDir}/cellSpike.sum_train.yaml
echo check for sumF=$sumF

echo
if [  -e "${sumF}"  ] ;then
    ./predict_CellSpike.py   --design $design   -n 5000 --noXterm --venue poster  --outPath   $outDir --dataPath $dataPath --seedModel same  --probeType $probeType >&log.pred_$logN
    echo D: predict-done-`date`
else
    echo D:traing crashed no predictions
fi


echo 'D:task done  on  '`date` 
