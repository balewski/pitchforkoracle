#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;  #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

outPath=/global/cscratch1/sd/balewski/ontra3/predPerCell/b


# per cell  design=smt190927_a
trainPath0=/global/cscratch1/sd/balewski/ontra3/1181540/
#Cori, BBP019 HPO design=smt190927_a
trainPathOntra3=~/prjn/neuronBBP-outHPO-8kHz/ontra3-packSetD/1179761_1_bbp019/out

numEve=50000
taskList=witnessOntra3.list

echo path=$outPath  numEve=$numEve
mkdir -p $outPath



for k in `seq 16 17`; do
    cellName=`head -n $[ $k ] $taskList |tail -n 1`
    echo M: k=$k cellName=$cellName

    modelPath=${trainPath0}/${k}/out
    echo $k  $modelPath 

    #...... per cell:
    prefix=${cellName}_perCell
    log=$outPath/log.${prefix}
    ./predict_CellSpike.py --cellName $cellName -n $numEve --seedModel $modelPath  -X >& $log
    grep "global lossMSE" $log
    mv out/cellSpike_predict_test_f9.png $outPath/${prefix}.cellSpike_predict_witness_f9.png
    mv out/cellSpike.sum_pred_test.yaml  $outPath/${prefix}.cellSpike.sum_pred_witness.yaml
    
    #...... Ontra3:
    modelPath=$trainPathOntra3
    prefix=${cellName}_ontra3
    log=$outPath/log.${prefix}
    ./predict_CellSpike.py --cellName $cellName -n $numEve --seedModel $modelPath  -X >& $log
    grep  "global lossMSE" $log
    mv out/cellSpike_predict_test_f9.png $outPath/${prefix}.cellSpike_predict_witness_f9.png
    mv out/cellSpike.sum_pred_test.yaml  $outPath/${prefix}.cellSpike.sum_pred_witness.yaml
    
    
    #exit
done

exit


# per cell  design=a2f791f3a	
trainPath0=/global/homes/b/balewski/prjn/neuronBBP-outHPO-8kHz/ontra3-packSetD/1149899_perCell
#Summit Ontra3 HPO design=a2f791f3a	
trainPathOntra3=/global/homes/b/balewski/prjn/neuronBBP-outHPO-8kHz/ontra3-packSetD/459230_6_out/


