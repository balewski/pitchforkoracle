#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;  #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value


function init_cell( ) {
    mkdir -p $path
    cp ${inpPath}/${cell}/stim.chaotic_2.yaml $path
}

inpPath=/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr
outPath=/global/cfs/cdirs/m2043/balewski/neuronBBP-packTest9
#outPath=/global/cscratch1/sd/balewski/neuronBBP-packTest8
echo path=$outPath
cellL="bbp006"
# 4 Roy's cells bbp019:23pr
#cellL="bbp006 bbp153 bbp027 bbp102 bbp019"
#cellL="bbp006 bbp027  bbp019"
#cellL="bbp153  bbp102"


mkdir -p ${outPath}/scripts

for probes in 6 ; do
    pt=${probes}pr8kHz
    echo work on probeType=$pt
    path0=${outPath}/probe_${pt}
    echo $pt $path0
    for cell in $cellL ; do
	#echo cell=$cell
	path=${path0}/${cell}
	echo $path
	#init_cell ; continue  #only  when new dir-tree is created
	./freqTransformMeta.py --cellName ${cell} --outPath $outPath ; chmod a+x ${outPath}/scripts/repack_${cell}.sh 
       
	#exit
	#break
    done
    # end of probeType
    #break
done

