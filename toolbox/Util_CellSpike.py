__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from tensorflow.keras.callbacks import Callback
import tensorflow.keras.backend as K
import numpy as np

from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Dropout,   Input, Conv1D,MaxPool1D,Flatten,Reshape, Lambda, BatchNormalization
from  tensorflow.python.keras.layers.advanced_activations import LeakyReLU 
import tensorflow as tf

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
class MyLearningTracker(Callback):
    def __init__(self):
        self.hir=[]
    def on_epoch_end(self, epoch, logs={}):
        optimizer = self.model.optimizer
        lr = K.eval(optimizer.lr)
        self.hir.append(float(lr))

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
class MyEpochEndLoss(Callback):
    def __init__(self,inpGen):
        self.hir=[]
        self.inpGen=inpGen
        print('MyEpochEndLoss numSteps',inpGen.__len__())
    def on_epoch_end(self, epoch, logs={}):
        loss = self.model.evaluate(self.inpGen)
        self.hir.append(loss)
        


#...!...!..................
def param_by_name(parNameL):
    #print(parNameL,len(parNameL))
    morphArmL=[]
    morphD={}
    for i,x in enumerate(parNameL):
        xL=x.split('.')
        #print(i,xL)
        arm=xL[-1]
        if arm not in morphD: morphD[arm]=[]
        morphD[arm].append(i)

    morphIdxL=[]
    morphArmL=[]
    for arm in sorted(morphD):
        armL=morphD[arm]
        #print('add',arm,armL)
        if len(armL)>0:
            morphArmL.append((arm,len(morphIdxL)))
        morphIdxL+=armL
    #print('aa1',morphArmL)
    #print('aa2',morphIdxL)
    # test order is correct
    
    #for i in morphIdxL: print(i,parNameL[i])
    
    return morphIdxL,morphArmL

#...!...!..................
def get_UmZ_correl(UmZ,dom,parNameL):
        #print('UmZ dims:',UmZ.shape)

        # compute cov matrix, it automatically subtracts bias (bias= avr UmZ per param)
        c=np.cov(UmZ,rowvar=False)
        nv=c.shape[0]
        nvL=range(nv)

        c1=np.diagonal(c)
        rmseU=np.sqrt(c1)
        #print('one RMSE diag for UmZ:',rmseU)
        #print('MSE per parameter:',c1)
        
        avrSig=rmseU.mean()
        print('global RMS avr=%.3f , var=%.3f '%(avrSig, avrSig*avrSig))

        avrResi=UmZ.mean(axis=0)
        #print('avrResi',avrResi.shape,avrResi)

        #print('gg',parNameL, nvL)
        print('\nnames ',end='')
        [ print('   %2d:%s'%(i,parNameL[i]),end='') for i in nvL]

        print('\nresidue:',end='')
        [ print(' U%d-> %4.1f%s  '%(i,100*rmseU[i],chr(37)),end='') for i in nvL ]
        lossAudit=[ [parNameL[i],float(avrResi[i]),float(rmseU[i])] for i in nvL ]        
        lossMAE=np.absolute(UmZ).mean()
        lossMSE=c1.mean()
        print('\n global lossMSE=%.4f , lossMAE=%.3f (computed, bias corrected), dom=%s'%( lossMSE,lossMAE,dom))
        #print('sde',UmZ.dtype,c1.dtype,c1.shape,'\nc=',c.shape,c1)
        

        #print('\ncorrelation matrix for %d dim vector'%nv)
        r=np.corrcoef(UmZ,rowvar=False)
        #pprint(r)

        if 0: # prints big table of correlation coef - not readbale for 31 probes
            print('\nnames  ',end='')
            [ print(' %2d:%s'%(i,parNameL[i]),end='') for i in nvL]
            print('\ncorrM   ',end='')
            [ print(' __var%1d__ '%i,end='') for i in nvL]
            for k in nvL:
                print('\n var%1d '%k,end='')
                [ print('    %6.3f'% r[i][k],end='') for i in range(k+1)]
                print('      .   '*(nv-k-1),end='')
            print()
            
        return np.array([lossMSE]), lossAudit




#...!...!..................
def param_physRange(inpL):
    # pre-process phys ranges
    par_out=[]
    for (name,[a,b],k) in inpL:
        h=(b-a)/2.
        m=(b+a)/2.
        par_out.append([a,b,m,h,name])
    #print('par_out=',par_out)
    return par_out


#...!...!....................
def build_model_cellSpike2(hpar,hvd=None,verb=1):
    inpLyr=Input(shape=tuple(hpar['inpShape']),name='inp')
    lyr=inpLyr
    C=hpar['cnn_block']
    
    # decide location of BN
    bn_k=-1
    if  hpar['batch_norm_cnn']:
        bn_k=len( C['filters'])//2
        assert bn_k>0

    for k,dim in enumerate(C['filters']):
        if bn_k==k: lyr=BatchNormalization()(lyr)

        lyr=Conv1D(dim,C['kernel'],strides=1,activation='linear',padding='same' )(lyr)
        lyr =LeakyReLU()(lyr)
        lyr=MaxPool1D(pool_size=C['pool_size'],padding='same')(lyr)

    # transition to FC
    lyr=Flatten(name='to_1d')(lyr)

    if hpar['batch_norm_flat']:  lyr=BatchNormalization()(lyr)

    # assembly FC block
    C=hpar['fc_block']
    dropFrac=C['dropFrac']

    for fc_dim in C['units']:
        lyr=Dense(fc_dim,activation='linear')(lyr)
        lyr=LeakyReLU()(lyr)
        if dropFrac>0:  lyr= Dropout(dropFrac)(lyr)

    # add last layer with proper activation
    outAmpl=1.2
    lyr=Dense(hpar['outShape'], activation='tanh')(lyr)
    lyr=Lambda(lambda val: val*outAmpl)(lyr)

    model = Model(inputs=inpLyr, outputs=lyr)
    totLyrCnt=len(model.layers)
    totParCnt=model.count_params()

    C=hpar['train_conf']
    lossName=C['lossName']
    optName=C['optName']
    initLR=C['LRconf']['init']

    if verb:
        model.summary() # will print 
        print('compile model',optName,lossName)

    if optName=='adam' :
        opt=tf.keras.optimizers.Adam(lr=initLR)
    elif optName=='nadam' :
        opt=tf.keras.optimizers.Nadam(lr=initLR)
    elif optName=='adadelta' :
        opt = tf.keras.optimizers.Adadelta(lr=initLR)
    elif optName=='adagrad' :
        opt = tf.keras.optimizers.Adagrad(lr=initLR)
    else:
        crash_21


    lossName=hpar['train_conf']['lossName']
    # add Horovod if requested
    if hvd!=None:
        if verb: print('  add Horovod Distributed Optimizer, initLR=',initLR)
        opt = hvd.DistributedOptimizer(opt)
        model.compile(optimizer=opt, loss=lossName,experimental_run_tf_function=False) # needed for TF2
    else:  # noHorovod 1-worker training 
        model.compile(optimizer=opt, loss=lossName)

    if verb:
        print('Model summary   layers=%d , params=%.1f K, inputs:'%(totLyrCnt,totParCnt/1000.),model.input_shape,'output:',model.output_shape)

    return model,totParCnt

