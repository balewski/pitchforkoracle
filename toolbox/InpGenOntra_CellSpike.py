__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
reads all data at once and serves them from RAM
- optimized for mult-GPU training w/ Horovod
- only used block of data  from each cell-file
- reads data from multiple files

    Generates data for Keras.fit
    Keep all data in RAM
    Read block of data from each cell-file in constructor
    Shuffle: only  all samples after compleated read
    Allow Keras.fit to requests for random batches
'''

import time,  os
import random
import h5py
import numpy as np
import copy

from tensorflow.keras.utils import Sequence

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
class CellSpike_input_generator(Sequence):

#...!...!..................
    def __init__(self, conf0, verb=1):
        self.conf=copy.deepcopy(conf0)  # the input conf0 is reused later in the upper level code
        self.verb=verb
        self.stepTime =[] # history of completed epochs      
        self.cnt={'epoch':0,'step':0,'file':0}
        self.localBS=self.conf['localBS']

        cellL=self.conf['cellList']
        numCell=len(cellL)
        numTimeBin=self.conf['numTimeBin']
        numFeature=self.conf['numFeature']
        numPar=self.conf['numPar']
        assert numCell>=1
        self.conf['numSamplesPerCell']=self.conf['numLocalSamples']//numCell
        self.numLocFrames=self.conf['numSamplesPerCell']*numCell # can be < numLocalSamples
        assert self.conf['numSamplesPerCell']>=1
        self.stepPerEpoch=int(np.floor( self.numLocFrames/ self.localBS))
        if  self.stepPerEpoch <1:
            print('\nABORT, Have you requested too few samples per rank?, numLocFrames=%d, BS=%d  name=%s'%(self.numLocFrames, self.localBS,self.conf['name']))
            exit(67)
            
        if self.verb:
            print('\ngen-cnst name=%s  shuffle=%r BS=%d steps=%d myRank=%d numSampl/cell=%d'%(self.conf['name'],self.conf['shuffle'],self.localBS,self.__len__(),self.conf['myRank'],self.conf['numSamplesPerCell']),'H5-path=',self.conf['dataPath'])
        assert self.numLocFrames>0
        assert self.conf['myRank']>=0

        if self.verb>1:
            print('IG: use %d cells:'%(len(cellL)),cellL)

        # malloc storage for the data, to avoid concatenation
        self.data_frames=np.zeros((self.numLocFrames,numTimeBin,numFeature),dtype='float32')
        self.data_parU=np.zeros((self.numLocFrames,numPar),dtype='float32')
        if self.conf['x_y_aux']:
            self.data_parP=np.zeros((self.numLocFrames,numPar),dtype='float32')

        idxA=np.arange(self.numLocFrames)
        # RAM-efficient pre-shuffling of target indexes
        if self.conf['shuffle']: 
            np.random.shuffle(idxA)
            if self.verb: print('IG:pre-shufle all local %d frames'%self.numLocFrames)
                
        # prime this generator  ... takes few sec/cell
        startTm0 = time.time()
        for ic in range(numCell):
            numSamp=self.conf['numSamplesPerCell']
            idxOff=ic*numSamp
            goalIdxL=idxA[idxOff:idxOff+numSamp]
            self.openH5(cellL[ic],goalIdxL,ic)

        startTm1 = time.time()
        
        if self.verb :            
            print(' IG:load-end of all HD5, read time=%.2f(sec) name=%s numCell=%d, numLocSamp=%d, numLocSamp/cell=%d localBS=%d'%(startTm1 - startTm0,self.conf['name'],numCell,self.numLocFrames,self.conf['numSamplesPerCell'],self.localBS))
            print(' IG:Xall',self.data_frames.shape,self.data_frames.dtype)
            print(' IG:Uall',self.data_parU.shape,self.data_parU.dtype)
            if self.conf['x_y_aux']:
                print(' IG:Pall',self.data_parP.shape,self.data_parP.dtype)


        
#...!...!..................
    def openH5(self,cellN,goalIdxL,ic):
        fnameTmpl=self.conf['dataPath']+self.conf['h5nameTemplate']
        inpF=fnameTmpl.replace('*',cellN)
        numSamp=self.conf['numSamplesPerCell']
        numFeature=self.conf['numFeature'] # this is what user wants
        dom=self.conf['domain']

        pr=self.verb>0 and (ic<10 or ic%20==0)
        if self.verb>1 : print('IG:cell %s name=%s, idxOff[0..3]='%(cellN,self.conf['name']),goalIdxL[:3],'inpF=',inpF)
        if not os.path.exists(inpF):
            print('FAILD, missing HD5',inpF)
            exit(22)
                        
        doAux= self.conf['x_y_aux']
        startTm0 = time.time()
        
        if pr :
            print('IG:read numSamp=%d data from %d hdf5:%s'%(numSamp,ic,inpF),', doAux=%r '%(doAux))

        # = = = READING HD5  start
        h5f = h5py.File(inpF, 'r')
        Xshape=h5f[dom+'_frames'].shape
        totSamp=Xshape[0]
        maxShard=totSamp//numSamp
        if  maxShard <1:
            print('\nABORT, Have you requested too many samples per rank?, one cell Xshape:',Xshape,'name=',self.conf['name'])
            exit(66)
        # chosen shard is rank dependent, wraps up if not sufficient number of ranks
        myShard=self.conf['myRank'] %maxShard
        sampIdxOff=myShard*numSamp
        if pr: print('IG:file myShard=%d, maxShard=%d, sampIdxOff=%d, file:'%(myShard,maxShard,sampIdxOff),'X-cell:',Xshape)        

        assert self.conf['numTimeBin']==Xshape[1]
        assert numFeature<=Xshape[2]
        #print('zzzNF',numFeature,Xshape[2])
        # data reading starts
        if numFeature==Xshape[2]:
            self.data_frames[goalIdxL]=h5f[dom+'_frames'][sampIdxOff:sampIdxOff+numSamp]
        else:
            self.data_frames[goalIdxL]=h5f[dom+'_frames'][sampIdxOff:sampIdxOff+numSamp,:,:numFeature] 
        self.data_parU[goalIdxL]=h5f[dom+'_unitStar_par'][sampIdxOff:sampIdxOff+numSamp]
        if doAux:
            self.data_parP[goalIdxL]=h5f[dom+'_phys_par'][sampIdxOff:sampIdxOff+numSamp]
        h5f.close()
        # = = = READING HD5  done
        self.cnt['file']+=1

        if pr :
            startTm1 = time.time()        
            print(' hd5 read time=%.2f(sec) dom=%s cell=%s'%(startTm1 - startTm0,dom,cellN))
 
        # .......................................................
        #.... data embeddings, transformation should go here ....

        
        #.... end of embeddings ........
        # .......................................................
        

    #...!...!..................
    def __len__(self):
        'Denotes the number of local batches per epoch'
        return self.stepPerEpoch

    
    #...!...!..................
    def __getitem__(self, input_index):
        'use input index to assure good performance'

        #print('GI:',input_index,self.conf['name'])
        bs=self.localBS
        pCnt=input_index*bs
        
        # WARN : do NOT do data embeddings here
        # (most likely) this code is not thread-safe and works well (by accident) if the code below takes ~0 time
        
        X=self.data_frames[pCnt:pCnt+bs]
        Y=self.data_parU[pCnt:pCnt+bs]
        
        # hack for TF2.1 who is not calling on_epoch_end() any more
        self.cnt['step']+=1
        if self.cnt['step']%self.__len__()==0: self.on_epoch_end2()
                    
        if self.conf['x_y_aux']: # predictions for Roy
            AUX=self.data_parP[pCnt:pCnt+bs]
            return (X,Y,AUX)
        
        return (X,Y)

    #...!...!..................
    def XY_shapes(self):
        return {'X':list(self.data_frames.shape[1:]),'Y': list(self.data_parU.shape[1:] )}

    #...!...!..................
    def on_epoch_end2(self):
        'Updates history after each epoch'
        self.cnt['epoch']+=1
        if self.cnt['epoch']%10==0 and self.verb:
                print('GEN:%s on_epoch_end cnt:'%self.conf['name'],self.cnt)
        timeRec=[time.time(), self.cnt['epoch']]
        self.stepTime.append(timeRec)
 
#...!...!..................
    def on_epoch_end(self):
        #if self.verb: print('XXX Updates history after each epoch',self.conf['name'],self.cnt)
        a=1
