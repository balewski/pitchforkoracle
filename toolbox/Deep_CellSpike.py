__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import os, time
import warnings
import socket  # for hostname
import copy
import numpy as np
import h5py

from pprint import pprint

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import tensorflow as tf

from Util_IOfunc import write_yaml, read_yaml
from Util_CellSpike import MyLearningTracker, MyEpochEndLoss
from Util_CellSpike import build_model_cellSpike2

from tensorflow.python.keras.models import Model, load_model
from tensorflow.python.keras.callbacks import EarlyStopping, ModelCheckpoint , ReduceLROnPlateau

from InpGenOntra_CellSpike import  CellSpike_input_generator

VERBOSE_RANKS=[0]  # select here which ranks will print, it is a list

#............................
#............................
#............................
class Deep_CellSpike(object):

    def __init__(self,**kwargs):
        for k, v in kwargs.items():
            self.__setattr__(k, v)
        
        for xx in [ self.dataPath, self.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            exit(99)
        
        self.metaF='/meta.cellSpike_%s.yaml'%self.probeType
        #print(self.__class__.__name__,'TF ver:', tf.__version__,', prj:',self.prjName)


    # alternative constructors

    @classmethod #........................
    def trainer(cls, args):
        obj=cls(**vars(args))
        # initialize Horovod - if requested
        if obj.useHorovod:
            obj.config_horovod()
            if obj.verb:
                for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
        else:
            obj.myRank=0
            obj.numRanks=1
            obj.hvd=None
            print('Horovod disabled')

        if obj.verb:
            print('Cnst:train, myRank=',obj.myRank)
            print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))
            
            #gLpd=tf.config.list_physical_devices('GPU')
            #gprint('Lpd, devCnt=',len(Lpd), Lpd)


        obj.read_metaInp(obj.dataPath+obj.metaF)
        obj.train_hirD={'acc': [],'loss': [],'lr': [],'val_acc': [],'val_loss': []}
        obj.sumRec={}

        # . . . load hyperparameters
        hparF='./hpar_'+obj.prjName2+'.yaml'
        bulk=read_yaml(hparF,verb=obj.verb)
        if obj.verb: print('bulk',bulk)
        obj.hparams=bulk

        # overwrite some hpar if provided from command line
        if obj.dropFrac!=None:  
            obj.hparams['fc_block']['dropFrac']=obj.dropFrac
        
        if obj.localBS!=None:
            obj.hparams['train_conf']['localBS']=obj.localBS

        if obj.numFeature!=None:
            obj.hparams['inpShape'][1]=obj.numFeature

        LRconf=obj.hparams['train_conf']['LRconf']
        LRconf['init']*=obj.initLRfactor
        if obj.verb: print('use initLR=%.3g'%LRconf['init'])
            
        # sanity checks
        assert obj.hparams['fc_block']['dropFrac']>=0
        assert obj.hparams['train_conf']['localBS']>=1
        assert obj.hparams['inpShape'][1]>0
        
        return obj

    @classmethod #........................
    def predictor(cls, args):
        print('Cnst:sim-pred')
        obj=cls(**vars(args))
        obj.read_metaInp(obj.dataPath+obj.metaF)
        return obj

#...!...!..................
    def config_horovod(self):
        import horovod.tensorflow.keras as hvd 

        hvd.init()
        self.numRanks= hvd.size()
        self.myRank= hvd.rank()
        self.hvd=hvd
        self.verb*=self.myRank in VERBOSE_RANKS  # make silent other ranks
        

        print(' Horovod: initialize  myRank=%d of %d on %s verb=%d'%(self.myRank,self.numRanks,socket.gethostname(),self.verb))
        #exit(0)
        #time.sleep(self.myRank/0.5) # stagger ranks in time so they don't open files at the same time

        # Horovod: pin GPU to be used to process local rank (one GPU per process)
        # TF2
        assert tf.__version__[0]=='2'
        gpus = tf.config.experimental.list_physical_devices('GPU')
        
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        if gpus:
            tf.config.experimental.set_visible_devices(gpus[hvd.local_rank()], 'GPU')
        
#............................
    def read_metaInp(self,metaF):
        bulk=read_yaml(metaF,verb=self.verb)
        self.metaD=bulk['dataInfo']
        #?self.metaDrw=bulk['rawInfo'] # only for plotting of some raw info
        if self.verb: print('read metaInp:',self.metaD.keys())
        self.metaD['metaFile']=self.metaF
        if self.verb>1: print('read metaInp dump',self.metaD)
        # update prjName if not 'format'
        self.prjName2=self.prjName+'_'+self.modelDesign

   #............................
    def build_model(self):
        hpar=self.hparams
        if self.verb: print('build_model hpar:',hpar)
        self.model,totParCnt=build_model_cellSpike2(hpar,self.hvd,self.verb)
        # create summary record
        rec={'hyperParams':hpar,
             'modelWeightCnt':totParCnt,
             'modelLayerCnt':len(self.model.layers),
             'modelDesign' : self.modelDesign,
             'hostName' : socket.gethostname(),
             'numRanks': self.numRanks,
             'state': 'model_build',
             'jobId': self.jobId
        }
        self.sumRec.update(rec)
        
#............................
    def init_genetors(self):
        hpar=self.hparams
        opaque='practice'
        genConf={'h5nameTemplate':self.metaD['h5nameTemplate'],
                 'cellList':self.metaD['cellSplit'][opaque],
                 'numTimeBin':self.hparams['inpShape'][0],
                 'numFeature':self.hparams['inpShape'][1],
                 'numPar':self.metaD['numPar'],
                 'dataPath':self.dataPath,
                 'shuffle':self.shuffle_data,
                 'x_y_aux':False,
                 'numLocalSamples':self.localSamples,
                 'myRank':self.myRank
        }
        
            
        if self.cellName!=None:
            print('D: 1-cell training for',self.cellName)
            genConf['cellList']=[self.cellName]
        
        if self.verb:
            print('D:init Generators, inpDir=%s, localBS=%d, localSamples=%d inputProbes'%(self.dataPath, hpar['train_conf']['localBS'],self.localSamples),genConf['numFeature'])
            print('  gen use %d cells from opaque=%s'%(len(genConf['cellList']),opaque))

        for x in ['localBS',]:
            genConf[x]=hpar['train_conf'][x]
            
        self.sumRec['inpGen']={}
        self.inpGenD={}
        for dom in ['train','val']:
            genConf['domain']=dom
            genConf['name']='%d-%s-%s'%(self.myRank,dom,opaque)
            if dom=='val': genConf['numLocalSamples']//=8 # needs less data
            self.inpGenD[dom]=CellSpike_input_generator(genConf,verb=self.verb)
            self.sumRec['inpGen'][dom]=copy.deepcopy(genConf)
        self.sumRec['state']='model_build'
        self.sumRec['XY_shapes']=self.inpGenD[dom].XY_shapes()
        

#............................
    def train_model(self):
        if self.verb==0: print('train silently, myRank=',self.myRank)
        hpar=self.hparams
        callbacks_list = []

        if self.useHorovod:
            # Horovod: broadcast initial variable states from rank 0 to all other processes.
            # This is necessary to ensure consistent initialization of all workers when
            # training is started with random weights or restored from a checkpoint.
            callbacks_list.append(self.hvd.callbacks.BroadcastGlobalVariablesCallback(0))
            # Note: This callback must be in the list before the ReduceLROnPlateau,
            # TensorBoard or other metrics-based callbacks.
            callbacks_list.append(self.hvd.callbacks.MetricAverageCallback())
            #Scale the learning rate `lr = 1.0` ---> `lr = 1.0 * hvd.size()` before
            # the first five epochs. See https://arxiv.org/abs/1706.02677 for details.

            mutliAgent=hpar['train_conf']['multiAgent']
            if mutliAgent['warmup_epochs']>0 :
                callbacks_list.append(self.hvd.callbacks.LearningRateWarmupCallback(warmup_epochs=mutliAgent['warmup_epochs'], verbose=self.verb))
                if self.verb: print('added LearningRateWarmupCallback(%d epochs)'%mutliAgent['warmup_epochs'])
        
        lrCb=MyLearningTracker()
        callbacks_list.append(lrCb)

        trlsCb=None
        if self.train_loss_EOE:
            print('enable  end-epoch true train-loss as callBack')
            assert self.cellName==None  # not tested
            genConf=copy.deepcopy(self.sumRec['inpGen']['train'])
            # we need much less stats for the EOE loss:
            genConf['numLocalSamples']//=8 # needs less data
            genConf['name']='EOF'+genConf['name']

            inpGen=CellSpike_input_generator(genConf,verb=1)
            trlsCb=MyEpochEndLoss(inpGen)
            callbacks_list.append(trlsCb)

        if self.checkPtOn and self.myRank==0:
            outF5w=self.outPath+'/'+self.prjName+'.weights_best.h5'
            chkPer=1
            ckpt=ModelCheckpoint(outF5w, save_best_only=True, save_weights_only=True, verbose=1, period=chkPer,monitor='val_loss')
            callbacks_list.append(ckpt)
            if self.verb: print('enabled ModelCheckpoint, save_freq=',chkPer)

        LRconf=hpar['train_conf']['LRconf']
        if LRconf['patience']>0:
            [pati,fact]=LRconf['patience'],LRconf['reduceFactor']
            redu_lr = ReduceLROnPlateau(monitor='val_loss', factor=fact, patience=pati, min_lr=0.0, verbose=self.verb,min_delta=LRconf['min_delta'])
            callbacks_list.append(redu_lr)
            if self.verb: print('enabled ReduceLROnPlateau, patience=%d, factor =%.2f'%(pati,fact))

        if self.earlyStopPatience>0:
            earlyStop=EarlyStopping(monitor='val_loss', patience=self.earlyStopPatience, verbose=self.verb, min_delta=LRconf['min_delta'])
            callbacks_list.append(earlyStop)
            if self.verb: print('enabled EarlyStopping, patience=',self.earlyStopPatience)

        #pprint(hpar)
        if self.verb: print('\nTrain_model  goalEpochs=%d'%(self.goalEpochs),'  modelDesign=', self.modelDesign,'localBS=',hpar['train_conf']['localBS'],'globBS=',hpar['train_conf']['localBS']*self.numRanks)

        fitVerb=1   # prints live:  [=========>....] - ETA: xx s 
        if self.verb==2: fitVerb=1
        if self.verb==0: fitVerb=2  # prints 1-line summary at epoch end

        if self.numRanks>1:  # change the logic
            if self.verb :
                fitVerb=2  
            else:
              fitVerb=0 # keras is silent  

        startTm = time.time()
        hir=self.model.fit(  # TF-2.1
            self.inpGenD['train'],callbacks=callbacks_list, 
            epochs=self.goalEpochs, max_queue_size=10, 
            workers=1, use_multiprocessing=False,
            shuffle=self.shuffle_data, verbose=fitVerb,
            validation_data=self.inpGenD['val']
        )
        fitTime=time.time() - startTm
        hir=hir.history
        totEpochs=len(hir['loss'])
        earlyStopOccured=totEpochs<self.goalEpochs
        
        #print('hir keys',hir.keys(),lrCb.hir)
        for obs in hir:
            rec=[ float(x) for x in hir[obs] ]
            self.train_hirD[obs].extend(rec)

        # this is a hack, 'lr' is returned by fit only when --reduceLr is used
        if 'lr' not in  hir:
            self.train_hirD['lr'].extend(lrCb.hir)

        if trlsCb: # add train-loss for Kris
            nn=len(self.train_hirD['loss'])
            self.train_hirD['train_loss']=trlsCb.hir[-nn:]

        self.train_hirD['stepTimeHist']=self.inpGenD['train'].stepTime
                    
        #report performance for the last epoch
        lossT=self.train_hirD['loss'][-1]
        lossV=self.train_hirD['val_loss'][-1]
        lossVbest=min(self.train_hirD['val_loss'])

        if self.verb:
            print('end-hpar:')
            pprint(hpar) 
            print('\n End Val Loss=%s:%.3f, best:%.3f'%( hpar['train_conf']['lossName'],lossV,lossVbest),', %d totEpochs, fit=%.1f min, earlyStop=%r'%(totEpochs,fitTime/60.,earlyStopOccured))
        self.train_sec=fitTime

        xxV=np.array(self.train_hirD['loss'][-5:])
        trainLoss_avr_last5=-1
        if xxV.shape[0]>3: trainLoss_avr_last5=xxV.mean()

        # add info to summary
        rec={}
        rec['earlyStopOccured']=int(earlyStopOccured)
        rec['fitTime_min']=fitTime/60.
        rec['totEpochs']=totEpochs
        rec['trainLoss_avr_last5']=float(trainLoss_avr_last5)
        rec['train_loss']=float(lossT)
        rec['val_loss']=float(lossV)
        rec['steps_per_epoch']=self.inpGenD['train'].__len__()
        rec['state']='model_trained'
        rec['rank']=self.myRank
        rec['num_ranks']=self.numRanks
        rec['num_open_files']=len(self.inpGenD['train'].conf['cellList'])
        self.sumRec.update(rec)

    #............................
    def save_model_full(self):
        model=self.model
        fname=self.outPath+'/'+self.prjName+'.model.h5'     
        print('save model  to',fname)
        model.save(fname)


   #............................
    def load_model_full(self,path='',verb=1):
        fname=path+'/'+self.prjName+'.model.h5'     
        print('load model from ',fname)
        self.model=load_model(fname) # creates model from HDF5
        if verb:  self.model.summary()


    #............................
    def load_weights_only(self,path='.'):
        start = time.time()
        inpF5m=path+'/'+self.prjName+'.weights_best.h5'  #_best
        print('load  weights  from',inpF5m,end='... ')
        self.model.load_weights(inpF5m) # restores weights from HDF5
        print('loaded, elaT=%.2f sec'%(time.time() - start))


