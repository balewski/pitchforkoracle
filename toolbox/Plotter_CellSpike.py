__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np

from matplotlib import cm as cmap
import matplotlib as mpl # for LogNorm()
from Plotter_Backbone import Plotter_Backbone
from Util_CellSpike import    param_by_name

#............................
#............................
#............................
class Plotter_CellSpike(Plotter_Backbone):
    def __init__(self, args,metaD):
        Plotter_Backbone.__init__(self,args)
        self.maxU=1.31
        self.metaD=metaD
        self.formatVenue=args.formatVenue

#............................
    def plot_model(self,deep,flag=0):
        #print('plot_model broken in TF=1.4, skip it'); return

        if 'cori' not in socket.gethostname(): return  # software not installed
        model=deep.model
        fname=self.outPath+'/'+deep.prjName+'_graph.svg'
        plot_model(model, to_file=fname, show_layer_names=flag>0, show_shapes=flag>1)
        print('Graph saved as ',fname,' flag=',flag)

#............................
    def stims(self,dataA,stimName,timeAxis,figId=5):
        nBin=dataA.shape[0]
        maxX=nBin ;
        xtit='time bins, step=%.3f %s'%(timeAxis['step'],timeAxis['unit'])
        binsX=np.linspace(0,maxX,nBin)

        figId=self.smart_append(figId)
        nStim=len(dataA)
        fig=self.plt.figure(figId,facecolor='white', figsize=(12,3.5))
        ax = self.plt.subplot(1,1,1)

        jst=0
        stim=dataA
        ax.plot(binsX,stim)
        ax.set(title=' stimulus:'+stimName,xlabel=xtit)
        ax.axhline(0, color='green', linestyle=':')
        ax.grid()

#............................
    def frames_vsTime(self,X,Y,nFr,figId=7,metaD=None, metaD2=None, stim=[]):
        
        if metaD==None:  metaD=self.metaD
        probeNameL=metaD['featureName']
        
        nBin=X.shape[1]
        maxX=nBin ; xtit='time bins'
        binsX=np.linspace(0,maxX,nBin)
        numProbe=X.shape[-1]
        
        assert numProbe<=len(probeNameL)
        
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(14,8))

        nFr=min(X.shape[0],nFr)
        nrow,ncol=3,3
        if nFr==1:  nrow,ncol=1,1
        print('plot input traces, numProbe',numProbe)

        yLab='ampl (mV)'
        if metaD2['voltsScale'] <100:
            yLab='ampl (a.u.)'
        j=0
        for it in range(nFr):
            #  grid is (yN,xN) - y=0 is at the top,  so dumm
            ax = self.plt.subplot(nrow, ncol, 1+j)
            j+=1
                        
            for ip in range(0,numProbe):
                amplV=X[it,:,ip]/metaD2['voltsScale']                
                ax.plot(binsX,amplV,label='%d:%s'%(ip,probeNameL[ip]))
                
            if len(stim)>0:
                ax.plot(stim*10, label='stim',color='black', linestyle='--')

            tit='id%d'%(it)
            ptxtL=[ '%.2f'%x for x in Y[it]]
            tit+=' U:'+','.join(ptxtL)
            ax.set(title=tit[:45]+'..',ylabel=yLab, xlabel=xtit)
                        
            # to zoom-in activate:
            if nFr==11:
                ax.set_xlim(4000,5000)
                ax.set_ylim(-1.6,-1.4)
            ax.grid()
            if it==0:
                ax.legend(loc='best')
 #............................
    def train_history(self,deep,figId=10):
        '''
        - - - - - - - - - - 
         ax2b  | ax1 
         - - - | - - - - - - 
          ax3b |  ax2
         - - - | - - - - - -
               |  ax3
        '''
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(9,5.5))

        nrow,ncol=4,3
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((nrow,ncol), (0,1), colspan=2, rowspan=2 )

        DL=deep.train_hirD
        nEpochs=len(DL['val_loss'])
        localBS=deep.hparams['train_conf']['localBS']
        numRanks=deep.sumRec['num_ranks']
        trainSampl=localBS*deep.inpGenD['train'].__len__()
        valSampl=localBS*deep.inpGenD['val'].__len__()

        stepTL=DL['stepTimeHist']
        delTLfr=[]; epoCntL=[]; delTLep=[];
        t0=None
        glob_epoch_size=localBS * deep.sumRec['steps_per_epoch']*numRanks
        for t, eCnt in stepTL:
            if t0==None:
                t0=t; continue
            delT_step=(t-t0)
            t0=t
            frm2sec=glob_epoch_size/delT_step/1000.
            delTLfr.append(frm2sec)
            delTLep.append(delT_step)
            epoCntL.append(eCnt)

        # shorter all vectors by 1 or 2
                
        
        ple=len(DL['val_loss'])-2
        epoCntL=epoCntL[-ple:]
        delTLep=delTLep[-ple:]

        #print('gg',eCnt,len(stepTL),'L:',len(epoCntL),len(DL['val_loss']),len(DL['loss']))

        valLoss=DL['val_loss'][-1]
        tit1='%s, train %.1f min, end-val=%.3g'%(deep.prjName,deep.train_sec/60.,valLoss)
        ax1.set(ylabel='loss = %s'%deep.hparams['train_conf']['lossName'],title=tit1,xlabel='epochs, earlyStopOccured=%d'%deep.sumRec['earlyStopOccured'])
        ax1.plot(epoCntL,DL['loss'][2:],'b:',label='train fit locN=%d'%trainSampl)
        ax1.plot(epoCntL,DL['val_loss'][2:],'r.-',label='val locN=%d'%valSampl)

        if 'train_loss' in DL:
            ax1.plot(epoCntL,DL['train_loss'][2:],'b--',label='train epochEnd')

        legTit=''
        if deep.jobId : legTit='job:'+deep.jobId
        ax1.legend(loc='best',title=legTit)
        ax1.set_yscale('log')
        ax1.grid(color='brown', linestyle='--',which='both')

        # time per frame
        ax2b  = self.plt.subplot2grid((nrow,ncol), (0,0), colspan=1, rowspan=2 )
        ax3 = self.plt.subplot2grid((nrow,ncol), (3,1), colspan=2,sharex=ax1 )
        ax3b  = self.plt.subplot2grid((nrow,ncol), (2,0), colspan=1, rowspan=1 )

        # fig .... frames/sec
        ax2b.hist(delTLfr,bins=30)
        zmu=np.array(delTLfr)
        resM=float(zmu.mean())
        resS=float(zmu.std())
        deep.sumRec['glob_frame_per_sec']=[ resM, resS ]
        numOpenFiles=deep.sumRec['num_open_files']

        tit2='gl avr:%.1f std:%.1f (k fr/sec)'%(resM,resS)
        print('CosmoFlow speed %s, global, %d epochs'%(tit2,nEpochs))
        ax2b.set(title=tit2, xlabel='global k sample/sec')
        x0=0.1
        ax2b.text(x0,0.85,'loc BS=%d'%localBS,transform=ax2b.transAxes)
        ax2b.text(x0,0.75,'loc steps=%d'%deep.sumRec['steps_per_epoch'],transform=ax2b.transAxes)
        ax2b.text(x0,0.65,'myRank=%d of %d'%(deep.sumRec['rank'],deep.sumRec['num_ranks']),transform=ax2b.transAxes)

        txt3='loc files=%d\nepoch size=%d\nepochs=%d'%(numOpenFiles,glob_epoch_size,eCnt)
        ax2b.text(x0,0.35,txt3,transform=ax2b.transAxes)
        ax2b.axvline(resM, color='k', linestyle='--')

        # fig .... time per epoch
        zmu=np.array(delTLep)
        resM=float(zmu.mean())
        resS=float(zmu.std())

        ax3.plot(epoCntL,delTLep,'.-',c='g',label='epoch duration')
        ax3.grid(color='brown', linestyle='--',which='both')
        ax3.set(ylabel='(sec)')
        ax3.legend(loc='best')
        ax3.set_ylim(0,)
        ax3.axhline(resM, color='green', linestyle=':')

        # fig ....
        ax3b.hist(delTLep,facecolor='g',bins=30)
        deep.sumRec['glob_epoch_duration_sec']=[ resM, resS ]

        tit3=' avr epoch:%.2f std:%.2f (sec)'%(resM,resS)
        ax3b.set(title=tit3, xlabel='sec/epoch')
        ax3b.axvline(resM, color='green', linestyle=':')

        if 'lr' not in DL: return

        ax2 = self.plt.subplot2grid((nrow,ncol), (2,1), colspan=2,sharex=ax1 )
        # fig ....
        ax2.plot(epoCntL,DL['lr'][2:],'.-',label='learning rate')
        ax2.grid(color='brown', linestyle='--',which='both')
        ax2.set(title='model design='+deep.sumRec['modelDesign'])
        ax2.set_yscale('log')
        ax2.legend(loc='best')

#............................
    def fparam_residua_summary(self,ax,sumRec,figId=15):
        solo=False
        if ax==None:
            figId=self.smart_append(figId)
            fig=self.plt.figure(figId,facecolor='white', figsize=(8,8))
            ax = self.plt.subplot(1,1,1)
            solo=True

        lossAudit=sumRec['lossAudit']

        parNameL=[ x[0] for x in lossAudit]
        morphIdxL,morphArmL=param_by_name(parNameL)
        morphNameL=[ parNameL[i] for i in morphIdxL]

        numPar=len(lossAudit)
        lossThrHi=sumRec['lossThrHi']
        lossThrLo=sumRec['lossThrLo']
        print('plot summary for %d params, lossThrHi=%.2f'%(numPar,lossThrHi))
        binsX= np.arange(numPar)
        yV=[ 0. for x in lossAudit]
        yE=[ lossAudit[i][2] for i in   morphIdxL]

        nHi=0; nLo=0; nMi=0
        for e in yE:
            if e > lossThrHi: nHi+=1
            elif e>0.01 :
                if  e < lossThrLo:
                    nLo+=1
                else:
                    nMi+=1

        ax.errorbar(binsX, yV, yerr=yE, lolims=True,c='g')
        ax.axhline(lossThrLo, color='red', linestyle='--',lw=0.7)
        ax.axhline(lossThrHi, color='red', linestyle='--',lw=0.7)
        txt1='%s'%(sumRec['full_name'])
        if solo : txt1+=', featureType=%s, numFeature=%d'%(sumRec['featureType'],sumRec['numFeature'])

        ax.set(title=txt1, ylabel='residue RMS, '+sumRec['short_name'])
        if not solo :
            txt2='paramID, cnt: %d<%d<%d'%(nLo, nMi, nHi )
            ax.set_xlabel(txt2)
        #ax.set_ylim(0.03,.65)
        yHi=0.5
        for name,idx in morphArmL:
            ax.axvline(idx-0.4,c='b',linestyle='--',lw=0.7)
            ax.text(idx-0.4,yHi*0.8,name,rotation=70, color='b')

        if not solo: return

        #ax.grid()
        txt3=' %d params RMS < %.2f'%(nLo, lossThrLo)
        ax.text(0,lossThrLo-0.02,txt3,color='r')

        txt3=' %d params RMS in [ %.2f, %.2f] '%(nMi, lossThrLo, lossThrHi)
        ax.text(0,lossThrLo+0.1,txt3,color='r')
        txt3=' %d params RMS > %.2f'%(nHi, lossThrHi)
        ax.text(0,lossThrHi+0.02,txt3,color='r')

        dom=sumRec['domain']
        txt3='dom=%s  numSamp=%d  numFeat=%d  %s-loss=%.3e'%(dom,sumRec['numSamples'],sumRec['numFeature'],dom,sumRec[dom+'LossMSE'])
        ax.text(0.3,0.7,txt3,transform=ax.transAxes,color='m')

        # We want to show all ticks...
        ax.set_xticks(np.arange(numPar))
        ax.set_xticklabels(morphNameL,rotation=80)

#............................
    def fparam_residua_2D(self,U,Z,sumRec, do2DRes=False, figId=9):
        #print('vv',self.formatVenue)
        #colMap=cmap.rainbow
        assert U.shape[1]==Z.shape[1]
        colMap=cmap.GnBu

        parName=self.metaD['parName']
        nPar=self.metaD['numPar']

        nrow,ncol=4,5

        if  self.formatVenue=='poster':
            # grant August-2020
            colMap=cmap.Greys
            figId+=100


        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(14,9.))

        fig, axs = self.plt.subplots(nrow,ncol, sharex='col', sharey='row',
                                     gridspec_kw={'hspace': 0.3, 'wspace': 0.1},num=figId)

        axs=axs.flatten()
        j=0

        for iPar in range(0,nPar):
            ''' not needed
            if 'fixed' in parName[iPar]: continue
            if 'const' in parName[iPar]: continue
            '''

            ax1=axs[j]; j+=1
            ax1.set_aspect(1.0)
                        
            u=U[:,iPar]
            z=Z[:,iPar]

            mm2=self.maxU
            mm1=-mm2
            mm3=self.maxU/3.  # adjust here of you want narrow range for 1D residua
            binsX=np.linspace(mm1,mm2,30)

            zsum,xbins,ybins,img = ax1.hist2d(z,u,bins=binsX,#norm=mpl.colors.LogNorm(),
                                               cmin=1, cmap = colMap)

            ax1.plot([0, 1], [0,1], color='magenta', linestyle='--',linewidth=0.5,transform=ax1.transAxes) #diagonal
            # 
            ax1.set_title('%d:%s'%(iPar,parName[iPar]), size=10)

            if  self.formatVenue=='poster': continue

            # more details  will make plot more crowded
            self.plt.colorbar(img, ax=ax1)

            # .... compute residua per parameter
            umz=u-z
            resM=umz.mean()
            resS=umz.std()

            # additional info Roy+Kris did not wanted to see for nicer look
            if j>(nrow-1)*ncol: ax1.set_xlabel('pred (a.u.)')
            if j%ncol==1: ax1.set_ylabel('truth (a.u.)')

            ax1.text(0.4,0.03,'avr=%.3f\nstd=%.3f'%(resM,resS),transform=ax1.transAxes)
            #print('aa z, u, umz, umz-z',parName[iPar],z.mean(),u.mean(),umz.mean(),z.mean()-umz.mean())

            if resS > sumRec['lossThrHi']:
                ax1.text(0.1,0.7,'BAD',color='red',transform=ax1.transAxes)
            if resS < 0.01:
                ax1.text(0.1,0.6,'CONST',color='sienna',transform=ax1.transAxes)

            dom=sumRec['domain']
            tit1='dom='+dom
            tit2='MSEloss=%.3g'%sumRec[dom+'LossMSE']
            tit3='feat:'+str(sumRec['genConf']['numFeature'])
            
            yy=0.90; xx=0.04
            if j==0: ax1.text(xx,yy,tit1,transform=ax1.transAxes)
            if j==1: ax1.text(xx,yy,'nSampl=%d'%(u.shape[0]),transform=ax1.transAxes)
            if j==2: ax1.text(xx,yy,tit2,transform=ax1.transAxes)
            if j==3: ax1.text(xx,yy,'short:'+sumRec['short_name'],transform=ax1.transAxes)

            if j==6: ax1.text(0.2,yy,tit3,transform=ax1.transAxes)
            
        # more info in not used pannel
        dataTxt='\npredData:'+','.join(sumRec['predDataList'])
        txt3='name:%s\ndesign:%s'%(sumRec['full_name'],sumRec['modelDesign'])+dataTxt
        txt3+='\nMSEloss=%.3g'%sumRec[sumRec['domain']+'LossMSE']
        txt3+='\nfeat:'+str(sumRec['genConf']['numFeature'])+',  nSampl=%d'%(u.shape[0])
        ax1=axs[j]
        ax1.text(0.02,0.2,txt3,transform=ax1.transAxes)


          
#............................
    def fparams_corr(self,U,tit,figId=6,tit2='', metaD=None):

        if metaD==None:  metaD=self.metaD
        parName=metaD['parName']
        nPar=metaD['numPar']
        nrow,ncol=3,6

        nPar=12
        print('qq',U.shape, nPar)        

        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(2.7*ncol,2.2*nrow))
        unitStr=' (a.u.)'
        mm2=mm=self.maxU
        mm1=-mm2
        binsX= np.linspace(mm1,mm2,50)

        j=1
        for i in range(0,nPar,2):
            iPar=min(i,U.shape[1]-2) # account for odd num of params
            y1=U[:,iPar]
            y2=U[:,iPar+1]
            ax=self.plt.subplot(nrow,ncol,j)
            j+=1
            zsum,xbins,ybins,img = ax.hist2d(y1,y2,bins=binsX, cmin=1,
                                   cmap = cmap.rainbow)
            self.plt.colorbar(img, ax=ax)
            ax.set(title=tit+unitStr, xlabel='%d: %s'%(iPar,parName[iPar]), ylabel='%d: %s'%(iPar+1,parName[iPar+1]))
            ax.grid()
            if i==0: ax.text(0.1,0.95,tit2,transform=ax.transAxes)
            if self.maxU >1.3:
                rect1 = self.plt.Rectangle((-1,-1),2,2,fill=False,linestyle='--',edgecolor='r')
                ax.add_patch(rect1)

        for iPar in range(0,nPar):
            z=U[:,iPar]
            ax=self.plt.subplot(nrow,ncol,j) ; j+=1
            binsY=np.linspace(mm1,mm2,50)
            ax.hist(z,binsY)
            ax.set(title='%d: %s'%(iPar,parName[iPar]),ylabel='traces')
            for x in [-1.,1]:
                ax.axvline(x, color='yellow', linestyle='--')
            ax.grid()
            if iPar==0:
                ax.text(0.1,0.85,tit2,transform=ax.transAxes)
            if iPar==1:
                ax.text(0.1,0.85,'n=%d'%z.shape[0],transform=ax.transAxes)

        #self.plt.subplots_adjust(wspace = 0.2 )

#............................
    def params1D(self,P,tit,figId=4,tit2='',isPhys=False):
        #fix_phys_range_handling_iced_params
        metaD=self.metaD
        parName=metaD['parName']
        nPar=metaD['numPar']
        rangeLD=metaD['physRange']
        nrow,ncol=4,8
        
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(2.7*ncol,2.*nrow))
        
        j=1
        for i in range(0,nPar):
            
            if isPhys:
                range12=rangeLD[i]
                #print('\n',i,rangeLD[i][0],parName[i],range12,j)
                base=np.sqrt(range12[1]*range12[2])
                lgBase=np.log10(base)
                mm2=lgBase+1.1
                mm1=lgBase-1.1
                #print('www4',base,lgBase,mm1,mm2)
                p=np.log10(P[:,i])
                unitStr='log10(phys)'
                hcol='g'
            else:
                mm2=1.2; mm1=-mm2
                p=P[:,i]
                unitStr='unit'
                hcol='b'
            binsX= np.linspace(mm1,mm2,30)            

            ax=self.plt.subplot(nrow,ncol,j)
            j+=1
            binsY=np.linspace(mm1,mm2,50)
            ax.hist(p,binsX,color=hcol)
            ax.set(title='par %d'%(i),ylabel='frames',xlabel=unitStr)
            ax.text(0.02,0.5,parName[i],rotation=20, fontsize=10,transform=ax.transAxes)
            
            for x in [mm1,mm2]:
                ax.axvline(x, color='yellow', linestyle='--')
            ax.grid()
            if i==0:
                ax.text(0.1,0.85,tit2,transform=ax.transAxes)
            if i==1:
                ax.text(0.1,0.85,'n=%d'%p.shape[0],transform=ax.transAxes)

