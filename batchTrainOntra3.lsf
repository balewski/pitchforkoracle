#!/bin/bash
#-BSUB -nnodes 11
#BSUB -W 2:00
#BSUB -nnodes 1
#-BSUB -W 30
#BSUB -P NRO106
#BSUB -alloc_flags "smt4 nvme"
#BSUB -J lsf-pitch[1]
#BSUB -q batch

# INFO: current setup for per-cell training , cell name is passed via env: MYCELL=bbp198 bsub -J 'lsf-pitch[1-2]' batchTrainOntra3.lsf 


arrIdx=${LSB_JOBINDEX}
jobId=${LSB_JOBID}_${LSB_JOBINDEX}
nprocspn=6

runTimeMnt=110   # 2-h training
#runTimeMnt=10 ; pilotEpochs=7  # debug mode

#design=smt190927_b_ontra3 # found for bbp019 and manually upgraded
design=a2f791f3a_ontra3 # found in HpoSetD for Ontra3
designLib=./

doHPO=0

# all Ontra3 samples sizes are for -N11
#localSamples=30000 ; pilotEpochs=30 # total=2M samples, 1/40 of Ontra3
#localSamples=60000 ; pilotEpochs=30 # total=4M samples, 1/20 of Ontra3
#localSamples=120000 ; pilotEpochs=30 # total=8M samples, 1/10 of Ontra3
#localSamples=240000 ; pilotEpochs=20 # total=16M samples, 1/5 of Ontra3
#localSamples=300000 ; pilotEpochs=30 # total=20M samples, 1/4 of Ontra3
#localSamples=600000 ; pilotEpochs=20  # total=40M samples, 1/2 of Ontra3 
#localSamples=1200000  ; pilotEpochs=12  # total=80M samples, all  Ontra3 
#--- ONE_CELL samples sizes are for -N1
localSamples=83000 ; pilotEpochs=20 # total=100%+

cellName=$MYCELL
probeType=8inhib157cycleF_3prB8kHz
dataPath=/gpfs/alpine/nro106/proj-shared/neuronBBP-pack8kHz/probe_3prB8kHz/ontra3/etype_8inhib_v1
#wrkDir0="/gpfs/alpine/world-shared/nro106/balewski/cycleOntra3/
#wrkDir=$wrkDir0/$LSB_JOBID/$LSB_JOBINDEX

wrkDir0="/gpfs/alpine/world-shared/nro106/balewski/perCellOntra3/"
wrkDir=$wrkDir0/$cellName/$LSB_JOBINDEX

if [ $doHPO -gt 0 ]; then
    #....... select design from the list  START
    taskList=hpoSetD.list
    designLib=hpoSetD
    maxTask=`cat $taskList |wc -l`
    echo M:doHPO maxTask=$maxTask arrIdx=$arrIdx taskList=$taskList
    # count from 1 to M
    if [ $arrIdx -gt $maxTask ]; then
	echo "M:arrIdx $arrIdx above maxTask=$maxTask, exit"; exit 0
    fi
    line=`head -n $[ $arrIdx ] $taskList |tail -n 1`
    echo M: arrIdx=$arrIdx line=$line
    design=`echo $line | cut -f3 -d\ `
    #....... select design from the list  END
fi

echo M:  design=$design  max runTimeMnt=$runTimeMnt

# load modules
module load  ibm-wml-ce
date
echo "my job array JID="$LSB_JOBID" arrIdx="$arrIdx" host0="`hostname`
python -V
pwd

#determine number of nodes and total procs
nnodes=$(cat ${LSB_DJOB_HOSTFILE} | sort | uniq | grep -v login | grep -v batch | wc -l)
nprocs=$(( ${nnodes} * ${nprocspn} ))
echo 'M: nprocs='$nprocs

#run
cat ${LSB_DJOB_HOSTFILE} | sort | uniq | grep -v login | grep -v batch > host_list

codeList="*.py  toolbox/ batchTrainOntra3.lsf driveTrainSummit.sh  ${designLib}/hpar*${design}*.yaml  "


outPath=$wrkDir/out
mkdir -p $outPath
cp -rp $codeList  $wrkDir
cd  $wrkDir
echo lsfPWD=`pwd` 

echo "starting  jobId=$jobId pitchfork2021-01 " `date` " outPath="$outPath
echo final  design=$design

#- - - - - - - - - -
function guessEpochs {
    epochs=$1
    runTimeMinutes=$2
    echo CellSpike speed  estimator  runTimeMinutes=$runTimeMinutes
    sumF1=out/cellSpike.sum_train.yaml
    ls -l $sumF1
    pilotRT=`grep fitTime_min $sumF1 | cut  -f2 -d:`
    goalEpochs=`echo "print( int($runTimeMinutes / $pilotRT* $epochs - $epochs) )" | python3`
    avrEndLoss=`grep trainLoss_avr_last5 $sumF1 | cut  -f2 -d:`
    echo F:avrEndLoss=$avrEndLoss
    echo "print(\"F:pilotRT=%.1f min, avrEndLoss=%.2e, max goalEpochs=%d maxTrainTime=%.1f min\"%($pilotRT,$avrEndLoss,$goalEpochs,$runTimeMinutes))"  | python3
    goalEpochs=`echo "print( ($goalEpochs)*($avrEndLoss <0.1 ) )" | python3`
    echo Fend:goalEpochs=$goalEpochs
}
# - - - - - end function

mainCmd="  python -u ./train_CellSpike.py --noXterm  --localSamples $localSamples  --design $design --dataPath $dataPath --probeType $probeType --cellName $cellName "
pilotCmd="  --jobId ${jobId}_pilot  --earlyStop 0  --epochs $pilotEpochs "

echo pilot training for epochs=$pilotEpochs on numGpuPerNode=${nprocspn} on ${nnodes} nodes 

jsrun  -n ${nnodes} -g 6 -c 42 -a ${nprocspn} --bind=proportional-packed:7 --launch_distribution=packed stdbuf -o0  ./driveTrainSummit.sh " $mainCmd  $pilotCmd "  >& log.pilot

echo "pilot completed "`date`
guessEpochs $pilotEpochs $runTimeMnt

if [ $goalEpochs -le 0 ]; then
    echo M: abort training due to too large end loss
    exit 0
fi

mv out/cellSpike_train_f10.png out/cellSpike_pilot_f10.png
mv out/cellSpike.sum_train.yaml  out/cellSpike.sum_pilot.yaml

sleep 3


echo main training for epochs=$goalEpochs 
pilotCmd="  --jobId ${jobId}_train  --epochs $goalEpochs "

#
jsrun  -n ${nnodes} -g 6 -c 42 -a ${nprocspn} --bind=proportional-packed:7 --launch_distribution=packed stdbuf -o0  ./driveTrainSummit.sh   " $mainCmd  $pilotCmd "  >& log.train
echo "train completed "`date`
sleep 3
ls -l out/*h5

echo "start predict at "`pwd`" design=" $design
ls -l out/cellSpike.model.h5

sleep 10
export HDF5_USE_FILE_LOCKING=FALSE # to avoid file locking for some hd5 libs

# CPU prediction:
#Count GPS
jsrun  -n 1 -g 1 -c 42 -a 1 --bind=proportional-packed:7 --launch_distribution=packed stdbuf -o0 nvidia-smi >&log.numGpu
# GPU prediction
jsrun  -n 1 -g 1 -c 42 -a 1 --bind=proportional-packed:7 --launch_distribution=packed stdbuf -o0 python ./predict_CellSpike.py  --dataPath $dataPath  --probeType $probeType  --cellName $cellName  --events 50000   --noXterm  >&log.pred
# attic :   --venue poster multi-cell:  --events 200000
echo predict-done-`date`

echo "finished pitchFork-2021" `date`
# attic :   --venue poster $LSB_OUTPUTFILE

# notes for job array
# bsub -J 'lsf-pitch[11-14]' batchTrainOntra3.lsf
# bkill 520324[11]
# bjobs
