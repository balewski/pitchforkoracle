#!/usr/bin/env python
import h5py
import matplotlib.pyplot as plt
from matplotlib import cm as cmap
import numpy as np
import sys
import ruamel.yaml  as yaml


print(' args', len(sys.argv) , sys.argv)
if len(sys.argv)!=2 : 
    print('provide input YAML name, aborting'); exit(1)
h5N=sys.argv[1]

print('Opening ',h5N)

#varParL=[ 'skip%d'%i for i in range(23) ]
#for i in [0,5,6,8,9,10,11,14,16,19]:  varParL[i]='P%d'%i
#varParL=[ 'P%d'%i for i in range(23) ]

varParL=yaml.load('[gIhbar_Ih_basal, gNaTs2_tbar_NaTs2_t_apical, gSKv3_1bar_SKv3_1_apical, skip4, gImbar_Im_apical, gNaTa_tbar_NaTa_t, gK_Tstbar_K_Tst, skip8, gNap_Et2bar_Nap_Et2, gSK_E2bar_SK_E2_axonal, gCa_HVAbar_Ca_HVA,gK_Pstbar_K_Pst, gSKv3_1bar_SKv3_1_axonal , skip14, gCa_LVAstbar_Ca_LVAst, skip16, gSKv3_1bar_SKv3_1, gSK_E2bar_SK_E2_somatic, gCa_HVAbar_Ca_HVA_somatic, gNaTs2_tbar_NaTs2_t, gIhbar_Ih_somatic, skip22, gCa_LVAstbar_Ca_LVAst_somatic]')

probeNameL=yaml.load(' [soma, axon_0_0.5, axon_1_0, axon_1_1, apic_66_0.5, apic_79_0.5, dend_16_0.5]')

# repack varied param list
parNameIdx=[]
parNameL=[]
for i in range(len(varParL)):
    name=varParL[i]
    print(i,name)
    if 'skip' in name: continue
    parNameIdx.append( (name,i))
    parNameL.append(name)

numPar=len(parNameIdx)

print("Roy's hd5 path=",h5N,' numPar=',len(parNameL))
print("parList",parNameIdx)


# change filepath if neessary
h5f = h5py.File(h5N, 'r')
print('see keys number:',len(h5f))
for x in h5f.keys():
  print('key=',x, end=' ')
  y=h5f[x]
  print(', size:',y.shape)

voltAr=h5f['voltages']
physRange=np.array(h5f['phys_par_range'])

nTrace,nBins,numProbe=voltAr.shape
print('num frames=', nTrace,' numProbe=',numProbe)

#print('e.g. uPar:',h5f['norm_par'][0,:])
#print('Param ranges:',np.array(h5f['phys_par_range']))

# repack params to keep only varied ones

listU=[]; listP=[]
parNameL=[]

for (name,idx) in parNameIdx:
    print('set',name,idx,' physRange:',physRange[idx,:])
    #print('qq',name,idx)    
    listU.append(np.array(h5f['norm_par'][:,idx]))
    listP.append(np.array(h5f['phys_par'][:,idx]))
    parNameL.append(name)

assert len(listU) >0
parAU=np.stack(tuple(listU),axis=-1 )
parAP=np.stack(tuple(listP),axis=-1 )
print('parAU shape',parAU.shape)


qaS=np.array(h5f['binQA'])
print('agregated data:',qaS.shape,voltAr.shape)
print('ex QA',qaS[:10])
#  0=bad, 1=good , use only QA for soma
nBad=np.sum(qaS==0)
print('qaS shape:',qaS.shape, ' nBad=',nBad)

stim=h5f['stim']

nPar=parAP.shape[1]
nGood=qaS.shape[0]-nBad
print('nBins=',nBins,' nPar=',nPar, parNameL,'\n QA: good=%d bad=%d, B/G=%.2f'%(nGood, nBad,nBad/nGood))

cnt={'bad':0, 'inp':0}
parU=[]; voltB=[]; qaB=[]; parP=[]
itL=range(nTrace)

for i in itL:
    #print(i, qaA[i])
    qa = qaS[i]
    if len(parU)%1800==0: print('i=',i,' qa=',qa,'parU:',parAU[i])
    cnt['inp']+=1
    if qa==0:
        cnt['bad']+=1
        continue
    parP.append(parAP[i])
    parU.append(parAU[i])
    voltB.append(voltAr[i])
    qaB.append(qaS[i])

print('QA stats',cnt)

nTrace=len(voltB)
print('accepted frames:%d of %d'%(len(parP),i))

parU=np.array(parU)
parP=np.array(parP)
stim=np.array(stim)

voltB=np.array(voltB)
#print('voltB shape',voltB.shape, voltB.dtype)
#voltsScale=np.array([ 45.,30,30,26,26,38,38])
#voltsBias=np.array([ 0, 5., 10 , 15, 20, 25, 30])

#voltsBias=np.array( [ 0, 0, 0, 0, 0, 0, 0 ])
#voltsScale=np.array( [ 1, 1, 1, 1, 1, 1, 1])
#print('zzz',voltsBias.shape, voltsScale.shape, voltsScale.dtype)

#voltB=np.divide(voltB+voltsBias,voltsScale)

# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# plotter # select few random traces
plt.figure(facecolor='white', figsize=(13,10))
nrow,ncol=3,2
#nrow,ncol=2,2
j=0

for _ in range(50):  
    i=np.random.randint(nTrace)    
    volt = voltB[i] #-voltB[0]
    #print('vvv',volt.shape)
    
    id = i
    qa = int(qaB[i])
    paru=parU[i]
    parp=parP[i]

    ax = plt.subplot(nrow, ncol, 1+j)
    #print('i=',i,pars,id,parp)
    j+=1
    
    for i in range(numProbe):
        ax.plot(volt[:,i],label=probeNameL[i])
    #ax.plot(stim*5, label='stim*5',color='black', linestyle='--')
    ax.grid()
    if j<2:
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)

    parTxt=[ '%.2f, '%x for x in paru]    
    tit="U:%s"%(''.join(parTxt))

    ax.set(title=tit[:40],xlabel='time bin,  QA:%d'%qa, ylabel='trace:%d'%id)
    #ax.set_xlim(0,16000) ;
    #ax.axvline(5500, color='black', linestyle='--')  
    #ax.axvline(14500, color='black', linestyle='--') 
    #break
    if j>=ncol*nrow : break

plt.tight_layout()


#BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
# plotter, correaltion between params

nrow,ncol=3,nPar

nTop=nPar
if nPar>5 :
    nTop=int(nPar/2.+0.51)
    nrow,ncol=3*2,nTop
plt.figure(facecolor='white', figsize=(ncol*2.2,nrow*1.8))
print('BBB', nrow,ncol,nTop)   

mm=1.1
binsX= np.linspace(-mm,mm,30)

for j in range(nPar):
    jOff=0
    if j>=nTop : jOff=2*nTop
    #print('j',j,jOff, 1+j+jOff, 1+j+ncol+jOff, 1+j+2*ncol+jOff)
    ax = plt.subplot(nrow, ncol, 1+j+jOff)
    ax.hist(parU[:,j],bins=50)

    ax = plt.subplot(nrow, ncol, 1+j+ncol+jOff)
    ax.scatter(parU[:,j],parP[:,j])
    ax.set(title=parNameL[j],xlabel='Upar-%d'%j,ylabel='phys value')

    ax = plt.subplot(nrow, ncol, 1+j+2*ncol+jOff)
    j1=(j+1)%nPar
    #ax.scatter(parU[:,j],parU[:,j1])
    y1=parU[:,j]
    y2=parU[:,j1]
    zsum,xbins,ybins,img = ax.hist2d(y1,y2,bins=binsX, cmin=1,
                                   cmap = cmap.rainbow)    

    plt.colorbar(img, ax=ax)

    ax.set(title='corr Upar',xlabel='Upar-%d'%j,ylabel='Upar-%d'%j1)

    print('Ppar%d  %s physRange: %.3g , %.3g '%(j,parNameL[j],min(parP[:,j]),max(parP[:,j])),end='')
    print(',Urange: %.3g , %.3g'%(min(parU[:,j]),max(parU[:,j])))


plt.tight_layout()
plt.show()
