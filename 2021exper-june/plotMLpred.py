#!/usr/bin/env python3
'''
inspect ML predictions for an experiment

'''
import sys,os
sys.path.append(os.path.abspath("../"))
from toolbox.Util_H5io3 import  write3_data_hdf5, read3_data_hdf5

from toolbox.Plotter_Backbone import Plotter_Backbone
from pprint import pprint
import json
import copy

import numpy as np
import argparse

#...!...!..................
def get_arm_color(parName):
    armCol={'api':'C2', 'axn':'C3','som':'C4','den':'C5'}
    arm=parName.split('.')[-1]
    hcol=armCol[arm]
    return hcol



#...!...!..................
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")

    parser.add_argument("-d", "--dataPath",  default='mlPred/',help="predicted data location")

    parser.add_argument("--dataName",  default='210611_3_NI-a0.17_ab23cd', help="[.mlPred.h5] data file name ")

    parser.add_argument("-o","--outPath", default='out/',help="output path for plots and tables")

 
    args = parser.parse_args()
    args.formatVenue='prod'
    args.prjName='mlPred'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    if not os.path.isdir(args.outPath):  os.makedirs(args.outPath)
    return args


#...!...!..................
def aggregate_sweeps(Z,sweeps,wallT):
    idLast=-1
    zL=[]
    avrZ=[]; stdZ=[]; tm=[]
    def avr_and_remember():
        if len(zL)<=1: return
        zV=np.array(zL)
        #print('aa',np.mean(zV,axis=0),zV.shape,t0); ok11
        avrZ.append(np.mean(zV,axis=0))
        stdZ.append(np.std(zV,axis=0))
        tm.append(t0)
        
    for z,swid,t in zip(Z,sweeps,wallT):
        swa=int(swid)
        if swa==idLast: # just increment the list
            zL.append(z);  continue
        avr_and_remember()
        # start next
        t0=t
        zL=[z]
        idLast=swa
    # capture the last chunk of data
    avr_and_remember()

    '''
    i=0
    #... print result
    for a,b,c in zip(avrZ,stdZ,tm):
        print('avr[%d] %.2f +/- %.2f  %.2f (min)'%(i,a[i],b[i],c))
    '''
    
    return np.array(avrZ),np.array(stdZ),np.array(tm)

#...!...!..................
def unpack_ml_data(bigD):
    Z=bigD['pred_upar']
    sweepTA=bigD['sweep_trait']
    wallT=[]
    swIdL=[]
    for rec in sweepTA:
        sweepId, sweepTime, serialRes=rec
        wallT.append(sweepTime/60.)  # time in min
        swIdL.append(sweepId)
    wallT=np.array(wallT)  

    return Z,swIdL,wallT

#...!...!..................
def print_mlPred_summary(bigD,predMD):
    print('predMD keys:',list(predMD))
    expMD=predMD['exper_info']
    pprint(expMD)
    sweepTA=bigD['sweep_trait']
    Z=bigD['pred_upar']
    ampl=expMD['stimAmpl']
    nSweep=expMD['numSweep']
    print('%d sweeps details ampl=%.2f'%(nSweep,ampl))
    for iw in range(nSweep):
        sweepId, sweepTime, serialRes=sweepTA[iw]
        print(iw,sweepId, sweepTime, serialRes,Z[iw][:4])
        
 
#............................
#............................
#............................
class Plotter(Plotter_Backbone):
    def __init__(self,args,trainMD):
        Plotter_Backbone.__init__(self,args)
        self.trainMD=trainMD
        
#...!...!..................
    def params_vs_expTime(self,P,wallT,tit0,figId=4):  # only for experimental data       
        simMD=self.trainMD['input_meta']
        parName=simMD['parName']
        nPar=simMD['numPar']
        nrow,ncol=3,5
        
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(2.6*ncol,2.2*nrow))
        for i in range(0,nPar):            
            ax=self.plt.subplot(nrow,ncol,i+1)
            uval=P[:,i]
            hcol=get_arm_color(parName[i])
            ax.plot(uval,wallT,'*-',color=hcol)            
            ax.set(title=parName[i], xlabel='pred Upar %d'%(i),ylabel='wall time (min)')                       
            for x in [-1.,1.]:  ax.axvline(x, color='C2', linestyle='--')
            ax.grid()
            
            yy=0.9
            if i==0:  ax.text(-0.05,yy,tit0,transform=ax.transAxes, color='r')
            if i==1:  ax.text(0.05,yy,'n=%d'%uval.shape[0],transform=ax.transAxes, color='r')

        
#...!...!..................
    def avrParams_vs_expTime(self,avrZ,stdZ,wallT1,tit0,figId=4):  # only for experimental data       
        simMD=self.trainMD['input_meta']
        parName=simMD['parName']
        nPar=simMD['numPar']
        nrow,ncol=4,4

        #print('ee',avrZ.shape);ok11
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(2.6*ncol,2.2*nrow))
        for i in range(0,nPar):            
            ax=self.plt.subplot(nrow,ncol,i+1)
            #uval=P[:,i]
            hcol=get_arm_color(parName[i])
            #print(avrZ[i],wallT1)
            ax.errorbar(avrZ[:,i],wallT1,xerr=stdZ[:,i],color=hcol, fmt='o-')
            ax.set_title(parName[i])
            for x in [-1.,1.]:  ax.axvline(x, color='C2', linestyle='--')
            ax.grid()
            
            yy=0.9
            if i==0:  ax.text(-0.05,yy,tit0,transform=ax.transAxes, color='r')
            if i==1:  ax.text(0.05,yy,'n=%d'%avrZ.shape[0],transform=ax.transAxes, color='r')
            ax.set_xlabel('avr pred Upar %d'%(i))
            if i%ncol==0: ax.set_ylabel('wall time (min)')                       
            ax.set_xlim(-1.2,1.2)
        
#...!...!..................
    def Ystims(self,stims,plDD,presc=1,figId=5):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(16,8))
        ax = self.plt.subplot(1,1,1)
        pprint(plDD)
        timeV=plDD['timeV']
        ampls=plDD['stimAmpl']
        N=stims.shape[0]
        for n in range(0,N,presc):
            hexc=self.cL10[ n%10]
            ax.plot(timeV,stims[n], color=hexc, label='%.2f (FS)'%ampls[n],linewidth=0.7)

        ax.legend(loc='best',title='stim ampl')
        tit=plDD['shortName']
        xLab='time '+plDD['units']['time']
        yLab='stim '+plDD['units']['stimAmpl']
        ax.set(title=tit,xlabel=xLab,ylabel='stim (nA)')
        ax.grid()
        if 'zoom_ms' in plDD:
            [a,b]=plDD['zoom_ms']
            ax.set_xlim(a,b)

#...!...!..................
    def Ywaveform(self,waves,plDD,presc=1,tit2='',figId=5):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(16,8))
        ax = self.plt.subplot(1,1,1)

        timeV=plDD['timeV']
        ampls=plDD['stimAmpl']
        N=waves.shape[0]

        for n in range(0,N,presc):
            hexc=self.cL10[ n%10] 
            ax.plot(timeV,waves[n], color=hexc, label='ampl=%.2f'%ampls[n],linewidth=0.7)

        ax.legend(loc='best')
        tit=plDD['shortName']+tit2
        xLab='time '+plDD['units']['time']
        yLab='AP '+plDD['units']['waveform']
        ax.set(title=tit,xlabel=xLab,ylabel=yLab)
        ax.grid()
        if 'timeLR' in plDD:  ax.set_xlim(tuple(plDD['timeLR']))
        if 'amplLR' in plDD: ax.set_ylim(tuple(plDD['amplLR']))

        
#...!...!..................
    def Xsurvey_exp(self,bigD,plDD,figId=6):
        figId=self.smart_append(figId)
        nrow,ncol=2,2
        fig=self.plt.figure(figId,facecolor='white', figsize=(10,6))

        sweepC=bigD['sweepCnt']
        traitA=bigD['sweepTrait']
        N=sweepC.shape[0]
        
        # repack data for plotting
        stimA=[]; swTimeA=[]; resA=[]; swIdA=[]

        for ia in range(N): # loop over stmAmpl
            k=sweepC[ia]
            traits=traitA[ia,:k]
            stimAmpl=plDD['stimAmpl'][ia]
            #print('ia=%2d stimAmpl=%.2f numSweep=%d'%(ia,stimAmpl,k))
            for rec in traits:
                sweepId, sweepTime, serialRes=rec
                swTimeA.append(sweepTime)
                stimA.append(stimAmpl)
                resA.append(serialRes)
                swIdA.append(sweepId)               
                
        wallTA=np.array(swTimeA)/60.
        stimA=np.array(stimA)
        wtIdx = np.argsort(wallTA) # needed to sort data by wall time
        #print('ii',wtIdx)
        ax = self.plt.subplot(nrow,ncol,1)
        #ax.scatter(wallTA,stimA, alpha=0.6)
        ax.plot(wallTA[wtIdx],stimA[wtIdx],'*-', alpha=0.6)
        ax.set(xlabel='wall time (min)',ylabel='stim ampl (FS)')
        ax.text(0.01,0.94,plDD['shortName'],transform=ax.transAxes,color='m')
        ax.grid()

        ax = self.plt.subplot(nrow,ncol,2)
        ax.scatter(wallTA,resA, alpha=0.6)
        ax.set(xlabel='wall time (min)',ylabel='serial resistance (MOhm)')
        ax = self.plt.subplot(nrow,ncol,3)
        ax.scatter(wallTA,swIdA, alpha=0.6)
        ax.set(xlabel='wall time (min)',ylabel='routine ID')


#...!...!..................
    def Xsurvey_exp(self,bigD,plDD,figId=6):
        aa=1
#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":

    args=get_parser()

    outF='%s.mlPred.h5'%(args.dataName)
    bigD,predMD=read3_data_hdf5(args.dataPath+outF)
    print_mlPred_summary(bigD,predMD)
    #pprint(expMD)

    Z,sweeps,wallT=unpack_ml_data(bigD)
    avrZ,stdZ,wallT1=aggregate_sweeps(Z,sweeps,wallT)

        
    tit0=predMD['short_name'][:27]
    # - - - - - PLOTTER - - - - -

    plot=Plotter(args,predMD['train_info'])
    plot.params_vs_expTime(Z,wallT,tit0, figId=8)
    plot.avrParams_vs_expTime(avrZ,stdZ,wallT1,tit0, figId=9)
    
    plot.display_all()
    exit(0)
    plDD={}
    for x in [ 'units','shortName']: plDD[x]=expMD[x]
    plDD.update({'timeV':timeV,'stimAmpl':ampls})

    #- - - -  display
    plDD['timeLR']=[10.,160.]  # (ms)  time range 
    if 0:
        plot.stims(stims,plDD,presc=3)

    if 0:
        plDD['amplLR']=[-90,70]  #  (mV) amplitude range
        plot.waveform(waves,plDD,presc=3,tit2=' sweep=%d'%isw)

    if 1:
        plot.survey_exp(bigD,plDD)
    
    plot.display_all('baseA')
    
