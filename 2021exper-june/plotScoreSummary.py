#!/usr/bin/env python3
'''
plot summary of scores for many data points

'''
import sys,os
import h5py
sys.path.append(os.path.abspath("../"))
from toolbox.Util_H5io3 import  write3_data_hdf5, read3_data_hdf5
from toolbox.Util_IOfunc import write_yaml, read_yaml

from toolbox.Plotter_Backbone import Plotter_Backbone
from pprint import pprint
import json

from matplotlib.patches import Rectangle

import numpy as np
import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")

    parser.add_argument("--expPath",  default='outExp/',help="experimental spikes data")
    parser.add_argument("--simPath",  default='outRoy/',help="Ry-sim spikes data")
                        

    parser.add_argument("-o","--outPath", default='out/',help="output path for plots and tables")
    parser.add_argument('--venue', dest='formatVenue', choices=['prod','poster'], default='prod',help=" output quality/arangement")


    args = parser.parse_args()

    args.prjName='scoreSum'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#............................
#............................
#............................
class Plotter(Plotter_Backbone):
    def __init__(self,args):
        Plotter_Backbone.__init__(self,args)
        #self.cL10 = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']  # 10 distinct colors
        self.mL7=['*','^','x','h','o','x','D']
        self.formatVenue=args.formatVenue
 
#...!...!..................
    def score_multiCells(self,bigD,plDD,simD,figId=6):
        xIn,yIn=8,5
        if  self.formatVenue=='poster':
            figId+=100
            xIn,yIn=6.5,6
             
        figId=self.smart_append(figId)
        nrow,ncol=1,1
        fig=self.plt.figure(figId,facecolor='white', figsize=( xIn,yIn))
        ax = self.plt.subplot(nrow,ncol,1)

        # .... plot experiments
        cellL=sorted(bigD)
        ampFact=plDD['ampFact']
        for i,cellN in enumerate(cellL):
            #print('rr',i,cellN)
            MA=bigD[cellN]
            #print('MA',type(MA[0]))
            fact=ampFact[cellN]           
            stimAmpl=MA[0]
            avrScore=MA[1]
            errScore=MA[2]
            
            stimAmpl*=fact
            dLab=r'%s,   X*%.2f'%(cellN,fact)

            hcol='C%d'%i
            if  self.formatVenue=='poster':
                hcol='red'
            dmk=self.mL7[i%7]
            ax.errorbar(stimAmpl,avrScore,yerr=errScore,marker=dmk,linewidth=1.3,label=dLab, alpha=0.8,color=hcol)

        #.... plot simu
        if simD!=None:
            fact=simD['ampFact']
            yA=simD['spikeCount']
            xA=np.array(simD['stimAmpl'])*fact
            dLab=r'sim bbp153  X*%.2f'%(fact)
            ax.fill_between(xA, yA[0],yA[2], alpha=0.3,label=dLab,color='grey')
            avrScore=np.mean(yA,axis=0)
            ax.plot(xA,avrScore,'-',color='black')

    
        ax.set(ylabel='spike count',xlabel='stim ampl (FS)')
        ax.set_xlim(0,0.36)
        if  self.formatVenue=='prod':
            ax.legend(loc='best',title='dataName, stimAmpl factor')
            ax.grid()

#...!...!..................
def compute_avr_score(bigD,inpMD):
    sweepC=bigD['sweepCnt']        
    N=sweepC.shape[0] # num stim ampl
    
    scoreA=bigD['spikeCount'] 
    stimAmplA=inpMD['stimAmpl']
    #print('P:spikeCount',bigD['spikeCount'] )

    
    avrScore=[]; stdScore=[]
    for n in range(N):  # loop over stim-ampl         
        k=sweepC[n]
        scoreV=scoreA[n][:k]        
        stimV=[stimAmplA[n]]*k
        avrScore.append(np.mean(scoreV))
        stdScore.append(np.std(scoreV))
           
    return np.array([stimAmplA,avrScore,stdScore])
#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":

    args=get_parser()
    
    inpD={'210607_1_NI':1.15, '210611_3_NI':1., '210611_4_NIa':0.65, '210611_6_NI':.95}
    # inpD={'210607_1_NI':1, '210611_3_NI':1., '210611_4_NIa':1., '210611_6_NI':1.} #raw data

    # load all data
    bigD={}
    for x in inpD:
        inpF='%s.spikerSum.h5'%(x)
        blob,inpMD=read3_data_hdf5(args.expPath+inpF, verb=0)
        bigD[x]=compute_avr_score(blob,inpMD)

    inpF='bbp153.spikerSum.h5'
    blob,inpMD=read3_data_hdf5(args.simPath+inpF, verb=1)
    simD={'spikeCount':blob['spikeCount'],'stimAmpl':inpMD['stimAmpl'],'ampFact':0.17}
    #simD=None
    # - - - - - PLOTTER - - - - -

    plot=Plotter(args)
    plDD={}
    plDD['ampFact']=inpD
    
    plot.score_multiCells(bigD,plDD,simD)

    plot.display_all('scoreSum')
