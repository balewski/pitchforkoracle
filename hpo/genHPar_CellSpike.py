#!/usr/bin/env python3
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
 tool generaring and evaluating HPAR for CellSpike
 
save: hpar_XX.conf.yaml and hpar_XX.sum.yaml 

uses generator for input

Segments:
* gen_CNN_block
* gen_FC_block
* gen_train_block
* build_model
* eval_speed

****Run on Cori
   module load tensorflow/gpu-2.1.0-py37	

One worker:
it may need   export CUDA_VISIBLE_DEVICES=2 
./genHParEval_CellSpike.py

on GPUs
1 GPU
salloc -N1  -C gpu -n1 -c 10 --gres=gpu:1  -t4:00:00

srun bash -c '  nvidia-smi '

srun -n1 ./genHPar_CellSpike.py --numTrials 5

'''

import socket  # for hostname
import os, time
import secrets
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

from pprint import pprint
startT0 = time.time()
import numpy as np

from tensorflow.keras.layers import Dense, Dropout,   Input, Conv1D,MaxPool1D,Flatten,Reshape, Lambda, BatchNormalization
from  tensorflow.python.keras.layers.advanced_activations import LeakyReLU 
import tensorflow as tf
from tensorflow.python.keras import backend as K  # just for GPU count

print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))
import sys,os
sys.path.append(os.path.abspath("toolbox")) # add ln -s for interactive testing
from Util_IOfunc import  write_yaml, read_yaml
from Util_CellSpike import build_model_cellSpike2

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--genConf', default='cellSpike_hpoG.conf.yaml',help="defines all contrants for HPAR generation and evaluation")
    parser.add_argument("-v","--verb",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("-o","--outPath",default='out',help="output path for HPAR")
    parser.add_argument("-n","--numTrials",default=5, type=int,help="number of HPAR sets to be generated")

    args = parser.parse_args()
    args.epochs=2
    args.steps=8
    args.facility='cori-gpu'
    np.random.seed() #  set the seed to a random number obtained from /dev/urandom 
    for i in range(int(50*np.random.rand())):
        np.random.rand()  # now rnd is burned in

    for arg in vars(args):  
        print( 'myArg:',arg, getattr(args, arg))
    print('seed test:',np.random.rand(4))

    return args


# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
class HparGenerator_CosmoFlow():
    def __init__(self,conf,verb=1):
        self.myConf=conf
        self.wrkLyr=Input(shape=tuple(conf['inpShape']),name='inp')
        self.verb=verb

        if verb: print('HypGen inp layer shape',self.wrkLyr.get_shape(),'verb=',verb)
        self.proposal={'inpShape':conf['inpShape']} # generated HPAR configuration
        self.proposal['outShape']=conf['outShape']
        self.proposal['myId']=secrets.token_hex(nbytes=4)
        self.timeBegin = time.time()
        self.qa={}
        self.proposal['qa_hpo_1gpu']=self.qa # this is a shortcut

    #...!...!....................
    def prep_CNN_block(self):
        if self.verb: print('CNN_block start')
        C=self.myConf['cnn_block']

        timeBin=self.myConf['inpShape'][0]
        if self.verb>1:   pprint(C)
            
        #apply constraint:  pool^numLyr < inpShape[0]
        nTry=0
        while nTry <10:
            nTry+=1
            pool_size=int(np.random.choice(C['pool_size']))
            numLyr=int(np.random.choice(C['num_layer']))
            mxLyr=np.log(timeBin)/np.log(pool_size)
            if numLyr<=mxLyr : break

        if nTry>5:
            print('WARN many pool*lyr trials:',nTry)
            return False
        
        if self.verb>1: print('numLyr=',numLyr,'nTry=',nTry)
                
        kernel=int(np.random.choice(C['kernel']))
        propBlock={'kernel':kernel, 'pool_size':pool_size}
        if self.verb>1: print('com prop block',propBlock)
 
        lyr=self.wrkLyr
        filt=int(uni_exp(C['first_dim_range']))

        propL=[] 
        for k in range(numLyr):
            if lyr.get_shape()[-2]==1: break # nothing to reduce more
            if k>0:
                filt=lyr.get_shape()[-1]*int(np.random.choice(C['dim_fact']))
            if self.verb>1:print('lyr=%d cnn filt=%d'%(k,filt))

            try:
                lyr=Conv1D(filt,kernel,strides=1,activation='linear',padding='same')(lyr)
                lyr =LeakyReLU()(lyr)
                lyr= MaxPool1D(pool_size=pool_size,padding='same')(lyr)
            except:
                if self.verb>1: print('create Conv3D+Pool layer failed',k)
                return False
            if self.verb>1: print('layer=%d Conv3D+Pool, output:'%k,lyr.get_shape())
            propL.append(filt)

        if self.verb>0: print('genCNN success,last layer shape',self.wrkLyr.get_shape(),'filters:',propL)
        propBlock['filters']=propL
        self.proposal['cnn_block']=propBlock

        self.wrkLyr=lyr
        return True

        
    #...!...!....................
    def prep_FC_block(self):
        if self.verb>0: print('FC_block start')

        # decide if BN layers are added
        self.proposal['batch_norm_cnn']=np.random.uniform()< self.myConf['cnn_BN_prob']
        self.proposal['batch_norm_flat']=np.random.uniform()< self.myConf['flat_BN_prob']

        C=self.myConf['fc']

        if self.verb>1: pprint(C)        
        numLyr=int(np.random.choice(C['num_layer']))

        if self.verb>1: print('numLyr=',numLyr)

        dropFrac=float(uni_exp(C['dropFrac_range']))
        self.proposal['fc_block']={'dropFrac':dropFrac}
       
        # generate params in the reverse order, no need to check if 'buildable'
        filt1=int(uni_exp(C['last_dim_range']))
    
        propL=[filt1]
        for i in range(1,numLyr):
            fact=np.random.choice(C['dim_fact'])
            filt1=int(min(512,filt1*fact))
            propL.insert(0,filt1)

        if self.verb>1: 
            print('genFC success, proposal:')
            pprint(propL)
        self.proposal['fc_block']['units']=propL
        return

    #...!...!....................
    def prep_train_block(self):
        if self.verb>0: print('train_block start')
        C=self.myConf['train']
        if self.verb>1: pprint(C)
        optName='adam'
        pp=C['localBS_range_power2']
        j=np.random.randint(pp[0],pp[1]+1)
        localBS=1<<j
        outD={'optName':optName,'lossName':'mse','localBS':localBS}
        
        initLR=float(uni_exp(C['initLR_range']))
        min_delLR=float(uni_exp(C['min_deltaLR_range']))
        reduceLR=float(uni_exp(C['reduceLR_range']))
        outD['LRconf']={'init':initLR, 'reduceFactor':reduceLR, 'patience':8,'min_delta':min_delLR}
        outD['multiAgent']={'warmup_epochs': 5}
        self.proposal['train_conf']=outD

    #...!...!....................
    def eval_train_speed(self,batch_size0,steps,epochs):
        batch_size=int(batch_size0*1.05)
        trainGen = simple_data_generator(X,Y, batch_size)

        fitVerb=1   # prints live:  [=========>....] - ETA: xx s 
        if self.verb==2: fitVerb=1
        if self.verb==0: fitVerb=2  # prints 1-line summary at epoch end

        gen.model.fit(trainGen,steps_per_epoch=steps, epochs=1,
                      verbose=fitVerb,
                      max_queue_size=10, workers=1, use_multiprocessing=False)

        if self.verb: print('restart train for %d epochs'%epochs)
        startT = time.time()
        gen.model.fit(trainGen,steps_per_epoch=steps, epochs=epochs,
                      verbose=fitVerb,
                      max_queue_size=10, workers=1, use_multiprocessing=False)

        totSec= time.time()-startT
        epochT=totSec/epochs
        samp_per_sec=batch_size*steps/epochT
        if self.verb>0: print('eval_train_speed  %.1f sec/epoch'%epochT)
        self.qa.update({'localBS':batch_size,'steps':steps,'samples_per_sec':samp_per_sec,'used_time':(time.time()- self.timeBegin)})
        return samp_per_sec


#...!...!....................
def uni_exp(AB):
    [A,B]=AB
    assert A>0
    assert A<=B
    a=np.log(A)
    b=np.log(B)            
    u=np.random.uniform(a,b)
    return np.exp(u)


#...!...!....................
def create_fake_dataset(inpShape,outShape,numSamp):
    print('create_dataset: X,Y:',inpShape,outShape,'N:',numSamp)
    # The input data and labels
    startT = time.time()

    Y=np.random.uniform(-1,1, size=(numSamp,outShape)).astype('float32')

    inpL=inpShape.copy()
    inpL.insert(0,numSamp)
    X=np.random.uniform(-0.8,0.8, size=tuple(inpL)).astype('float32')
    print(' done X shape',X.shape,X.dtype,' elaT=%.1f sec,'%(time.time() - startT))
 
    return X,Y

#...!...!..................
def simple_data_generator(X,Y, bs):
    mxJ=Y.shape[0]
    assert mxJ>=bs
    print('dataset: X,Y:',X.shape,Y.shape)
    j=0
    # loop indefinitely
    while True:
        if j>=mxJ-1-bs: j=0 # reset pointer if not enough for next batch
        xb=X[j:j+bs]
        yb=Y[j:j+bs]
        j+=bs
        yield (xb,yb)
    
 
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
if args.verb>0:
    Lkr=K._get_available_gpus()
    print('Seen GPU devCnt=%d '%len(Lkr),Lkr)

confN=args.genConf
conf=read_yaml(confN)
pprint(conf)

startT0 = time.time()

# generate minimalistic data set
numSample=1<<conf['train']['localBS_range_power2'][1]
X,Y=create_fake_dataset(conf['inpShape'],conf['outShape'],int(numSample*1.06))

nok=0
cnt={'shot':0,'cnn':0,'build':0,'size':0,'run':0,'acc':0}
for shot in range(args.numTrials):
    print('\n = = = = = = = = = = M:new shot cnt:',cnt)
    gen=HparGenerator_CosmoFlow(conf,verb=(shot==0)*args.verb)
    gen.qa['facility']=args.facility
    cnt['shot']+=1
    propF=args.outPath+'/hpar_cellSpike_%s.yaml'%gen.proposal['myId']
    if not gen.prep_CNN_block(): continue
    cnt['cnn']+=1
    gen.prep_FC_block()
    gen.prep_train_block()

    '''
    inpF='../hpar_cellSpike_smt190927_b_ontra3.yaml'
    print(' test external design=',inpF)
    gen.proposal=read_yaml(inpF)
    gen.proposal['qa_hpo_1gpu']=gen.qa
    #'''

    # use utility function for eaiser porting of the model to DeepCosmoFlow
    try:
        gen.model,totParCnt=build_model_cellSpike2(gen.proposal,verb=gen.verb)
    except:
        continue

    cnt['build']+=1
    gen.qa['param_count']=totParCnt
    if args.verb:
        print('M:proposal:');  pprint(gen.proposal)

    if  totParCnt <conf['constraints']['minModelParameters'] or\
        totParCnt >conf['constraints']['maxModelParameters'] :
        if args.verb>1: print('M: model is too small/large=%.3e param, ABANDON'%totParCnt,'shot=',shot)
        write_yaml(gen.proposal,propF+'-tmp',verb=gen.verb)
        continue
    
    cnt['size']+=1
    #continue #tmp

    localBS=gen.proposal['train_conf']['localBS']
    try:
        sampSpeed=gen.eval_train_speed(localBS,args.steps,args.epochs)
    except:
        if args.verb>1: print('\nB: too large BS %d, try next'%bs,shot)
        write_yaml(gen.proposal,propF+'-tmp',verb=gen.verb)
        continue

    cnt['run']+=1
    if sampSpeed <conf['constraints']['minSamplesPerSec']:
        if args.verb>1: print('M: model is too slow=%.2e sampl/sec, ABANDON'%sampSpeed,'shot=',shot)
        write_yaml(gen.proposal,propF+'-tmp',verb=gen.verb)
        continue

    cnt['acc']+=1
    write_yaml(gen.proposal,propF)

    fd=open(args.outPath+'/good_proposal_%s.txt'%gen.proposal['myId'],'w')
    fd.write('HparGood design: %s  localBS: %d   param_count: %.2e   samples_per_sec: %.2e  shot: %d\n'%(gen.proposal['myId'], localBS, gen.qa['param_count'],gen.qa['samples_per_sec'],shot))
    fd.close()

    nok+=1
    #time.sleep(3)

totMin= (time.time()-startT0)/60.
print('M: done all %d trials, nOK=%d, totTime=%.1f min'%(args.numTrials,nok,totMin))
print('M:cnt:',cnt)
