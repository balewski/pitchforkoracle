#!/usr/bin/env python3
""" 
testing performance of input generator
read test data from HD5

Use cases, needs valid config: 3inhib_3prB8kHz.yaml 

./testInpGen_CellSpike.py  --dataPath /global/homes/b/balewski/prjn/neuronBBP-pack8kHzRam/probe_3prB8kHz/ontra2/etype_3inhib

On Graphcore, inside Docker image

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import sys,os
sys.path.append(os.path.abspath("toolbox"))
from Deep_CellSpike import Deep_CellSpike

import numpy as np
import  time
import ruamel.yaml  as yaml
from pprint import pprint

from InpGenOntra_CellSpike import  CellSpike_input_generator

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2], help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("-d", "--dataPath",help="path to input",
                        default='/global/cfs/cdirs/m2043/balewski/neuronBBP-pack8kHzRam/probe_4prB8kHz/ontra4/etype_excite_v1/')
    parser.add_argument("--probeType",default='excite_4prB8kHz',  help="probe partition")

    parser.add_argument("--dom",default='train', help="domain is the dataset for which predictions are made, typically: test",choices=['train','test','val','witness'])

    parser.add_argument("-o", "--outPath", default='out/',help="output path for plots and tables")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm', action='store_true', default=False, help="disable X-term for batch mode")

    parser.add_argument("-n", "--localSamples", type=int, default=30000, help="samples to read")


    args = parser.parse_args()
    args.prjName='testIG'
    args.dataPath+='/'
    args.outPath+='/'
    args.modelDesign='fake1'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

deep=Deep_CellSpike.predictor(args)
dom=args.dom
opaque=['practice','witness'][0]

# instantiate input generator , selected flgs in bulk
#yamlConfTxt='{ numFeature: null, x_y_aux: true}'
#genConf=yaml.safe_load(yamlConfTxt)

genConf={}
genConf['cellList']=deep.metaD['cellSplit'][opaque]
genConf['name']='IG-'+dom+'-'+opaque
genConf['domain']=dom
genConf['h5nameTemplate']=deep.metaD['h5nameTemplate']

genConf['myRank']=10
genConf['localBS']=128
genConf['numLocalSamples']=args.localSamples
genConf['dataPath']=deep.dataPath
genConf['numTimeBin']=deep.metaD['numTimeBin']
genConf['numFeature']=deep.metaD['numFeature']
genConf['numPar']=deep.metaD['numPar']
genConf['shuffle']=True  # use False for reproducibility
genConf['x_y_aux']=True # it will produce 3 np-arrays per call

pprint(genConf)
#.... creates data generator
inpGen=CellSpike_input_generator(genConf,verb=2)

steps=max(1,args.localSamples//genConf['localBS'])
mxSteps=inpGen.__len__()
if 0: # restrict to 1 epoch, no data repetition
    steps=min(mxSteps,steps)

print('\nM:start processing dom=',dom,' localSamples=',args.localSamples,' steps=',steps, ' mxSteps/epoch=',mxSteps,'localBS=',genConf['localBS'])

time.sleep(5) # too see memory usage, tmp
startT0 = time.time()
uL=[]
for i in range(steps):
    x,u,aux =inpGen.__getitem__(i)
    #if i%100==0 :  print('M:%d step of %d done, shapes u,z:'%(i,steps),u.shape)
    uL.append(u)

predTime=time.time() - startT0
print('RAM-read done, samples/k=%d, elaT=%.3f sec,'%(args.localSamples/1000.,predTime))
print('batch shape X:',x.shape,'U:',u.shape)

#print('IG stepTimeHist:'); pprint(inpGen.stepTime)

# add all steps 
U=np.concatenate(tuple(uL),axis=0)

avrU=np.mean(U,axis=0)
stdU=np.std(U,axis=0)
parNL=deep.metaD['parName']
print('got U avr,std:',U.shape)
for i in range(avrU.shape[0]):
    print("idx=%d  avr=%.3f  std=%.3f  %s"%(i, avrU[i],stdU[i],parNL[i]))









