#!/usr/bin/env python
import sys
import csv

#............................
def read_one_csv(fname,delim=','):
    print('read_one_csv:',fname)
    tabL=[]
    with open(fname) as csvfile:
        drd = csv.DictReader(csvfile, delimiter=delim)
        print('see %d columns'%len(drd.fieldnames),drd.fieldnames)
        for row in drd:
            tabL.append(row)

    print('got %d rows \n'%(len(tabL)))
    #print('LAST:',row)
    return tabL,drd.fieldnames

#=================================
#=================================
#  M A I N 
#=================================
#=================================
    
tabRoy,keyL=read_one_csv('../web-generator/2019_master3.csv','\t')


cnt={'inp':0,'prod1':0,'prod2':0}
cell2D={}
# add prod-info to each Roy record
for row in tabRoy:
    #print('got',row)
    cnt['inp']+=1
    bbpN=row['bbp_name']
    shortN=row['short_name']

    etype=row['e_type']
    #if etype=='cAD': continue # remove excitatory
    if etype!='cAD': continue # remove inhibitory
    if etype not in cell2D: cell2D[etype]=[]
    cell2D[etype].append(shortN)
    cnt['prod1']+=1

    #print(shortN,row['e_type'],row['num_prob'])        
    #- - - - -  Slurm job
    #  sbatch batchOntraTransform.slr bbp202; sleep 3
    print('sleep 3; sbatch misc/batchOntraTransform.slr '+shortN)
    #- - - - -  morph-scan
    # ./probeLocator.py --cellName bbp202 -X |grep outSelD# |cut -d\# -f2
    #print('./probeLocator.py --cellName %s -X |grep outSelD# |cut -d\# -f2'%(shortN))

    # - - - PREDICTIONS
    # ./predict_CellSpike.py  --dataPath /global/cfs/cdirs/m2043/balewski/neuronBBP-pack8kHzRam/probe_3prB8kHz/etype_cNAC_v1_pred/ --design smt190927_57_5_bbp019   --probeType 3prB8kHz --seedModel aug17-cNAC_v1/train/out-cNAC_v1-3pr --outPath aug17-cNAC_v1/pred/3pr -n6000 --dom bbp027

print('echo DONE')

etypeL=sorted(cell2D)
print('\ncnt',cnt, len(etypeL),etypeL)
allL=[]
for etype in etypeL:
    cellL=cell2D[etype]
    allL+=cellL
    #print(etype,len(cellL),cellL[0])
    print('  # e-type=%s num-cell=%d\n  '%(etype,len(cellL)),end='')
    print(', '.join(cellL))

#print('\ntestCellNames: [',', '.join(allL),' ]')
print('\n./uparCalib.py  --cellName ',' '.join(allL))
#print('\nLall= ',"','".join(cellL))

