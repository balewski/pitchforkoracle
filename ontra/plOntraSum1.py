#!/usr/bin/env python
""" 
extract prediction residua mean+std for ONTRA

Input:
- meta-data used to generate ONTRA dataset:  etype_cNAC_v1.probLoc.yaml
- prediction summaries for all cells: cellSpike.sum_pred_bbp019.yaml

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import numpy as np
import  time
import sys,os
sys.path.append(os.path.abspath("../"))
from Util_IOfunc import write_yaml, read_yaml
from Plotter_Backbone import Plotter_Backbone
import matplotlib.patches as mpatches # for legend
import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2], help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm', action='store_true', default=False,help="disable X-term for batch mode")

    parser.add_argument( "--configName",default='etype_cNAC_v1', help="meta-data used to generate ONTRA dataset")

    #parser.add_argument("-s", "--slurmDir", nargs='+',help="path to inputs, a l blank separated list,expands 1234/10-13", default=['32462211/0'])


    parser.add_argument("-i", "--inpPath", default='../out/',help="input with prediction summary yaml's")
    parser.add_argument("-o", "--outPath", default='out2/',help="output path for plots and tables")

    #parser.add_argument("-N", "--cellName", type=str, default=None, help="cell shortName")
    args = parser.parse_args()
    args.prjName='ontraSum1'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#...!...!..................
def harvest_prediction(cellN,dataD):
    predF=args.inpPath+'/cellSpike.sum_pred_%s.yaml'%cellN
    predD=read_yaml(predF)

    for [conductN,avrx,stdx] in predD['lossAudit']:
        if 'fixed' in conductN or 'const' in conductN : continue
        if conductN not in dataD: dataD[conductN]={'avr':[],'std':[]}
        dataD[conductN]['avr'].append(avrx)
        dataD[conductN]['std'].append(stdx)
        
        
#............................
#............................
#............................
class Plotter_OntraPredSum(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)
        self.cellName=args.configName
#...!...!..................
    def modelSummary(self,conductNameL, cellND,tit1='',figId=5):

        def set_axis_style(ax, labels): # helper....
            ax.get_xaxis().set_tick_params(direction='out')
            ax.xaxis.set_ticks_position('bottom')
            ax.set_yticks(np.arange(1, len(labels) + 1))
            ax.set_yticklabels(labels)
            ax.set_ylim(0.25, len(labels) + 0.75)
            
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(8,9))
        ax = self.plt.subplot(1,1,1)
        set_axis_style(ax, conductNameL)

        ax.set_ylabel('varied %d conductances'%len(conductNameL))
        ax.set_xlabel('std of prediction residua')
        
        groupLab=[]

        for groupN,dataD,cellL in cellND:
            data2D=[]
            stats={'avr':[],'err':[]}
            for x in conductNameL:
                valV=np.array(dataD[x]['std'])
                data2D.append(valV)
                stats['avr'].append(valV.mean())
                stats['err'].append(valV.std())
                
            viop=ax.violinplot(data2D,showmeans=True, showmedians=False,  showextrema=False, vert=False, widths=0.95)
            color = viop["bodies"][0].get_facecolor().flatten()
            groupLab.append((mpatches.Patch(color=color), '%d %s cells'%(len(cellL),groupN)))
            '''
            binsY=[i+1 for i in range(len(conductNameL))]
            mkSym='o'
            if 'witness' in groupN: mkSym='s'
            ax.errorbar(stats['avr'], binsY, xerr=stats['err'],fmt=mkSym,linestyle=None,color='k')
            '''

        # Place a legend above this subplot, expanding itself to fully use the given bounding box.
        ax.legend(*zip(*groupLab),bbox_to_anchor=(0., 1.01, 1., .106), loc=3, ncol=2, mode="expand", borderaxespad=0.)
        ax.grid()
        ax.axvline(0.5, color='k', linestyle='--')

#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":

    args=get_parser()
   
    plot=Plotter_OntraPredSum(args)
    
    Lall= [ 'bbp002','bbp003','bbp006','bbp009','bbp011','bbp014','bbp019','bbp025','bbp027','bbp036','bbp042','bbp048','bbp052','bbp062','bbp068','bbp071','bbp079','bbp081','bbp088','bbp092','bbp096','bbp108','bbp112','bbp114','bbp122','bbp126','bbp134','bbp142','bbp147','bbp161','bbp165','bbp167','bbp175','bbp181','bbp189','bbp195','bbp200']
    print('all cells:',len(Lall))
    Lwit=[ 'bbp019', 'bbp027', 'bbp048', 'bbp092', 'bbp181' ] #witnessCell
    for x in Lwit: Lall.remove(x)
    print('train cells:',len(Lall))
    Ltrn=Lall
    #
    cellND=[('train',{},Ltrn),('witness',{},Lwit)]

    conductNameL=None
    for groupN,dataD,cellL in cellND:
        for cellN in cellL:
            harvest_prediction(cellN,dataD)
        if conductNameL==None: conductNameL=sorted(dataD)
    
    # order conductances by compartment
    bodyL=['som','axn','den','api']
    tmpD={x:[] for x in bodyL}
    for name in conductNameL:
        x=name.split('.')[-1]
        tmpD[x].append(name)
    outL=[]
    for x in bodyL:
        outL+=tmpD[x]
    #print('outL:',outL)
    conductNameL=outL

        
    #
    print('M:harvested %d conductances residua'%len(conductNameL),conductNameL[:4])

    #cellND=[('witness',dataD)]
    plot.modelSummary(conductNameL, cellND)
 
    
    plot.display_all('all')


    plot.dcaScore(sumD)
    
    plot.display_all(args.cellName,png=1)
