#!/usr/bin/env python3
""" 
split ~160 inhibitory cells into 10 cycles to allow each cell to be a witness cell
Works only on meta-data file
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import sys,os
sys.path.append(os.path.abspath("../toolbox"))
from Util_IOfunc import write_yaml, read_yaml

import  time
from pprint import pprint
import random

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2], help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("-d", "--dataPath",help="path to input",
                        #default='/global/cfs/cdirs/m2043/balewski/neuronBBP-pack8kHzRam/probe_4prB8kHz/ontra4/etype_excite_v1/'
                        default="/global/homes/b/balewski/prjn/neuronBBP-pack8kHzRam/probe_3prB8kHz/ontra3/etype_8inhib_v1"
                        )
    parser.add_argument("--probeType",  #default='excite_4prB8kHz',
                        default='8inhib157c_3prB8kHz',
                        help="probe partition")

    parser.add_argument("-o", "--outPath", default='out/',help="output path for plots and tables")
 
    args = parser.parse_args()
    args.prjName='testIG'
    args.dataPath+='/'
    args.outPath+='/'
    args.modelDesign='fake1'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
metaF='/meta.cellSpike_%s.yaml'%args.probeType
inpMD=read_yaml(args.dataPath+metaF)
pprint(sorted(inpMD))
cellSplit=inpMD['dataInfo']['cellSplit']
cellL=cellSplit['practice'].copy() + cellSplit['witness'].copy()
random.shuffle(cellL)
print('cellL', len(cellL),cellL[0],cellL[-1])
K=16; N=10
assert K*N >len(cellL)
assert (K-1)*N <len(cellL)

if 0: # testing
    K=4; N=3
    cellL=[i for i in range(N*K-1)]
    print(cellL)

cycD={}    
for n in range(N):
    i0=n*K; i1=i0+K
    practice=cellL[:i0]+cellL[i1:]
    witness=cellL[i0:i1]
    print('n=',n,witness,practice)
    cellSplit['practice']=practice
    cellSplit['witness']=witness
    cycN='cycle%s'%chr(ord('A')+n)
    cycD[cycN]=witness
    inpMD['dataInfo']['splitID']=cycN
    outF=metaF.replace('157c','157'+cycN)
    print(outF)
    write_yaml(inpMD,args.outPath+outF)

# save summary
for x in ['fullName','shortName','h5nameTemplate']:
    cycD[x]=inpMD['dataInfo'][x]
outF='inhibit157_witness_cycles.yaml'
write_yaml(cycD,args.outPath+outF)
                       
inpMD=read_yaml(args.dataPath+metaF)

exit
for x in cellL:
    print("MYCELL=%s bsub -J 'lsf-pitch[1-2]' batchTrainOntra3.lsf  "%x)
