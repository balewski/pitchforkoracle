#!/usr/bin/env python

''' 
plot probes location on the graph
plots 1st probe at the distance >50 

'''

import sys
import networkx as nx
import numpy as np
import sys,os
import ruamel.yaml  as yaml

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("--cellName", type=str, default='bbp153', help="cell shortName list, blanks separated")
    parser.add_argument("-o", "--outPath", default='out/',help="output path for plots and tables")
    parser.add_argument("-i", "--inpPath", default='/global/homes/b/balewski/prjn/neuronBBP-morphGraph/',help="input path for NX-yaml files")
    parser.add_argument("-k", "--numProbe", default=11,type=int,help="number of probes")
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,
        help="disable X-term for batch mode")
    args = parser.parse_args()

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    for xx in [ args.inpPath, args.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            exit(99)
    # handle graphics here
    import matplotlib as mpl
    if args.noXterm:
        mpl.use('Agg')  # to plot w/o X-server
        print('Graphics disabled')
    else:
        mpl.use('TkAgg') # on baci-desktop
        print('Graphics started')
            
    return args

#...!...!..................
def alternative_67probe_dump(probeL,cellN):
    print('alt67Dump#  %s: ['% cellN, end='')
    for i,x in enumerate(probeL):
        print('[%d, %s , 0.0], '%(i,x), end='')
    print(']')

    print('goalFeatureName: [', end='')
    for i,x in enumerate(probeL):
        print('pr%d, '%(i), end='')
    print(']')

    ok88


#=================================
#=================================
#  M A I N
#=================================
#=================================
args=get_parser()
import matplotlib.pyplot as plt  # quirk to alow disabling of graphics

grfF=args.inpPath+args.cellName+'.nx.yaml'

print('read graph:',grfF)
with open(grfF, 'r') as ymlFd :
    bulk=yaml.load( ymlFd, Loader=yaml.CLoader)
    G=bulk['nxgraph']
    xy_pos=bulk['xy_pos']
    probeInfo=bulk['probeInfo']
    probeL=bulk['probeName']

#assert args.numProbe<=len(probeL)

#probeL=probeL[:args.numProbe]
print('considred probes:',probeL)
#alternative_67probe_dump(probeL,args.cellName)
 
#------ graph partition in to : apic,axon and everything else
numNode= G.number_of_nodes()
print('G numNode=',numNode,'edges:',G.number_of_edges())
print('\nG any arms:',len(G['soma_0']))

# for now only deal with axon and apic separately
arms=["axon_0"]
isExcite=True
if isExcite and "apic_0" in G: arms.append('apic_0')

armsG={}
for arm in arms:
    H=nx.ego_graph(G,arm ,  radius=1000)
    G.remove_edge('soma_0',arm)
    armsG[arm]=H
    numEdge=H.number_of_edges()
    print('arm=%s edges=%d'%(arm,numEdge))
    #break
#reorganize soma_dend
H=nx.ego_graph(G,'soma_0',  radius=1000)
numEdge=H.number_of_edges()
print('arm=soma_dend edges=%d'%(numEdge))
nx.relabel_nodes(H,{'soma_0':'soma_dend'}, copy=False)
assert 'soma_0' not in H
armsG['soma_dend']=H
arms.append('soma_dend')
xy_pos['soma_dend']=xy_pos['soma_0']

if isExcite and 0: #reorganize apical_dend
    H=nx.ego_graph(G,'apic_0',  radius=1000)
    numEdge=H.number_of_edges()
    print('arm=apic_dend edges=%d'%(numEdge))
    nx.relabel_nodes(H,{'apic_0':'apic_dend'}, copy=False)
    assert 'apic_0' not in H
    armsG['apic_dend']=H
    arms.append('apic_dend')
    xy_pos['apic_dend']=xy_pos['soma_0']


prog='dot'

figId=0
sumFound=0
#dist_thr=50  #microns
dist_opt={'axon_0':150, 'soma_dend':50} #microns
if isExcite: dist_opt['apic_0']=150

nn='soma_0'
k,d2s=probeInfo[nn]
outSelD=[[k,nn,d2s] ]

for arm in arms:
    figId+=1
    fig=plt.figure(figId,facecolor='white', figsize=(5,8))
    ax = plt.subplot(1,1,1)
    node_size=2
    H=armsG[arm]
    dist_goal=dist_opt[arm]
    #pos=graphviz_layout(H,prog=prog)
    nx.draw(H, with_labels=False,pos=xy_pos,node_size=node_size)
    x,y=xy_pos[arm]
    plt.text(x,y+50,s=arm, bbox=dict(facecolor='red', alpha=0.5),horizontalalignment='center')

    # paint nodes from list
    probNL=[] 
    for nn in probeL:
        if nn in H: probNL.append(nn)

    print('\n',arm,' arm found probNL:',len(probNL))
    del0=99999
    nn0=None
    k0=None
    for nn in probNL:
        k,d2s=probeInfo[nn]
        dd=np.abs(d2s-dist_goal)
        #if d2s<dist_thr: continue
        if del0 < dd : continue
        del0=dd
        nn0=nn
        #print(k,nn,d2s)
    assert nn0!=None
    k,d2s=probeInfo[nn0]
    print(k,nn0,d2s)
    outSelD.append([k,nn0,d2s] )  # <=== THE OUTPUT
    if k0==None: continue
    sumFound+=len(probNL)
    nx.draw_networkx_nodes(H, xy_pos, nodelist=[nn0],
                           node_color='r',node_size=node_size*20, alpha=0.8)
    txt="num probe=%d\nfound:%d"%(len(probeL),len(probNL))
    ax.text(-0.05,-0.05,txt,transform=ax.transAxes,color='b')
    ax.text(-0.05,1.05,args.cellName,transform=ax.transAxes,color='b')
    (a,b)=xy_pos[nn0]
    ax.text(a,b,'%d %s %.1f'%(k0,nn0,d0),color='m')
    #break
print('qqq',len(probeL),sumFound)
print('outSelD#  %s:'%args.cellName,outSelD)

if not args.noXterm: plt.show()

