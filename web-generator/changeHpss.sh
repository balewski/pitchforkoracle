#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;  #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

fileL=" bbp001/  bbp010/  bbp020/  bbp032/ bbp042/  bbp051/  bbp061/  bbp071/  bbp081/  bbp090/  bbp098/  bbp109/  bbp120/  bbp129/  bbp141/  bbp149/  bbp159/  bbp168/  bbp180/  bbp192/  bbp200/  bbp002/  bbp011/  bbp023/  bbp034/  bbp043/  bbp052/  bbp062/  bbp072/ bbp082/  bbp091/  bbp099/  bbp111/  bbp121/  bbp132/  bbp142/  bbp150/  bbp160/  bbp171/  bbp181/  bbp193/  bbp201/ bbp003/  bbp012/  bbp024/  bbp035/  bbp045/  bbp053/  bbp063/  bbp075/  bbp083/  bbp092/  bbp100/  bbp112/  bbp122/  bbp133/  bbp143/  bbp151/  bbp161/  bbp172/  bbp182/  bbp194/  bbp202/ bbp004/  bbp013/  bbp025/  bbp036/  bbp046/  bbp054/  bbp066/  bbp076/  bbp084/  bbp093/  bbp101/  bbp113/  bbp124/  bbp134/  bbp144/  bbp152/  bbp162/  bbp173/  bbp185/  bbp195/  bbp203/ bbp005/  bbp014/  bbp026/  bbp037/  bbp047/  bbp055/  bbp067/  bbp077/  bbp086/  bbp094/  bbp102/  bbp114/  bbp125/  bbp135/  bbp145/  bbp153/  bbp164/  bbp174/  bbp186/  bbp196/  bbp204/ bbp006/  bbp017/  bbp027/  bbp038/  bbp048/  bbp056/  bbp068/  bbp078/  bbp087/  bbp095/  bbp106/  bbp115/  bbp126/  bbp136/  bbp146/  bbp154/  bbp165/  bbp175/  bbp187/  bbp197/  bbp205/ bbp007/  bbp018/  bbp028/  bbp040/  bbp049/  bbp057/  bbp069/  bbp079/  bbp088/  bbp096/  bbp107/  bbp118/  bbp127/ bbp139/  bbp147/  bbp155/  bbp166/  bbp176/  bbp188/  bbp198/  bbp206/ bbp008/  bbp019/  bbp031/  bbp041/  bbp050/  bbp060/  bbp070/  bbp080/  bbp089/  bbp097/  bbp108/  bbp119/  bbp128/  bbp140/  bbp148/  bbp156/  bbp167/  bbp179/  bbp189/  bbp199/  bbp207/ bbp009/ " 

path3=/nersc/projects/m2043/NeuronSim-vault-2019-data_67pr/
for x in $fileL; do
    echo =${x}=
    hsi ls -l $path3/$x
    #hsi chmod a+r $path3/$x/*
    break
done
