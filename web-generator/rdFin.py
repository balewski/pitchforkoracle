#!/usr/bin/env python
import sys,os,time
import csv
import ruamel.yaml  as yaml
from pprint import pprint

#...!...!..................
def read_yaml(ymlFn):
        print('  read  yaml:',ymlFn,end='')
        start = time.time()
        ymlFd = open(ymlFn, 'r')
        bulk=yaml.load( ymlFd, Loader=yaml.CLoader)
        ymlFd.close()
        print(' done, size=%d'%len(bulk),'  elaT=%.1f sec'%(time.time() - start))
        return bulk

#...!...!..................
def write_yaml(rec,ymlFn,verb=1):
        start = time.time()
        ymlFd = open(ymlFn, 'w')
        yaml.dump(rec, ymlFd, Dumper=yaml.CDumper)
        ymlFd.close()
        xx=os.path.getsize(ymlFn)/1048576
        if verb:
                print('  closed  yaml:',ymlFn,' size=%.2f MB'%xx,'  elaT=%.1f sec'%(time.time() - start))



#............................
def read_one_csv(fname,delim=','):
    print('read_one_csv:',fname)
    tabL=[]
    with open(fname) as csvfile:
        drd = csv.DictReader(csvfile, delimiter=delim)
        print('see %d columns'%len(drd.fieldnames),drd.fieldnames)
        for row in drd:
            tabL.append(row)

    print('got %d rows \n'%(len(tabL)))
    #print('LAST:',row)
    return tabL,drd.fieldnames

#............................
def write_one_csv(fname,rowL,colNameL):
    print('write_one_csv:',fname)
    print('export %d columns'%len(colNameL), colNameL)
    with open(fname,'w') as fou:
        dw = csv.DictWriter(fou, fieldnames=colNameL)#, delimiter='\t'
        dw.writeheader()
        for row in rowL:
            dw.writerow(row)


#=================================
#=================================
#  M A I N 
#=================================
#=================================

tabE,keyLE=read_one_csv('aa.csv','\t')
print('end-keys:',keyLE)

pprint(tabE)

'''
tabRoy0,keyL0=read_one_csv('jid-V141.csv','\t')
doneShortL=[]
for row in tabRoy0:
    shortN=row['short_name']
    doneShortL.append(shortN)
print('doneL len=',len(doneShortL), doneShortL[:5])
'''
    
tabRoy,keyL=read_one_csv('jid-Vall.csv','\t')

inpPath='/global/cscratch1/sd/vbaratha/neuronBBP-data_67pr/'
#out0='/global/cscratch1/sd/balewski/neuronBBP-train7/'

cnt={'inp':0,'prod':0,'totGB':0., 'totHd5':0,'totFrames_M':0}
tabOut=[]
for row in tabRoy:
    #print('got',row)
    cnt['inp']+=1
    shortN=row['short_name']
    inpDir=inpPath+shortN+'/'
    if not os.path.exists(inpDir): 
        print('skip missing  dir:',inpDir)
        continue
    sizeGB=sum(os.path.getsize(inpDir+f) for f in os.listdir(inpDir) if os.path.isfile(inpDir+f))/1024/1024/1024
    print('found ', inpDir,sizeGB)
    #for f in os.listdir(inpDir): print(f,os.path.getsize(inpDir+f))
    
    
    row2={ x:row[x] for x in ['bbp_name','short_name','etype','num_nominal_par'] }
    inpF=inpDir+'/meta.cellSpike.yaml'
    blob=read_yaml(inpF)
    assert blob['rawInfo']['bbpName']==row2['bbp_name']
    
    row2['num_all_par']=blob['dataInfo']['numPar']
    row2['num_iced_par']=blob['dataInfo']['numIcedPar']
    row2['num_dyn_par']=row2['num_all_par']-row2['num_iced_par']
    row2['num_sim_frame']=blob['dataInfo']['totalGoodFrames']
    row2['num_hd5_file']=blob['dataInfo']['numDataFiles']
    row2['num_prob']=blob['rawInfo']['num_probes']
    row2['size_hd5_GB']='%.0f'%sizeGB
    cnt['totGB']+=sizeGB
    cnt['totHd5']+=row2['num_hd5_file']
    cnt['totFrames_M']+=row2['num_sim_frame']/1.e6
    
    if len(tabOut)<5: pprint(row2)
    tabOut.append(row2)
    cnt['prod']+=1    
    #break
    
print('\ncnt',cnt)

print('\nfinal keyL=',keyLE)
write_one_csv('form_all55.csv',tabOut,keyLE)

