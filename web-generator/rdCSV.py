#!/usr/bin/env python
import sys
import csv


#=================================
#=================================
#  M A I N 
#=================================
#=================================

    
tabL1,k1=read_one_csv('tabA.csv','\t')
tabRoy,keyL=read_one_csv('tabM.csv')


# repack tab to dict using short name as prim key
key2=['simuA_who']
keyL+=key2
# append to tabRoy infro about test productions
key3=key2+['simuA_done',	'ML17pr_jobId',	'ML17pr_test_loss',	'ML17pr_epochs',	'ML17pr_frames2sec']

cnt={'inp':0,'prod':0}
# add prod-info to each Roy record
for row in tabRoy:
    #print('got',row)
    cnt['inp']+=1
    bbpN=row['bbp_name']
    for k in key2: row[k]=''
    for row1 in tabL1:    
        bbpN1=row1['bbp_name'][:-2]
        row1['jan_name']=row1['short_name']
        #print('do1', bbpN1)
        if bbpN1 not in bbpN: continue
        cnt['prod']+=1
        for k in key3: row[k]=row1[k]
        break
print('cnt',cnt)


print('\nfinal keyL=',keyL)
write_one_csv('current.csv',tabRoy,keyL)

print('exec on laptop:  scp cori.nersc.gov:~/pitchforkOracle/web-generator/current.csv .')
