#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;  #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

cellName=bbp206
inpPath1=/global/cscratch1/sd/vbaratha/neuronBBP-data_67pr/$cellName
inpPath2=/global/cscratch1/sd/balewski/bbp-recoveryA/$cellName
echo path1=$inpPath1
N=10

k=0
# select a random sample of N files 
ls ${inpPath1}/*h5 |sort -R |tail -$N |while read nameFull; do
    name=`basename "$nameFull"`
    echo "verify $name , $k of $N  ..."
    k=$[ $k +1 ]
    hash1=`md5sum $inpPath1/$name | cut -f1 -d\ `
    hash2=`md5sum $inpPath2/$name | cut -f1 -d\ `
    echo "   hash:  $hash1  $hash2"
    if [[ "$hash1" != "$hash2" ]] ; then
	echo "mismatch , failed"
	exit
    fi
done

echo "done comparing $N files"
exit
path3=/nersc/projects/m2043/NeuronSim-vault-2019-data_67pr/
