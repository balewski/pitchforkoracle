#!/usr/bin/env python3
""" generates web page w/ all trained cell results """
__author__ = "Jan Balewski"
__email__ = "balewski@lbl.gov"
 
rawSimPath='/global/cscratch1/sd/vbaratha/izhi/runs/'
packSimPath='/global/cscratch1/sd/balewski/neuronBBP-play/'
mlTrainPath='/global/cscratch1/sd/balewski/neuronBBP/'
webPath0='/project/projectdirs/m2043/www/'
url0='https://portal.nersc.gov/project/m2043/'
    
import sys,os
import time
import datetime
import shutil
from  pprint import pprint

import numpy as np
import csv

#............................
def read_one_csv(fname,delim=','):
    print('read_one_csv:',fname)
    tabL=[]
    with open(fname) as csvfile:
        drd = csv.DictReader(csvfile, delimiter=delim)
        print('see %d columns'%len(drd.fieldnames),drd.fieldnames)
        for row in drd:
            tabL.append(row)

    print('got %d rows \n'%(len(tabL)))
    #print('LAST:',row)
    return tabL,drd.fieldnames


#---------------------
def createOutDir(path0,child):
    # creates output dir on disc
    assert(os.path.isdir(path0))
    assert(len(child)>1)
    path1=path0+'/'+child
    print('create dir=',path1)
    if os.path.isdir(path1):
        path2=path1+'Old'
        if os.path.isdir(path2):
            shutil.rmtree(path2)
        os.rename(path1,path2)

    try:
        os.makedirs(path1)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

    os.chmod(path1,0o765) # a+r a+x
    return path1+'/'


#---------------------
def  report_one(webPath,row):
    simuId=row['simuA_jobId']
    if len(simuId)<5 : return

    if len(row['ML17pr_epochs']) <=0: return

    bbpN=row['bbp_name']
    #print('DO jan :',row['jan_name']);  return
    
          
    print('\nreport:',bbpN)#, simuId,janN,trainId)
    webDir=row['short_name']+':'+bbpN
    webPath1=createOutDir(webPath,webDir)

    '''
    # copy simu files
    rsPath=rawSimPath+row['simuA_jobId']
    print('rsPath=',rsPath)
    # Copy the content of 
    dest = shutil.copyfile(rsPath+'/*yaml',webPath1 )
    print('dest=',dest)
    '''

    # copy packed simu info
    psPath=packSimPath+row['jan_name']+'/packed17pr/'
    print('psPath=',psPath)
    # Copy the content of
    fnameL=['out/cellSpike_form_f%d.pdf'%i for i in range(4,9)]
    fnameL=fnameL+['rawMeta.cellSpike.yaml','meta.cellSpike.yaml']
    for fname in fnameL:
        fout=fname
        if 'out/' in fout: fout=fname[4:]
        outF = shutil.copyfile(psPath+fname,webPath1+fout )
        print('dest=',outF)
        os.chmod(outF,0o764) # a+r 


    # copy training files    
    trPath=mlTrainPath+row['ML17pr_jobId'].replace('_','/')+'/'
    print('trPath=',trPath)
    # Copy the content of
    fnameL=[]#'out/cellSpike_form_f%d.pdf'%i for i in range(4,9)]
    fnameL=fnameL+['hpar_cellSpike_100165_693.yaml','out/cellSpike.model.h5','out/cellSpike.sum_train.yaml','out/cellSpike.sum_pred.yaml','out/cellSpike_predict_f109.pdf','out/cellSpike_train_f10.pdf']
    for fname in fnameL:
        fout=fname
        if 'out/' in fout: fout=fname[4:]
        outF = shutil.copyfile(trPath+fname,webPath1+fout )
        print('dest=',outF)
        os.chmod(outF,0o764) # a+r
    xx=webPath1+'/cellSpike_predict_f109.pdf'
    print('AAA',xx.replace('/project/projectdirs/m2043/www//tryML/','https://portal.nersc.gov/project/m2043/ML4neuron2/'))

#==============================
#==============================
#==============================
def main():
    """To be used in CLI mode"""
    pathSuffix='tryML'
    webPath=createOutDir(webPath0,pathSuffix)

    if  len(sys.argv)>2:
        webPath=sys.argv[2]

    #tabRoy,keyL=read_one_csv('current.csv')
    tabRoy,keyL=read_one_csv('tabM-dec14.csv','\t')

    cnt={'inp':0,'prod':0}
    for row in tabRoy:
        #print('got1',row)
        cnt['inp']+=1
        report_one(webPath,row)
        #break

    print('\nURL:  ', url0+pathSuffix)
#- - - - - - - - - 
#- - - - - - - - - 
#- - - - - - - - - 
if __name__ == '__main__':
    main()
    print('ok-end-web')
    exit(0)
