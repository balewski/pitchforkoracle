#!/usr/bin/env python
'''
 generate random data for cellSpike probelm 
'''

import ruamel.yaml  as yaml
import numpy as np
import time

from Util_Func import  write_yaml, write_data_hdf5

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-v","--verb",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

parser.add_argument("-o","--outPath",
                        default='outX',help="output path for plots and tables")
parser.add_argument("-n", "--events", type=int, default=15,
                        help="number of frames per file")
parser.add_argument("-i",'--index' , type=int, default=3,
                        help="index of the output file name")
parser.add_argument( "-m","--metaOnly", type=int, default=0,
                     help="produce only meta-file assuming thi snumber of shards")


args = parser.parse_args()
nPar=28
nProbe=10
stimLen=10000
args.volts_shape=(args.events,stimLen,nProbe)
args.param_shape=(args.events,nPar)
args.coreName='mock_cell'
for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))

#=================================
#=================================
#  M A I N 
#=================================
#=================================
#mmm=[print('V%d, '%i,end='') for i in range(nProbe) ]; ok88

physRange=[ [i+5.,i+10.] for i in range(nPar) ]
stimV=np.array([ 2.*i/stimLen for i in range(stimLen) ] ).astype('float32')
parV=np.random.uniform(-0.8,0.8, size=args.param_shape).astype('float32')

vMx=10000
voltV=np.random.randint(-vMx,vMx, size=args.volts_shape, dtype=np.int16)

# add breadcrums: embed solution+some  noise  in X
parI=15000*(parV + 0.01*np.random.uniform(0,1, size=args.param_shape))
i1=100; i2=i1+nPar-2  # insert bread-crumbs not at the edge 
voltV[:,i1:i2,0]=parI[:,:nPar-2].astype('int16') # skip last 2 params

# Note: use bread-crumb (tiny) model for training to converg
rec={
    'binQA':np.zeros(args.events, dtype=np.int16),
    'norm_par':parV,
    'phys_par':10*parV,
    'phys_par_range': np.array(physRange).astype('float32'),
    'stim':stimV,
    'voltages':voltV
    }

for x in rec:
    print(x,rec[x].shape,rec[x].dtype)

hd5F=args.outPath+'/%s_shard%d.h5'%(args.coreName,args.index)
write_data_hdf5(rec,hd5F)

