#!/usr/bin/env python3
""" 
reduce data size by applying  precomputed PCA
For mass proecessing use pcaTransformMeta.py to generate the repack_bbp006.sh script
which will call this python script with appropriate arguments

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Util_IOfunc import write_yaml, read_yaml,write_data_hdf5,read_data_hdf5
from pprint import pprint
import numpy as np
from numpy import linalg as LA

import argparse, os
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i","--inpPath",help="oryginal data",  default='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr')
    parser.add_argument("-o","--outPath",default='outFixMe/',help="output for plots and Hd5")

    parser.add_argument("-N", "--cellName", type=str, default='bbp102',help="cell shortName")

    parser.add_argument("--allProbes",help="optional, needed for some cells, e.g. bbp019 wants 23", type=int,  default=67)
    parser.add_argument("--outShardId",help=" target id", type=int,  default=22)
    parser.add_argument("--inpShardId",help=" sorce ids",  nargs='+', type=int,  default=[10,11])
    parser.add_argument("--freqRebin",help=" frequency rebin factor", type=int,  default=1)
    

    args = parser.parse_args()
    args.verb=1
    args.inpPath+='/'
    args.outPath+='/'
    
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    for sub in ["11pr8kHz","67pr8kHz"]:
        path1=args.outPath+"probe_%s/%s"%(sub,args.cellName)
        if not os.path.exists(path1):
            print('missing out dir:',path1)
            exit(55)
    return args

def rebin_data(X,nReb):
        nS,tBin,nF=X.shape
        print('X1',X.shape,'nReb=',nReb)
        assert tBin==8000 # tested only for original data
        assert tBin%nReb==0
        a=X.reshape(nS,tBin//nReb,nReb,nF)
        b=np.sum(a,axis=2)/nReb
        print('X2',a.shape,b.shape,b.dtype)
        return b

    
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

metaF=args.outPath+"probe_11pr8kHz/%s/meta.cellSpike_11pr8kHz.yaml"%(args.cellName)
metaD=read_yaml(metaF)
num_tbin=metaD['dataInfo']['numTimeBin']
num_par=metaD['dataInfo']['numPar']

#pprint(metaD)

num_skim11=11 # hardcoded, quad
frame_per_inp=6144  # hardcoded

numFramesPerFile= frame_per_inp*len(args.inpShardId)

Fall=np.zeros((numFramesPerFile,num_tbin,args.allProbes),dtype='int16')
Fskim11=np.zeros((numFramesPerFile,num_tbin,num_skim11),dtype='int16')
Uall=np.zeros((numFramesPerFile,num_par),dtype='float32')
Pall=np.zeros((numFramesPerFile,num_par),dtype='float32')
print(' created output storage:',Fskim11.shape,Uall.shape)

iOff=0
for shardId in args.inpShardId:
    inpF=args.inpPath+args.cellName+"/%s.cellSpike.data_%d.h5"%(args.cellName,shardId)
    inpD=read_data_hdf5(inpF)
    assert frame_per_inp<= inpD['frames'].shape[0]
    assert num_skim11<=inpD['frames'].shape[2]

    Uall[iOff:iOff+frame_per_inp]=inpD['unit_par'][:frame_per_inp]
    Pall[iOff:iOff+frame_per_inp]=inpD['phys_par'][:frame_per_inp]

    X=rebin_data(inpD['frames'],args.freqRebin)
  
    Fall[iOff:iOff+frame_per_inp]=X[:frame_per_inp]
    Fskim11[iOff:iOff+frame_per_inp]=X[:frame_per_inp,:,:num_skim11]
    iOff+=frame_per_inp

print('repacking done')

# save data
probType='67pr8kHz'

outD={'unit_par':Uall, 'phys_par':Pall, 'frames':Fall}
outF=args.outPath+"probe_%s/%s/%s.cellSpike_%s.data_%d.h5"%(probType,args.cellName,args.cellName,probType,args.outShardId)
write_data_hdf5(outD,outF)

probType='11pr8kHz'
outD['frames']=Fskim11
outF=args.outPath+"probe_%s/%s/%s.cellSpike_%s.data_%d.h5"%(probType,args.cellName,args.cellName,probType,args.outShardId)
write_data_hdf5(outD,outF)

print('done transform  %s  for outShard: %d\n\n'%(args.cellName,args.outShardId))


