#!/usr/bin/env python3
""" 
format training data, save as hd5
 ./format_CellSpike.py  --dataPath data_bbp8v4_1_5h

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import sys,os

sys.path.append(os.path.abspath("../toolbox"))
from Plotter_CellSpike import Plotter_CellSpike
from Util_IOfunc import write_yaml, read_yaml, read_one_csv
from Pack_Func import agregate_raw_data
#from InpGen_CellSpike import  CellSpike_input_generator

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--venue', dest='formatVenue', choices=['prod','poster'], default='prod',help=" output quality/arangement")

    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")
    parser.add_argument( "--mock",action='store_true', default=False,
                         help="disable data generation, make only plots")
    parser.add_argument("-d","--dataPath",help="output path",  default='data')
    parser.add_argument("-o","--outPath",
                        default='out',help="output path for plots and tables")

    parser.add_argument("--predSumF",help="2nd meta data file",  default=None)
    
    args = parser.parse_args()
    args.prjName='cellSpike'
    if args.outPath=='same': args.outPath=args.dataPath
    args.verb=1
    args.dataPath+='/'
    args.outPath+='/'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
def shorten_param_names(rawMeta):
    mapD={'_apical':'_api', '_axonal':'_axn','_somatic':'_som','_dend':'_den'}
    inpL=rawMeta['varParL']
    outL=[]
    print('M: shorten_param_names(), len=',len(inpL))
    for x in inpL:
        #print('0x=',x)
        for k in mapD:
            x=x.replace(k,mapD[k])
        x=x.replace('_','.')
        
        '''# tmp - iceing Roy's params
        for y in iceD:
            if y['par_name']!=x: continue
            #print('rr',y,y['roys_exec_decision']=='1',y['roys_exec_decision'])
            if y['roys_exec_decision']=='1': continue
            if 'const' in x : continue
            x='ice.'+x
            break'''

        #print('1x=',x)
        
        outL.append(x)
    rawMeta['varParL']=outL
          
#............................
def triage_param_names(lossThr,rawMeta,predMeta):

    inpL=rawMeta['varParL']
    lossAudit=predMeta['lossAudit']
    outL=[]
    print('M: triage_param_names, len=',len(inpL),len(lossAudit))

    cnt={'inp':0, 'var':0, 'ice':0,'skip':0}
    for x in inpL:
        cnt['inp']+=1
        for a,b,c in lossAudit:
            if a!=x : continue
            #print('found', a,b,c)
            if c > lossThr : x='ice.'+x
            break
        print('0x=',x)
        if 'skip' in x :
            cnt['skip']+=1
        elif 'ice' in x:
            cnt['ice']+=1
        else:
            cnt['var']+=1
        outL.append(x)
    rawMeta['varParL']=outL
    rawMeta['parCnt']=cnt
    print('M: triage_param_names() cnt=',cnt)

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

rawMF=args.dataPath+'/rawMeta.'+args.prjName+'.yaml'
metaF=args.dataPath+'/meta.'+args.prjName+'.yaml'

if args.mock:  # 1=skeep reading raw data, just read meta-file
    blob=read_yaml(metaF)
    metaD=blob['dataInfo']
    rawD=blob['rawInfo']
else:
    rawMeta=read_yaml(rawMF)
    args.targetName='%s.%s.data'%(rawMeta['bbpId'],args.prjName)
    #iceF='roy-ice-list.csv'
    #iceL,iceH=read_one_csv(iceF,'\t')
    shorten_param_names(rawMeta)#,iceL)
    
    if args.predSumF !=None:
        what_isThat
        predMeta=read_yaml(args.predSumF)
        triage_param_names(0.54,rawMeta,predMeta)
    if 'num_probes' in rawMeta:
        nProbes=len(rawMeta['probeName'])
        rawMeta['num_probes'] =min(rawMeta['num_probes'],nProbes)
    else:
        rawMeta['num_probes']=nProbes
    
    metaD,stimul=agregate_raw_data(args,rawMeta)
    write_yaml(metaD,metaF)
    print('M: main task completed, metaF=',metaF)
    print('M: meta content:', list(metaD['dataInfo'].keys()))
    rawD=metaD['rawInfo']
    metaD=metaD['dataInfo']
    stimF=args.dataPath+'stim.%s.yaml'%rawD['stimName']
    outD={'timeAxis': rawD['timeAxis'],'stimName':rawD['stimName'],'stimFunc':stimul}
    write_yaml(outD,stimF)
    

dom='train'
# approximate # of frames/file
num1=metaD['totalGoodFrames']/ metaD['numDataFiles']
print('M: only test generator, do some plotting, dom=',dom,'num1=',num1)
fnameTmpl=args.prjName+'.data_*.h5'
digiNoise=None
#digiNoise=[22., 0.6]

num2=int(num1*.5)
genConf={'h5nameTemplate':metaD['h5nameTemplate'], 'dataPath':args.dataPath, 'shuffle':False, 'digiNoise': digiNoise, 'voltsScale':50., 'batch_size':num2, 'fakeSteps':None,'x_y_aux':True, 'num_probes':9 ,'useDataFrac':1.0}

genConf['fileIdxL']= metaD['splitIdx'][dom]
genConf['name']=dom    
 
inpGen=CellSpike_input_generator(genConf,verb=1)
maxStep=inpGen.__len__()
print('inpGen batch size=',maxStep)
x,u,p =inpGen.__getitem__()

plot=Plotter_CellSpike(args,metaD )

plot.frames_vsTime(x,u,1,metaD=rawD, stim=stimul)
plot.frames_vsTime(x,u,8,metaD=rawD, stim=stimul)
plot.fparams_corr(u,'true U')
plot.params1D(p,'true P',figId=3,isPhys=True)
plot.params1D(u,'true U',figId=4)
plot.stims(stimul, rawD['stimName'], rawD['timeAxis'],figId=1 )
plot.display_all('form',pdf=2)
print('form_done%s'%metaD['h5nameTemplate'])
