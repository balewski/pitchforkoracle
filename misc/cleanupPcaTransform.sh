#!/bin/bash
# this script moves transformed data to final locations
# note, it write also to 'pr67' origianl data place - do it once!

set -u ;  # exit  if you try to use an uninitialized variable
set -e ;  #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value



pr67Path=/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr
inpPath=/global/cfs/cdirs/m2043/balewski/neuronBBP-packTest7
outPath=/global/cfs/cdirs/m2043/balewski/neuronBBP-packMay1

echo path=$outPath
cellL1="bbp205"
cellL="bbp206 bbp207" # bbp102 bbp153 bbp054  bbp205 bbp098 bbp152 bbp154 bbp155 bbp156 bbp176
exit


for cell in $cellL ; do
    echo cell=$cell
    metaF=$inpPath/probe_orig/${cell}/meta.cellSpike_orig.yaml
    metaS=$pr67Path/${cell}/
    echo metaF=$metaF $metaS
    cp $metaF $metaS
    #exit
    #break

    for pt in pca99 quad octal ; do
	echo work on probeType=$pt
	
	path1=${inpPath}/probe_$pt/$cell
	path2=${outPath}/probe_$pt
	echo $pt $path1  $path2
	time mv  $path1  $path2
	
    done
    # end of probeType
    #break
done

