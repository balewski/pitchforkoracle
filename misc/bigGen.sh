#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;  #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

outPath='/global/homes/b/balewski/prj/roy-neuron-sim-data/mock28p_10v/raw/'
echo path=$outPath

numEve=512

for k in `seq 0 1023`; do
    ./genVoltage_mockdata.py -n $numEve --index $k --outPath $outPath
    #exit
done

