#!/usr/bin/env python3
import sys,os,time
sys.path.append(os.path.abspath("../toolbox"))

from Util_IOfunc import write_data_hdf5,read_data_hdf5
import numpy as np

data1=np.zeros(55).astype('float32')
data1[3]=33
data2=np.zeros(66).astype('float32')

outD={'bbp01':data1,'bbp02':data2}

outF='abc.h5'
write_data_hdf5(outD,outF)

# read data back
blob=read_data_hdf5(outF)
data1b=blob['bbp01']
print('data1b',data1b)
