#!/usr/bin/env python3
import socket  # for hostname
import horovod.tensorflow.keras as hvd 
import tensorflow as tf

gpus = tf.config.experimental.list_physical_devices('GPU')
locGPUs=len(gpus)

hvd.init()
numRanks= hvd.size()
myRank= hvd.rank()
locRank=hvd.local_rank()

print(' Horovod: initialize  myRank=%d of %d on %s locGPUs=%d , locRank=%d'%(myRank,numRanks,socket.gethostname(),locGPUs,locRank))


