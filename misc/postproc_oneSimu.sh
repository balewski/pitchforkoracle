#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

# use case: ./postproc_oneSimu.sh bbp001 L1_DAC_bNAC219_1 27301979,27308458,27246954 67  1K


files_out=40 #  null   or a number
simuSourcePath=/global/cscratch1/sd/vbaratha/DL4neurons/runs/

bbpId=$1  # short_name
bbpName=$2  
simuJobIdL=$3  # list:  123,567,324  (no spaces)
num_probes=${4-17}
packExt=${5-4K}


# get 1st jobid for the list for meta-data
simuJobId=`echo $simuJobIdL | cut -f1 -d,`

# init other variables
if [[ $packExt == "1K" ]]; then
    frames_out=1024 
elif [[ $packExt == "2K" ]]; then
    frames_out=2048
elif [[ $packExt == "4K" ]]; then
    frames_out=4096 
elif [[ $packExt == "6K" ]]; then
    frames_out=6144 
elif [[ $packExt == "8K" ]]; then
    frames_out=8196
elif [[ $packExt == "16K" ]]; then
    frames_out=16384
elif [[ $packExt == "40K" ]]; then
    frames_out=40960
else 
    echo wrong packExt=$packExt=
    exit 99
fi

echo $packExt $frames_out
targetPath0=/global/cscratch1/sd/balewski
#targetPath0=/global/cscratch1/sd/vbaratha
targetPath=$targetPath0/neuronBBP-packed7y/data_${num_probes}pr_${packExt}fr/

#echo "# <p><hr> <p> $bbpId &nbsp; &nbsp; &nbsp; $bbpName  &nbsp; &nbsp;  job=$simuJobId <br> <img src="$bbpId/cellSpike_predict_f109.png"> <hr>"


inpPath1=$simuSourcePath/$simuJobId/$bbpName/
outPath=$targetPath/$bbpId/
echo neuronSim prep simuJobId=$simuJobId, bbpName=$bbpName bbpId=$bbpId  inp1=$inpPath1  out=$outPath frames_out=$frames_out
date

echo 1b-count hd5 files in $inpPath1 ...
nHD5=` find $inpPath1/ -type f -name  *h5 |wc -l`
echo see $nHD5 files in $simuJobId , there may be more...

echo 2-create output dir $outPath
rm -rf $outPath
mkdir -p $outPath/out

echo 3-produce rawMeta stub
metaF=$outPath/stubMeta.yaml
echo "# automatic metadata generated on `date` " > $metaF
echo "bbpId: $bbpId " >> $metaF
echo "simuJobId: $simuJobId " >> $metaF
echo "maxOutFiles: $files_out"  >> $metaF
echo "numFramesPerOutput: $frames_out "  >> $metaF  
echo "shuffle: True # use False only for debugging"  >> $metaF
echo "digiNoise: None"  >> $metaF
echo "useQA: False "  >> $metaF
echo "num_probes: $num_probes "  >> $metaF
echo "rawPath0: $simuSourcePath "  >> $metaF
echo "rawJobIdL: [ $simuJobIdL ]  "  >> $metaF
echo "# "  >> $metaF
echo "# add meta data from Vyassa below: "  >> $metaF

echo 'metaStub dump - - - - - -'
cat $metaF
metaF2=$outPath/rawMeta.cellSpike.yaml
metaF0=`ls $inpPath1/../${bbpName}*yaml`
ls -l $metaF0
cat  $metaF   > $metaF2

# remove 'rawPath' from Vyassa's yaml because now we have muti-job scheme and this path points to just 1 job
cat  $metaF0 |grep -v rawPath >> $metaF2
echo created complete metafile
ls -l $metaF2

echo 4-execute formating of Vyassas $nHD5 HD5 files ...
time python3 -u ./format_CellSpike.py --noXterm --dataPath $outPath  --outPath $outPath/out

echo 5-open read access
chmod a+rx -R $targetPath/$bbpId
chmod a+rx -R $outPath

echo DONE $bbpId
date
