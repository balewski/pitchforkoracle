#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;  #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value


# Ontra4 data on Summit
dataPath=/gpfs/alpine/nro106/proj-shared/neuronBBP-pack8kHz/probe_4prB8kHz/ontra4/etype_excite_v1
probeType=excite_4prB8kHz
wrkDir0="/gpfs/alpine/world-shared/nro106/balewski/hpoOntra4/"

jid=519520  # 390
#jid=517447  # 20


trainPath=${wrkDir0}/$jid
echo path=$trainPath
numEve=20000

if [ ! -f "predict_CellSpike.py" ]; then
    echo "wrong starrting dir, abort"
    pwd
    exit
fi


k=0
for aid in $( ls $trainPath)  ; do   
    modelPath=${trainPath}/${aid}/out
    echo
    echo $aid  $modelPath 
    testFile=${modelPath}/cellSpike.sum_pred_test.yaml
    if [ ! -f "$testFile" ]; then
	echo "$testFile not exists, skip"
	continue
    fi
    
    for name in practice  witness ; do
	echo predict $name 
	./predict_CellSpike.py   --dataPath $dataPath --probeType $probeType --events $numEve  --noXterm   --seedModel  $modelPath --cellName $name  --outPath  $modelPath 2>&1 | grep lossMSE
    done
    k=$[ $k + 1 ]
    echo " ==================  $k  done  aid=$aid "
    #exit
done
exit



# Ontra3 data on Summit: 
dataPath=/gpfs/alpine/nro106/proj-shared/neuronBBP-pack8kHz/probe_3prB8kHz/ontra3/etype_8inhib_v1/
probeType=8inhib157c_3prB8kHz
wrkDir0="/gpfs/alpine/world-shared/nro106/balewski/hpoOntra3/"

jid=458117  # 80
#jid=457485  # 20
