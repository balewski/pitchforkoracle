#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;  #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

#dataPath=data_izhi_4pv6c
#coreOut=izhi_4pv6c-ML693-izhi_4pv6c
#MLtrainJob=102120

#dataPath=data_mainen_4pv29
#coreOut=mainen_4pv29-ML693-mainen_4PV29
#MLtrainJob=103186

#dataPath=data_mainen_7pv31
#coreOut=mainen_7pv31-ML693-mainen_7pv31
#MLtrainJob=102179

#dataPath=data_mainen_10pv32
#coreOut=mainen_10pv32-ML693-mainen_10pv32
#MLtrainJob=102229

#dataPath=data_hh2dend_10pv2c
#coreOut=hh_2dend_10pv2c-ML693-hh_2dend_10pv2c
#MLtrainJob=103238

#dataPath=data_hhbast_7pv3
#coreOut=hh_ballstick_7pv3-ML693-hh_ballstick_7pv3
#MLtrainJob=106879 

#dataPath=data_hhbast_4pHv3
#coreOut=hh_ballstick_4pHv3-ML693-hh_ballstick_4pHv3
#MLtrainJob=106937

#dataPath=data_hhbast_4pEv3
#coreOut=hh_ballstick_4pEv3-ML693-hh_ballstick_4pEv3
#MLtrainJob=107006

# cross-polination .....
#dataPath=data_izhi_4pv6c
#coreOut=${dataPath}-ML693-mainen_10pv32
#MLtrainJob=102229

# accuracy vs. training size study ...
dataPath=data_mainen_10pv32b
#coreOut=train_1.8M-ML693-mainen_10pv32b
#MLtrainJob=142030
#coreOut=train_900k-ML693-mainen_10pv32b
#MLtrainJob=142012
#coreOut=train_450k-ML693-mainen_10pv32b
#MLtrainJob=138913
coreOut=train_220k-ML693-mainen_10pv32b
MLtrainJob=138789


trainPath='/global/cscratch1/sd/balewski/neuron3/'$MLtrainJob

echo path=$trainPath
outPath0=/project/projectdirs/mpccc/balewski/roy-neuron-sim-data/paper1/may16_pred/$coreOut/
numEve=5000

k=0

for Kid in $( ls $trainPath)  ; do   
    modelPath=${trainPath}/${Kid}/out
    outPath=$outPath0/$Kid
    echo $Kid  $modelPath $outPath

    mkdir -p $outPath
    cp ${modelPath}/../data/meta.cellRegr.yaml $outPath/meta.model.cellRegr.yaml
    cp ${dataPath}/meta.cellRegr.yaml $outPath/meta.trace.cellRegr.yaml
    ./predict_CellHH.py --dataPath $dataPath --seedModel $modelPath --events $numEve -X --outPath $outPath
    #exit
    chmod a+rx $outPath
    chmod a+r $outPath/*
    k=$[ $k + 1 ]
    echo " ==================  $k  done  Kid=$Kid "
    #exit
done
# give access to mother-dir
chmod a+rx ${outPath}/..
