#!/usr/bin/env python3
""" 
master, dispatches re-sampling of timebins
from 40 kHz to 8 kHz, freqRebin=5
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Util_IOfunc import write_yaml, read_yaml,write_data_hdf5,read_data_hdf5
from pprint import pprint
import numpy as np

import random

import argparse, os
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i","--inpPath",help="oryginal data",  default='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr')
    parser.add_argument("-o","--outPath",default='/global/cfs/cdirs/m2043/balewski/neuronBBP-packTest8/',help="output for plots and Hd5")
    parser.add_argument("--cellName", type=str, default='bbp006',help="cell shortName")

    args = parser.parse_args()
    args.verb=1
    args.freqRebin=5
    args.inpPath+='/'
    args.outPath+='/'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

#..... open command file
cmdF=args.outPath+"/scripts/repack_%s.sh"%args.cellName
cmdFd = open(cmdF, 'w')
cmdFd.write("#!/bin/bash\n")
cmdFd.write("set -u ;  # exit  if you try to use an uninitialized variable\n")
cmdFd.write("set -e ;  #  bash exits if any statement returns a non-true return value\n")
cmdFd.write("set -o errexit ;  # exit if any statement returns a non-true return value\n")

npack=4
print('# ',args.cellName,npack)

metaF=args.inpPath+args.cellName+"/meta.cellSpike_orig.yaml"
metaD=read_yaml(metaF)
#pprint(metaD)

print('metaD primary keys',list(metaD.keys()))
timeAxis=metaD['rawInfo']['timeAxis']
timeAxis['step']*=args.freqRebin
metaD['dataInfo']['numTimeBin']= metaD['dataInfo']['numTimeBin']//args.freqRebin
metaRaw=metaD['rawInfo']
metaInp=metaD['dataInfo']
h5nameTemplate=metaInp.pop('h5nameTemplate')
metaInp.pop('numDataFiles')
feature_std=metaInp.pop('feature_std')
XX=np.array(feature_std)/np.sqrt(args.freqRebin)
metaInp['feature_std']=[ float('%.1f'%x) for x in XX]
wrong std --> x/sqrt(5) ???
splitIdx=metaInp.pop('splitIdx')

# ..... NEW META files ......
outMeta={'rawInfo':metaRaw, 'dataInfo':metaInp}



# ........  repack splitIdx
inpIdxL=[]
for dom in splitIdx:
    inpIdxL+=splitIdx[dom]
print('see inpIdxL:',len(inpIdxL),'h5nameTemplate=',h5nameTemplate)

m=len(inpIdxL)//npack
assert m>0
moreArgs=''
#moreArgs=' --allProbes 23 ' # for bbp019
for i in range(m):
    i2=i+1
    idxL=inpIdxL[i*npack:i2*npack]
    #print(i,idxL)
    cmdFd.write('time ./freqTransformOne.py --cellName %s --outShardId %d --freqRebin %d --inpShardId '%(args.cellName,i,args.freqRebin))
    [ cmdFd.write('%d '%i) for i in idxL]
    cmdFd.write(' --outPath %s %s \n'%(args.outPath, moreArgs))
cmdFd.write('\n date \n echo done with all shards\n')

# generate new idxL
allL=[i for i in range(m) ]
random.shuffle(allL) # works in place
frac=0.10
nFrac=max(1,int(m*frac))
if 2*nFrac+1>m:
    print('\n\nWARN,  sometning went wrong, only %d output hd files was created. It is not enough to assigne 1 for test & validation. All is saved but training will most likely crash. Fix it\n\n'%m)
    exit(111)

splitIdxD={'test': allL[:nFrac]}
splitIdxD['val']=allL[nFrac:2*nFrac]
splitIdxD['train']=allL[2*nFrac:]

metaInp['numDataFiles']=m
metaInp['splitIdx']=splitIdxD
metaInp['numFramesPerFile']=npack*6144
metaInp['totalFrameCount']=m*metaInp['numFramesPerFile']


skimL=[('67pr8kHz',67),('11pr8kHz',11)]
# ..... skim output
for skimStr,num_skim in skimL:
    #assert len(metaRaw['probeName']) >=num_skim
    if  num_skim>metaRaw['num_probes']:
        num_skim=metaRaw['num_probes']
        print('\n **** Warn **** - to many probles requested, reduce to: ',num_skim)
        
    metaInp['h5nameTemplate']=h5nameTemplate.replace('cellSpike','cellSpike_'+skimStr)
    metaInp['featureType']='probes_'+skimStr
    metaInp['numFeature']=num_skim
    metaInp['featureName']=metaRaw['probeName'][:num_skim]
    #pprint(metaInp)
    metaF=args.outPath+"probe_%s/%s/meta.cellSpike_%s.yaml"%(skimStr,args.cellName,skimStr)
    write_yaml(outMeta,metaF)
