#!/usr/bin/env python
""" 
extract HPAR values and the training loss for random forest training
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import numpy as np
import  time
import os
from Util_IOfunc import write_yaml, read_yaml#,write_one_csv

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("-s", "--slurmDir", nargs='+',help="path to inputs, a l blank separated list", default=['590283/3'])


    parser.add_argument("-o", "--outPath",
                        default='out/',help="output path for plots and tables")
 

    args = parser.parse_args()
    args.sourcePath='/global/cscratch1/sd/balewski/neuronBBP-pca2/'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
def locateSummaryData(src_dir,prefix,verb=1):
    src_dir+='/out/'  # for  trainMulti data
    for xx in [ src_dir]:
        if os.path.exists(xx): continue
        print('Aborting on start, missing  dir:',xx)
        exit(1)

    if verb>0: print('use src_dir:',src_dir,'prefix:',prefix)

    jobL=os.listdir(src_dir)
    print('locateSummaryData got %d potential jobs, e.g.:'%len(jobL), jobL[0])
    print('sub-dirs',jobL)

    # at this point the list of dirs is fixed, now picking of sum.yaml
    outD={}
    nJob=0
    for jobId in jobL:
        verb1=len(outD)==0
        dataPath=src_dir+'/'+jobId
        sumF=''
        anyL=os.listdir(dataPath)
        for x in anyL:
            if 'cellSpike.sum_pred.yaml' in x: 
                sumF=dataPath+'/'+x ; break
        #print('open sumF=',sumF)
        if len(sumF)<10:
            print(' missing jobId=',jobId,' skip')
            continue
        try:
            predD=read_yaml(sumF,verb=verb1)
        except:
            crashIt34
            continue

        design=predD['design']
        hparF=dataPath+'/../../hpar_cellSpike_%s.yaml'%design
        hparD=read_yaml(hparF)
        hparD['design']=design
        hparD['testMSE_loss']=predD['testLossMSE']
        nJob+=1
        
        if verb>1 and len(outD)==0:print(hparD)
        key='%s_%s'%(prefix,jobId)
        outD[key]=hparD
        #break #tmp
        
    return outD
            

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
def expand_hapr(hpar,verb=1):
    skipL=['inp_batch_norm','junk1','myId','numFeature','lastAct','outAmpl','steps']
    for x in skipL: hpar.pop(x)
    conv_repeat=hpar.pop('conv_repeat')
    conv_filter =hpar.pop('conv_filter')
    optimizer=hpar.pop('optimizer')
    conv_exp=[]
    for x in conv_filter:
        for j in range(conv_repeat):
            conv_exp.append(x)
            
    hpar['conv_dims']=conv_exp
    hpar['optimizer']=optimizer[0]
    hpar['init_lr']=optimizer[1]
    return hpar
    # testing only
    for k in hpar:
        val=hpar[k]
        print(k,val)


#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":

    args=get_parser()

    sumD={}
    # grid sampling
    for slurmJid in args.slurmDir:
        sumD1=locateSummaryData(args.sourcePath+slurmJid, slurmJid)
        sumD.update(sumD1)
    print('M: got num sum=',len(sumD))

    desL=[]
    for jid in sumD:
        rec=sumD[jid]
        desL.append(expand_hapr(rec))

    keyL=sorted(desL[0])    
    # split data to train/test and save
    np.random.shuffle(desL)
    print(' harvested %d hpar sets'%len(desL))
    test_frac=0.2
    nTest=int(test_frac*len(desL))
    assert nTest>2

    outF='/hpar_survey.test.yaml'
    write_yaml({'test':desL[:nTest]},args.outPath+outF)
    outF='/hpar_survey.train.yaml'
    write_yaml({'train':desL[nTest:]},args.outPath+outF)

