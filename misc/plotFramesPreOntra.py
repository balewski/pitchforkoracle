#!/usr/bin/env python3
import sys,os

sys.path.append(os.path.abspath("../"))
from Util_IOfunc import write_yaml, read_yaml, write_data_hdf5, read_data_hdf5

import numpy as np
from Plotter_Backbone import Plotter_Backbone

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")

    parser.add_argument("-o","--outPath",
                        default='out/',help="output path for plots and tables")

    parser.add_argument("-N", "--cellName", type=str, default='bbp153',
                        help="cell shortName")

    args = parser.parse_args()
    args.dataPath='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr/' #Cori
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#............................
#............................
#............................
class Plotter_OneAAA(Plotter_Backbone):
    def __init__(self, args,metaD):
        Plotter_Backbone.__init__(self,args)
        self.metaD=metaD
#...!...!..................
    def VoltsSomaNice(self,X,stim,ifr,figId=100):

        probeNameL=self.metaD['featureName']
        nBin=X.shape[1]
        maxX=nBin ; xtit='time bins'
        binsX=np.linspace(0,maxX,nBin)
        numProbe=X.shape[-1]

        assert numProbe<=len(probeNameL)

        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(7,3))

        print('plot input traces, numProbe',numProbe)

        yLab='ampl (mV)'
        ax = self.plt.subplot(1,1,1)
        ipr=0
        amplV=X[ifr,:,ipr]/150.
        ax.plot(binsX,amplV,label='%d:%s'%(ipr,probeNameL[ipr]),marker='x',markersize=5, linewidth=0.5)
        ax.plot(stim*10, label='stim',color='black', linestyle='--')
        ax.legend(loc='best')
        ax.set_xlim(700,1800)
        #ax.set_xlim(910,980)
        #ax.set_xlim(0,6000)
        ax.grid()
        
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
args.prjName='niceA'

shortN=args.cellName
shard=7
inpF='%s/%s.cellSpike.data_%d.h5'%(shortN,shortN,shard)
bulk=read_data_hdf5(args.dataPath+inpF)

frames=bulk['frames']#[::50]
print('inp data',frames.shape)

metaF=args.dataPath+"/%s/meta.cellSpike_orig.yaml"%(args.cellName)
metaD=read_yaml(metaF)
print(metaD.keys())
plot=Plotter_OneAAA(args,metaD['dataInfo'])

metaF=args.dataPath+"/%s/stim.chaotic_2.yaml"%(args.cellName)
stimD=read_yaml(metaF)
print(stimD.keys())
stim=stimD['stimFunc']

frid=3
plot.VoltsSomaNice(frames,stim,frid, figId=frid)
plot.display_all()
