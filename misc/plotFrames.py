#!/usr/bin/env python3
import sys,os

# just plot data, Ontra-2 data format

sys.path.append(os.path.abspath("../"))
from Util_IOfunc import write_yaml, read_yaml, write_data_hdf5, read_data_hdf5

import numpy as np
from Plotter_Backbone import Plotter_Backbone

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")

    parser.add_argument("-o","--outPath",
                        default='out/',help="output path for plots and tables")

    parser.add_argument("-N", "--cellName", type=str, default='bbp027',
                        help="cell shortName")

    args = parser.parse_args()
    #args.dataPath='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr/' #Cori
    args.dataPath='/gpfs/alpine/nro106/proj-shared/neuronBBP-pack8kHz/probe_3prB8kHz/ontra2/etype_3inhib_v2' #Summit
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#............................
#............................
#............................
class Plotter_OneAAA(Plotter_Backbone):
    def __init__(self, args,metaD):
        Plotter_Backbone.__init__(self,args)
        self.metaD=metaD
#...!...!..................
    def VoltsSomaNice(self,X,stim,ifr,figId=100):

        probeNameL=self.metaD['featureName']
        nBin=X.shape[1]
        maxX=nBin ; xtit='time bins'
        binsX=np.linspace(0,maxX,nBin)
        numProbe=X.shape[-1]

        assert numProbe<=len(probeNameL)

        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(10,6))

        print('plot input traces, numProbe',numProbe)

        yLab='ampl (a.u.)'
        ax = self.plt.subplot(1,1,1)
        ipr=0
        for ipr in range(X.shape[2]):
            amplV=X[ifr,:,ipr] # ampl was normalized
            ax.plot(binsX,amplV,label='%d:%s'%(ipr,probeNameL[ipr]),marker='x',markersize=5, linewidth=0.5)
        ax.plot(stim, label='stim',color='black', linestyle='--')
        ax.legend(loc='best')
        #ax.set_xlim(700,1800)
        #ax.set_xlim(910,980)
        #ax.set_xlim(0,6000)
        ax.grid()
        
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
shortN=args.cellName
args.prjName='niceA_'+shortN
args.dom='train'



inpF='/%s_3inhib.cellSpike_3prB8kHz.data.h5'%(shortN)
bulk=read_data_hdf5(args.dataPath+inpF)

frames=bulk[args.dom+'_frames']#[::50]
print('inp data',frames.shape)

metaF=args.dataPath+inpF.replace('data.h5','meta.yaml')
metaD=read_yaml(metaF)
print(metaD.keys())
plot=Plotter_OneAAA(args,metaD['dataInfo'])

metaF=args.dataPath+"/stim.chaotic_2_8kHz.yaml"
stimD=read_yaml(metaF)
print(stimD.keys())
stim=stimD['stimFunc']

if 0:
   print('dump stim')
   for x in stim:
       print('%.3f,'%x)

frid=3
plot.VoltsSomaNice(frames,stim,frid, figId=frid)
plot.display_all()


