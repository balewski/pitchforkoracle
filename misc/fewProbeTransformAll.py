#!/usr/bin/env python3
""" 
 reduction of probe count from 67 to 3 or 4 
 INPUT:  67probes,  8 kHz
 it reads *all* data in to RAM and then writes 3 output files at once.
 data are normalized to N(0,1) and saved as float32
 Output:
 - metaData
 - bbp006.cellSpike_67pr8kHz.data_train.h5,  *test.h5,  *val.h5  

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Util_IOfunc import write_yaml, read_yaml,write_data_hdf5,read_data_hdf5
from pprint import pprint
import numpy as np

import random

import argparse, os
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i","--inpPath",help="oryginal data",  default='/global/cfs/cdirs/m2043/balewski/neuronBBP-pack8kHzDisc/probe_67pr8kHz/')
    parser.add_argument("-o","--outPath",default='/global/cfs/cdirs/m2043/balewski/neuronBBP-packTest9/',help="output for plots and Hd5")
    parser.add_argument("--cellName", type=str, default='bbp006',help="cell shortName")
    parser.add_argument( "--numFeature", type=int, default=None, nargs='+',
        help="input features, overwrite hpar setting, can be a list")

    args = parser.parse_args()
    args.verb=1
    args.inpPath+='/'
    args.outPath+='/'
    if args.numFeature!=None:
        #print('nnn',len(args.numFeature))
        # see Readme.numFeature for the expanation
        if len(args.numFeature)==1: # Warn: selecting 1 feature which is not soma is not possible with this logic
            args.numFeature=[i for i in range (args.numFeature[0])]
        else: #assure uniqnenss of elements
            assert len(args.numFeature)==len(set(args.numFeature))           

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

# only to rebin stimulus I forgot earlier
def rebin_data1D(X,nReb):
        tBin=X.shape[0]
        print('X1',X.shape,nReb)
        assert tBin==8000 # tested only for original data
        assert tBin%nReb==0
        a=X.reshape(tBin//nReb,nReb)
        b=np.sum(a,axis=1)/nReb
        print('X2',a.shape,b.shape,b.dtype)
        return b



#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
out_numFeature=len(args.numFeature)
skimStr='%dpr8kHz'%out_numFeature

metaF=args.inpPath+args.cellName+"/meta.cellSpike_67pr8kHz.yaml"
metaD=read_yaml(metaF)
#pprint(metaD)

print('metaD primary keys',list(metaD.keys()))

metaInp=metaD['dataInfo']
splitIdx=metaInp.pop('splitIdx')

# ..... modify META Data ......
totalFrameCount=metaInp['totalFrameCount']
frac=0.10
nFrac=max(1,int(totalFrameCount*frac))

splitSize={'train': totalFrameCount-2*nFrac}
splitSize['test']=nFrac
splitSize['val']=nFrac
print('splitFrames:',splitSize)

metaInp['numDataFiles']=3
metaInp['splitIdx']=splitSize  # this name is kept for backward compatibility 
metaInp.pop('numFramesPerFile')

inp_h5nameTemplate=metaInp['h5nameTemplate']
metaInp['h5nameTemplate']=inp_h5nameTemplate.replace('67pr8kHz',skimStr)

metaInp['featureType']='probes_'+skimStr
metaInp['numFeature']=out_numFeature

# open stimulus to compress it
stimName=metaD['rawInfo']['stimName']
stimD=read_yaml(args.inpPath+args.cellName+"/stim.%s.yaml"%stimName)
print('stimD:', sorted(stimD))
stimD['timeAxis']=metaD['rawInfo']['timeAxis']
stimA=stimD.pop('stimFunc')
print('inp stim',stimA.shape, stimA.dtype)
#off stimA=rebin_data1D(stimA,5)
stimName+='_8kHz'
print('out stim',stimA.shape, stimA.dtype)
stimD['stimFunc']=stimA
stimD['stimName']=stimName

metaD['rawInfo']['stimName']=stimName

a=[];b=[];c=[]
for i in args.numFeature:
    a.append(metaInp['featureName'][i])
    b.append(metaInp['feature_mean'][i])
    ss=metaInp['feature_std'][i]*2.236  # un-do sqrt(rebin), the summed data were NOT independent, but rather very correlated so variance has almost not changed.
    c.append(float('%1.f'%ss))

metaInp['featureName']=a
metaInp['feature_mean']=b
metaInp['feature_std']=c
norm_mean=np.array(b)
norm_std=np.array(c)
#print('metaInp:'); pprint(metaInp)

metaInp['dataNormalized']=True

# ........  repack splitIdx
inpIdxL=[]
for dom in splitIdx:
    inpIdxL+=splitIdx[dom]
print('see inpIdxL:',len(inpIdxL),'h5nameTemplate=',inp_h5nameTemplate)

inpIdxL=sorted(inpIdxL)
#print('sss',inpIdxL)

m1=len(inpIdxL)
assert m1>0


metaF=args.outPath+"probe_%s/%s/meta.cellSpike_%s.yaml"%(skimStr,args.cellName,skimStr)
write_yaml(metaD,metaF)

stimF=args.outPath+"probe_%s/%s/stim.%s.yaml"%(skimStr,args.cellName,stimName)
write_yaml(stimD,stimF)


print('Start repacking of %d H5 files'%m1)

num_tbin=metaD['dataInfo']['numTimeBin']
num_par=metaD['dataInfo']['numPar']
num_skim11=11 # hardcoded, quad
frame_per_inp=6144  # hardcoded

Fall=np.zeros((totalFrameCount,num_tbin,out_numFeature),dtype='float32')
Uall=np.zeros((totalFrameCount,num_par),dtype='float32')
Pall=np.zeros((totalFrameCount,num_par),dtype='float32')
print(' created output storage:',Fall.shape,Uall.shape)

frame_per_inp=-1
iOff=0
for shardId in inpIdxL:
    print('read shard %d of %d'%(shardId,m1))
    inpF=args.inpPath+args.cellName+"/%s.cellSpike_67pr8kHz.data_%d.h5"%(args.cellName,shardId)
    inpD=read_data_hdf5(inpF,verb=shardId<10)
    assert out_numFeature<=inpD['frames'].shape[2]
    if frame_per_inp<0:
        frame_per_inp=inpD['frames'].shape[0]
        assert frame_per_inp*m1==totalFrameCount
    else:
        frame_per_inp==inpD['frames'].shape[0]

    Uall[iOff:iOff+frame_per_inp]=inpD['unit_par'][:]
    Pall[iOff:iOff+frame_per_inp]=inpD['phys_par'][:]

    # scale data to ~N(0,1)
    X=(inpD['frames'][...,args.numFeature] - norm_mean) / norm_std
    Fall[iOff:iOff+frame_per_inp]=X
    iOff+=frame_per_inp

    #break
print('repacking done for',args.cellName)

'''
# check mean & std
for i in range(out_numFeature):
    X1=Fall[:iOff,:,i]
    xa=X1.mean()
    xb=X1.std()
    xaa=metaInp['feature_mean'][i]
    xbb=metaInp['feature_std'][i]
    print(i,X1.shape, xa.shape,xa,xaa,xb,xbb)
    #break
exit(0)
'''

dom='test'
outD={'unit_par':Uall[:nFrac], 'phys_par':Pall[:nFrac], 'frames':Fall[:nFrac]}
outF=args.outPath+"probe_%s/%s/%s.cellSpike_%s.data_%s.h5"%(skimStr,args.cellName,args.cellName,skimStr,dom)
write_data_hdf5(outD,outF)


dom='val'
outD={'unit_par':Uall[nFrac:2*nFrac], 'phys_par':Pall[nFrac:2*nFrac], 'frames':Fall[nFrac:2*nFrac]}
outF=args.outPath+"probe_%s/%s/%s.cellSpike_%s.data_%s.h5"%(skimStr,args.cellName,args.cellName,skimStr,dom)
write_data_hdf5(outD,outF)


dom='train'
outD={'unit_par':Uall[2*nFrac:], 'phys_par':Pall[2*nFrac:], 'frames':Fall[2*nFrac:]}
outF=args.outPath+"probe_%s/%s/%s.cellSpike_%s.data_%s.h5"%(skimStr,args.cellName,args.cellName,skimStr,dom)
write_data_hdf5(outD,outF)
