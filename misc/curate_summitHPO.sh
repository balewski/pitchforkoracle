#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;  #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

outPath='outHPO/'

myList=../hpo_8kHz_summit.jobs
n=0
while read line ; do
    if [[ ${line:0:1} == "#" ]] ; then continue; fi
    n=$[ $n +1]
    #echo $n,$line
    cell=`echo $line | cut -f1 -d\ `
    probe=`echo $line | cut -f2 -d\ `
    jid=`echo $line | cut -f3 -d\ `
    arr1=`echo $line | cut -f4 -d\ `
    arr2=`echo $line | cut -f5 -d\ `
    #echo cell=$cell
    out=${outPath}/${cell}/${probe}
    mkdir -p $out
    echo  ./rank_Design.py -X -s ${jid}/${arr1}-${arr2} --outPath $out --topK 7
    #exit #tmp
done <$myList
echo "echo done all"
echo
echo done $n lines

