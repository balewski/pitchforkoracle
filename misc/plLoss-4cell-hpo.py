#!/usr/bin/env python
""" 
 make plot from data copy/past from gDoc table - data are stored in text file 1 cell per line. This program re-build the table and makes plots

This plot shows end-loss for  HPO for 4 cells , 8kHz, on Summit

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np
import  time
from pprint import pprint

import sys, os
sys.path.append(os.path.abspath("../"))
from Plotter_Backbone import Plotter_Backbone

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("-t", "--tableName",help="input table copied from gDoc", default='loss-4cell-hpo-8kHz_v2.txt')


    parser.add_argument("-o", "--outPath",
                        default='out/',help="output path for plots and tables")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    args.prjName='plLoss'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
#............................
#............................
class Plotter_Design(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)

#............................
    def allCell(self,allD,figId=21,tit='',ytit=''):
        nrow,ncol=1,1
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(9,6))

        ax=self.plt.subplot(nrow,ncol,1)

        xLab=[ '1pr_8kHz', '2pr_8kHz', '3pr_8kHz', '4pr_8kHz', '11pr_8kHz', '67pr_8kHz', 'PCA_8kHz', 'PCA_40kHz','11pr_40kHz']

        sD={}
        sD['BBP153']=('D',' excite + complx','b')
        sD['BBP102']=('o',' excite + simple','g')
        sD['BBP027']=('>',' inhib + complx','r')
        sD['BBP019']=('x',' inhib + simple','m')
        
        for cell in ['BBP153','BBP102','BBP027','BBP019']:
            rec=allD[cell]            
            yL=[]; eL=[]; xL=[]
            for xl in xLab:
                if xl not in rec['probe']: continue
                i=rec['probe'].index(xl)
                xL.append(xl)
                yL.append(rec['lossAvr'][i])
                eL.append(rec['lossErr'][i])
            mt,lab,col=sD[cell]
            ax.errorbar(xL[:-2], yL[:-2], yerr=eL[:-2], fmt=mt, label=cell+lab, ls='--',linewidth=1.,color=col, lolims=True)
            ax.errorbar(xL[-2:], yL[-2:], yerr=eL[-2:], fmt=mt,  ls='--',linewidth=1.,color=col)

            #break
        #ax.grid(True)
        ax.legend(loc='upper center')
            #ax.set_ylim(0,)
        ax.set(xlabel='sampling type',ylabel='MSE loss',title=tit)

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
def readTableA(inpF,ncol):
    fd=open(inpF,'r')
    data=fd.readlines()
    fd.close()
    npt=len(data)
    nrow=npt//ncol
    print('data size=',npt,' nrow=',nrow)
    assert npt==ncol*nrow
    data=[x[:-1] for x in data]  # get rid of \n for each line
    # unpack rows
    outD={}
    #nprobN=[]; avrLoss=[];stdLoss=[];convFrac=[];trainTime=[]
    for i in range(nrow):
        j=i*ncol
        x=data[j+0]
        #print(j,i,x)
        if 'ignore' in x: continue  #  skip this  row
        if x[:3]=='BBP': # new cell
            rec={'probe':[],'lossAvr':[],'lossErr':[]}
            outD[x]=rec
        rec['probe'].append(data[j+1])
        xL=data[j+4].split()
        #print('xL:',xL)
        val=float(xL[0])
        
        try: # error may be m issing, use default in such case
            err=float(xL[2])
        except:
            err=val/50. # default
            
        rec['lossAvr'].append(val)
        rec['lossErr'].append(err)

    return outD
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

allD=readTableA(args.tableName,ncol=7)
#pprint(allD['BBP102'])

plot=Plotter_Design(args)
tit='data='+args.tableName
plot.allCell(allD,tit=tit,figId=10)

plot.display_all()  
