#!/usr/bin/env python
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

# experimental code for boosting quality of generated HPAR with pre-trained Random Forest

import ruamel.yaml  as yaml
from pprint import pprint
import numpy as np
from  hpar4forest  import  expand_hapr

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--numTimeBin", help="num inp time bins", type=int, default=8000)
parser.add_argument("--nOutFeat", help="num out features", type=int, default=31)
parser.add_argument("-v","--verb",type=int,choices=[0, 1, 2], help="increase output verbosity", default=1, dest='verb')
parser.add_argument("-o","--outPath", default='out',help="output path for plots and tables")
parser.add_argument('--design', dest='modelDesign', default='experiment',help=" model design of the network")
parser.add_argument('--rfModel', default='fixMe12',help=" pre-trained Random Forest")

args = parser.parse_args()
for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))

np.random.seed() #  set the seed to a random number obtained from /dev/urandom 
print('seed:',np.random.rand(4))
for i in range(int(50*np.random.rand())):
    np.random.rand()
# now rnd is burned in

# - - - - - - - - - - - - - -
# - - - - - - - - - - - - - -
    
def get_input_HPar():
    myId='%.11f'%np.random.uniform()
    norm_batch_bool = True
    hpar={'myId':'id1'+myId[2:], 'inp_batch_norm' : norm_batch_bool,'junk1': [1],'numFeature': None}
    return hpar

def get_CNN_HPar():
    # use 2^k size and make subsequent layers not smaller
    numLyr=np.random.randint(3,8)
    filt1=int(np.random.choice([8,16,32,64,128]))
    filtL=[filt1]
    for i in range(1,numLyr):
        fact=np.random.choice([1,2,4])
        filt1=int(min(256,filt1*fact))
        filtL.append(filt1)

    kern=np.random.randint(3,10)
    pool=np.random.randint(3,10)
    repeat=np.random.randint(1,4)
    hpar={'conv_filter': filtL, 'conv_kernel': kern,
          'conv_repeat': repeat, 'pool_len': pool }
    return hpar

# - - - - - - - - - - - - - -
# - - - - - - - - - - - - - -
def isValid_CNN_HPar(nInp,hpar):
    filtL=hpar['conv_filter']
    ker=hpar['conv_kernel']
    pl=hpar['pool_len']
    #print('isValid_CNN_HPar ?  inp=%d filtL='%nInp,filtL)

    nn=nInp
    for dim  in filtL:
        for irep in range(hpar['conv_repeat']):
            x=( nn - ker)/pl 
            nn=int(x)
            if nn<=2 : return False
        #print('aa',dim,x,nn)
 
    return True
# - - - - - - - - - - - - - -
# - - - - - - - - - - - - - -
def get_FC_HPar(nOut):
    numLyr=np.random.randint(2,8)

    filt1=nOut*np.random.choice([1,2,4,8,16])
    #print('oo',nOut,filt1)
    filtL=[int(filt1)]
    for i in range(1,numLyr):
        fact=np.random.choice([1,2,4])
        filt1=int(min(512,filt1*fact))
        filtL.insert(0,filt1)
        #print('ff',i,fact,filt1, )

    #lastAct=str(np.random.choice(['linear','tanh']))
    #ampl=np.random.uniform(1.0,2.0)
    lastAct='tanh'
    ampl=1.2
    dropFrac=float(np.random.choice([0.01, 0.02, 0.05,0.10]))

    hpar={'fc_dims': filtL, 'lastAct':  lastAct, 'outAmpl': ampl, 'dropFrac': dropFrac}
    return hpar


# - - - - - - - - - - - - - -
# - - - - - - - - - - - - - -
def get_Optimizer_HPar():

    loss=str(np.random.choice(['mse','mae']))
    optName=str(np.random.choice(['adam','nadam'])) #,'adadelta'
    optP0=1.e-3; optP1=1.1e-7
    j=np.random.randint(5,8)
    bs=1<<j
    
    #bs=8 #tmp for Graphcore
    #optP0=1.e-3*float(np.random.choice([0.05, 0.10, 0.20,0.40,1.0])) #tmp for Graphcore
    xx=np.random.uniform(0.2,0.8)
    lrReduce=xx*xx
    hpar={'lossName':loss,'optimizer':[optName,optP0,optP1],'batch_size':bs,'reduceLR_factor':lrReduce,'steps':None}
    return hpar

def genHpar():
    hpar0=get_input_HPar()
    #pprint(hpar0)

    while True:
        hpar1=get_CNN_HPar()
        if isValid_CNN_HPar(args.numTimeBin,hpar1) : break
    #pprint(hpar1)

    hpar2=get_FC_HPar(args.nOutFeat)
    #pprint(hpar2)
    # exit(0)
    hpar3=get_Optimizer_HPar()
    #pprint(hpar3)
    return hpar0,hpar1,hpar2,hpar3

#=================================
#=================================
#  M A I N 
#=================================
#=================================

print('Load RF from:',args.rfModel)
# Harry1 - create RF model predictor object here
rfModel=None  

targetMSEloss=0.01

nTry=0
while 1:
    nTry+=1
    hpar0,hpar1,hpar2,hpar3=genHpar()
    hparD={}
    hparD.update(hpar0)
    hparD.update(hpar1)
    hparD.update(hpar2)
    hparD.update(hpar3)
    hparFlat=expand_hapr(hparD)
    #pprint(hparFlat)
    predLoss=rfModel.predict(hparFlat) 
    if predLoss < targetMSELoss: break
    
print('HPAR accepted by RF after %d traials'%nTry)
pprint(hparD)

outF=args.outPath+'/hpar_cellSpike_%s.yaml'%(args.modelDesign)


with open(outF, 'w') as myfd:
    myfd.write('# Hyper params for cellSpike regression\n# You can set key value to None-string  and it will get auto-converted to None \n')
    myfd.write('\n# # input\n')
    yaml.safe_dump(hpar0, myfd)
    myfd.write('\n# # CNN params\n')
    yaml.safe_dump(hpar1, myfd)
    myfd.write('\n# FC params\n')
    yaml.dump(hpar2, myfd)
    myfd.write('\n# training\n')
    yaml.dump(hpar3, myfd, default_flow_style=False)
    myfd.write('\n')
print ('saved',outF)

