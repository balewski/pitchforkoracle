#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value


echo 'packing-driver start on '`hostname`' '`date`
# this must produce 1 in shifter
echo inShifter=`env|grep  SHIFTER_RUNTIME`=

nameList=$1
wrkDir=$2
echo wrkDir=$wrkDir
cd $wrkDir  # important if running as vyassa, he has non-standard ~/

procIdx=${SLURM_PROCID}
maxProc=`cat $nameList |wc -l`

echo my procIdx=$procIdx   maxProc=$maxProc "pwd: "`pwd`
sleep $(( 1 + ${SLURM_LOCALID}*30  + $SLURM_PROCID))
echo RANK_`hostname` $SLURM_PROCID 

if [ $procIdx -ge $maxProc ]; then
   echo "rank $procIdx above maxProc=$maxProc, idle ..."; exit 0
fi

line=`head -n $[ $procIdx +1 ] $nameList |tail -n 1`
echo line=${line}=

shortN=`echo $line | cut -f2 -d\ `

echo Do $shortN line=$line

#run string with values as a command 
eval "$line" >& log.$procIdx.$shortN

echo "$shortN task_done  on  "`date` 
