#!/usr/bin/env python
""" 
 make plot from data copy/past from gDoc - data are stored in text file 1 cell per line. This program re-build the table and makes plots
Example input for a table with 6 rows, each row start with '[n]pr' value as a tag.

head loss-bbp153-table2.txt
1pr
705338/7
68.3
0.31
7/7
275
3pr
705342/7
51.3
0.27

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_Backbone import Plotter_Backbone
import numpy as np
import  time

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("-t", "--tableName",help="input table copied from gDoc", default='loss-bbp153-table2.txt')


    parser.add_argument("-o", "--outPath",
                        default='out/',help="output path for plots and tables")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    args.prjName='plLoss'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
#............................
#............................
class Plotter_Design(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)

#............................
    def val_err(self,xV,yV,eV=0,figId=21,tit='',ytit=''):
        nrow,ncol=1,1
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(5,3))

        ax=self.plt.subplot(nrow,ncol,1)
        #print(xV,yV,eV)
        ax.errorbar(xV, yV, yerr=eV, fmt='o')
        ax.grid(True)
        ax.set_ylim(0,)
        ax.set(xlabel='num probes',ylabel=ytit,title=tit)

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
def readTableA(inpF,ncol):
    fd=open(inpF,'r')
    data=fd.readlines()
    fd.close()
    npt=len(data)
    nrow=npt//ncol
    print('data size=',npt,' nrow=',nrow)
    assert npt==ncol*nrow
    data=[x[:-1] for x in data]  # get rid of \n for each line
    # unpack rows
    nprob=[]; avrLoss=[];stdLoss=[];convFrac=[];trainTime=[]
    for i in range(nrow):
        j=i*ncol
        x=data[j+0]
        #print(j,i,x,x[-2:])

        assert x[-2:]=='pr' # check start of the row
        npr=int(x[:-2])
        print(i,npr)
        nprob.append(npr)
        avrLoss.append(float(data[j+2]))
        stdLoss.append(float(data[j+3]))
        trainTime.append(float(data[j+5]))

    return np.array(nprob), np.array(avrLoss), np.array(stdLoss), np.array(trainTime)
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

nprob,avrLoss,stdLoss,trainTime=readTableA(args.tableName,ncol=6)
plot=Plotter_Design(args)
tit='data='+args.tableName
plot.val_err(nprob,avrLoss,stdLoss,tit=tit,ytit='MSE-loss*1000',figId=10)
plot.val_err(nprob,trainTime,tit=tit,ytit='avr train time (min)',figId=11)
plot.display_all()  
