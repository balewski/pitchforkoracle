#!/usr/bin/env python3
""" 
Ontra3-traing , uses Horovod, runs on 30-60 GPus

* * *  CPU execution  (~20 x slower vs. GPU)

Cori+InpGenOntra CPUs, 1 node, Ontra3:
./train_CellSpike.py   --localSamples 30000 --noHorovod  

Ontra4:
./train_CellSpike.py   --localSamples 30000 --noHorovod   --dataPath /global/cfs/cdirs/m2043/balewski/neuronBBP-pack8kHzRam/probe_4prB8kHz/ontra4/etype_excite_v1 --probeType excite_4prB8kHz  --design a2f791f3a_ontra4 

For Roy: traing on variable number of probes for 4 cells, up to 67 probes
./train_CellSpike.py   --localSamples 30000 --noHorovod   --dataPath /global/cfs/cdirs/m2043/balewski/neuronBBP-pack8kHzRam/probe_67pr8kHz/etype_excite_v1/  --probeType  excite_67pr8kHz   --design a2f791f3a_ontra4 --cellName bbp153 --numFeature 67


./train_CellSpike.py   --localSamples 30000 --noHorovod   --dataPath /global/cfs/cdirs/m2043/balewski/neuronBBP-pack8kHzRam/probe_67pr8kHz/etype_inhibit_v1/  --probeType  inhibit_67pr8kHz   --design a2f791f3a_ontra3 --cellName bbp027 --numFeature 67


OR 1-cell training
./train_CellSpike.py   --localSamples 30000 --cellName bbp012 --noHorovod

May need:
export HDF5_USE_FILE_LOCKING=FALSE

* * *   GPU  executions, interactive
ssh cori 
module load esslurm
module load tensorflow/gpu-2.1.0-py37

salloc -N1  -C gpu -n1 -c 10 --gres=gpu:1  -t2:00:00 

2 GPUs:
salloc -C gpu -N 1 -t 4:00:00 -c 10 --gres=gpu:2 --ntasks-per-node=2 
srun -n2   bash -c '  nvidia-smi -l 3 >&L.smi_`hostname` & python -u  ./train_CellSpike.py   --localSamples 350000 '

srun -n1 bash -c '  nvidia-smi '
OR 1-NODE:
salloc -N1 -C gpu --ntasks-per-node=8 -c 10  --gres=gpu:8  --exclusive -t4:00:00


Summit:
module load cuda
module load ibm-wml-ce

Graphcore on AzureVM:
gc-docker  -- -u balewski  -it --volume /home/balewski/pitchforkOracle:/pitchforkOracle --volume /datadrive/balewski/ontra4b:/ontra4b  balewski/poplar-1.3.0:v2  bash

Ontra4b on Graphcore:
cd /pitchforkOracle
./train_CellSpike.py   --localSamples 30000 --noHorovod   --dataPath /ontra4b --probeType excite2_4prB8kHz  --design a2f791f3a_ontra4 

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import sys,os,time
sys.path.append(os.path.abspath("toolbox"))
from Plotter_CellSpike import Plotter_CellSpike
from Deep_CellSpike import Deep_CellSpike
from Util_IOfunc import write_yaml


import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2], help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "--noHorovod", dest='useHorovod',  action='store_false', default=True, help="disable Horovod to run on 1 node on all CPUs")
    parser.add_argument("--designPath", default='./',help="path to hpar-model definition")

    parser.add_argument('--design', dest='modelDesign', default='smt190927_b_ontra3', help=" model design of the network")
    parser.add_argument('--venue', dest='formatVenue', default='prod', choices=['prod','poster'],help=" output quality and layout")
    parser.add_argument("--cellName", type=str, default=None, help="cell shortName , alternative for 1-cell training")
 
    parser.add_argument("--seedWeights",default=None, help="seed weights only, after model is created")

    parser.add_argument("-d","--dataPath",help="path to input",
        default='/global/homes/b/balewski/prjn/neuronBBP-pack8kHzRam/probe_3prB8kHz/ontra3/etype_8inhib_v1') 
                        
    parser.add_argument("--probeType",default='8inhib157c_3prB8kHz',  help="data partition")
    parser.add_argument("--numFeature",default=None, type=int, help="if defined, reduces num of input probes.") 


    parser.add_argument("-o","--outPath", default='out',help="output path for plots and tables")

    parser.add_argument( "-X","--noXterm", dest='noXterm', action='store_true', default=False,  help="disable X-term for batch mode")

    parser.add_argument("-e", "--epochs", type=int, default=5, dest='goalEpochs', help="training  epochs assuming localSamples*numRanks per epoch")
    parser.add_argument("-b", "--localBS", type=int, default=None,  help="training local  batch_size")
    parser.add_argument( "--localSamples", type=int, required=True,  help="samples per worker, it defines 1 epoch")
    parser.add_argument("--initLRfactor", type=float, default=1., help="scaling factor for initial LRdrop")
 
    parser.add_argument("--dropFrac", type=float, default=None, help="drop fraction at all FC layers, default=None, set by hpar")
    parser.add_argument("--earlyStop", type=int, dest='earlyStopPatience', default=20, help="early stop:  epochs w/o improvement (aka patience), 0=off")
    parser.add_argument( "--checkPt", dest='checkPtOn', action='store_true',default=True,help="enable check points for weights")

    parser.add_argument( "--reduceLR", dest='reduceLRPatience', type=int, default=5,help="reduce learning at plateau, patience")

    parser.add_argument("-j","--jobId", default=None, help="optional, aux info to be stored w/ summary")
       
    args = parser.parse_args()
    args.train_loss_EOE=False #True # 2nd loss computation at the end of each epoch
    args.shuffle_data=True # use False only for debugging

    args.prjName='cellSpike'
    args.dataPath+='/'
    args.outPath+='/'

    if not args.useHorovod:
        for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))

    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
deep=Deep_CellSpike.trainer(args)

if deep.myRank==0:
    plot=Plotter_CellSpike(args,deep.metaD  )

# delay ranks to avoid RAM congestion
#time.sleep(deep.myRank*10) # rarely needed, should use localRank !!!

deep.init_genetors() # before building model, so data dims are known
try:
    deep.build_model()
except:
    print('M: deep.build_model failed') #probably HPAR were pathological
    exit(0) 

if deep.myRank==0 and args.goalEpochs >10: deep.save_model_full() # just to get binary dump of the graph

if args.seedWeights=='same' :  args.seedWeights=args.outPath
if args.seedWeights:
    deep.load_weights_only(path=args.seedWeights)

if deep.myRank==0:
    sumF=deep.outPath+deep.prjName+'.sum_train.yaml'
    write_yaml(deep.sumRec, sumF) # to be able to predict while training continus

deep.train_model()

if deep.myRank>0: exit(0)

deep.save_model_full()

try:
    plot.train_history(deep,figId=10)
except:
    deep.sumRec['plots']='failed'
    print('M: plot.train_history failed')
    
write_yaml(deep.sumRec, sumF)

plot.display_all('train')


