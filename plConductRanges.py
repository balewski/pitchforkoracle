#!/usr/bin/env python3
""" 
overaly ranges of conductances for two data streams

Ontra4:
./plConductRanges.py    --dataPath /global/cfs/cdirs/m2043/balewski/neuronBBP-pack8kHzRam/probe_4prB8kHz/ontra4/etype_excite_v1 --probeType excite_4prB8kHz 

Ontra3:
   --dataPath v/global/homes/b/balewski/prjn/neuronBBP-pack8kHzRam/probe_3prB8kHz/ontra3/etype_8inhib_v1 --probeType 8inhib157c_3prB8kHz
 

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import sys,os
sys.path.append(os.path.abspath("toolbox"))

from Deep_CellSpike import Deep_CellSpike
from Plotter_Backbone import Plotter_Backbone

import numpy as np
import  time
import ruamel.yaml  as yaml
from pprint import pprint

from InpGenOntra_CellSpike import  CellSpike_input_generator

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2], help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("-d", "--dataPath",help="path to input",
                        default='/global/cfs/cdirs/m2043/balewski/neuronBBP-pack8kHzRam/probe_4prB8kHz/ontra4/etype_excite_v1/')
    parser.add_argument("--probeType",default='excite_4prB8kHz',  help="probe partition")

    parser.add_argument("--dom",default='train', help="domain is the dataset for which predictions are made, typically: test",choices=['train','test','val','witness'])

    parser.add_argument("-o", "--outPath", default='out/',help="output path for plots and tables")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm', action='store_true', default=False, help="disable X-term for batch mode")

    parser.add_argument("-n", "--localSamples", type=int, default=30000, help="samples to read")

    args = parser.parse_args()
    args.prjName='testIG'
    args.dataPath+='/'
    args.outPath+='/'
    args.modelDesign='fake1'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
#............................
#............................
class Plotter_Conduct(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)

   #...!...!....................
    def conductHisto(self,U1,U2,parNameL,figId=7,title=''):

        if len(parNameL)==15:
            nrow,ncol=3,5
        else:
             nrow,ncol=4,5
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(11,7.5))

        binsY=np.linspace(-1.1,1.1,40)
        for j,name in enumerate(parNameL):
            u1=U1[:,j]; u2=U2[::4,j];
            ax=self.plt.subplot(nrow,ncol,j+1)
            ax.hist(u2,binsY, orientation="horizontal")
            ax.hist(u1,binsY,histtype=u'step',linewidth=2., orientation="horizontal")
            ax.set(title='%d:%s'%(j,name),ylabel='truth (a.u.)')

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

deep=Deep_CellSpike.predictor(args)
parNameL=deep.metaD['parName']
print('parNameL=',parNameL)

plot=Plotter_Conduct(args)

dom=args.dom
opaque=['practice','witness'][0]

genConf={}
genConf['cellList']=deep.metaD['cellSplit']['practice']
genConf['name']='IG-'+dom+'-1'
genConf['domain']=dom
genConf['h5nameTemplate']=deep.metaD['h5nameTemplate']

genConf['myRank']=10
genConf['localBS']=128
genConf['numLocalSamples']=args.localSamples
genConf['dataPath']=deep.dataPath
genConf['numTimeBin']=deep.metaD['numTimeBin']
genConf['numFeature']=deep.metaD['numFeature']
genConf['numPar']=deep.metaD['numPar']
genConf['shuffle']=True  # use False for reproducibility
genConf['x_y_aux']=False

pprint(genConf)
#.... creates data generator 1
inpGen=CellSpike_input_generator(genConf,verb=1)

#.... creates data generator 2
genConf['cellList']=deep.metaD['cellSplit']['witness']
#genConf['cellList']=['bbp176']
genConf['name']='IG-'+dom+'-one-cell'
inpGen2=CellSpike_input_generator(genConf,verb=1)

steps=max(1,args.localSamples//genConf['localBS'])
mxSteps=inpGen.__len__()
if 1: # restrict to 1 epoch, no data repetition
    steps=min(mxSteps,steps)

print('\nM:start processing dom=',dom,' localSamples=',args.localSamples,' steps=',steps, ' mxSteps/epoch=',mxSteps,'localBS=',genConf['localBS'])

time.sleep(5) # too see memory usage, tmp
startT0 = time.time()
uL=[]; uL2=[]
for i in range(steps):
    x,u,aux =inpGen.__getitem__(i)
    x,u2,aux =inpGen2.__getitem__(i)
    uL.append(u)
    uL2.append(u2)

print('batch shape x:',x.shape,'u:',u.shape)

# add all steps 
U=np.concatenate(tuple(uL),axis=0)
U2=np.concatenate(tuple(uL2),axis=0)
print('got U avr,std:',U.shape)

plot.conductHisto(U,U2,parNameL)

plot.display_all('conduct')  

exit(1)

avrU=np.mean(U,axis=0)
stdU=np.std(U,axis=0)
parNL=deep.metaD['parName']

for i in range(avrU.shape[0]):
    print("idx=%d  avr=%.3f  std=%.3f  %s"%(i, avrU[i],stdU[i],parNL[i]))









