#!/usr/bin/env python
import sys
import numpy as np

''' 
graph operations, based on
https://networkx.github.io/documentation/stable/tutorial.html

Examples:
https://networkx.github.io/documentation/stable/auto_examples/index.html

'''

import sys

import matplotlib.pyplot as plt
import networkx as nx
import sys,os
#from networkx.drawing.nx_agraph import graphviz_layout
import ruamel.yaml  as yaml


import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("-s", "--shortName",help="bbpNNN name for output graph", default='bbp153')
    parser.add_argument("-o", "--outPath", default='out/',help="output path for plots and tables")
    parser.add_argument("-i", "--inpPath", default='nxdata/',help="input path for roy's files")
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,
        help="disable X-term for batch mode")
    args = parser.parse_args()
    #args.prjName='cellSpike'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    for xx in [ args.inpPath, args.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            exit(99)

    return args


#=================================
#=================================
#  M A I N
#=================================
#=================================
args=get_parser()
grfF=args.inpPath+args.shortName+'.nx.yaml'

print('read graph:',grfF)

with open(grfF, 'r') as ymlFd :
    bulk=yaml.load( ymlFd, Loader=yaml.CLoader)
    G=bulk['nxgraph']
    xy_pos=bulk['xy_pos']
    probeInfo=bulk['probeInfo']

print("is_tree: %s" % nx.is_tree(G))
print("density: %s" % nx.density(G))
if  nx.is_tree(G):
    Gu=nx.to_undirected(G)
    print("radius: %d" % nx.radius(Gu))
    print("diameter: %d" % nx.diameter(Gu))
    print("eccentricity(0 sm): %s" % nx.eccentricity(Gu,v='0 sm'))
    #The eccentricity of a node v is the maximum distance from v to all other nodes in G.
    print("center: %s" % nx.center(Gu))
    print("periphery count: %s" % len(nx.periphery(Gu)))
    

'''The line graph of a graph G has a node for each edge in G and an edge joining those nodes if the two edges in G share a common node. 
G=nx.line_graph(G)  
'''

if args.noXterm: exit(0)


ax = plt.subplot(1,1,1)
#nx.draw(G,node_size=10)
#nx.draw(H, with_labels=True,pos=pos,node_size=10,font_size=7)

#a) draw all at once:
nx.draw(G,pos=xy_pos,node_size=10,font_size=7)

#b) draw firts graph then labels
labTxtIter = nx.draw_networkx_labels(G,xy_pos)

# set colors for node labels
bodyCol={'soma':'g','axon':'b','dend':'m','apic':'r'}

for nn,t in labTxtIter.items():
    bdk=nn[:4]
    #print(nn,bdk)
    ts=1
    if nn in probeInfo:
        ts=7
        idx,d2s=probeInfo[nn]
        t.set(text='%.1f %s'%(d2s,nn))
    t.set(rotation=20,size=ts,color=bodyCol[bdk])
      
numNode= G.number_of_nodes()
numEdge=G.number_of_edges()
txt='cell:%s   nodes:%d   edges:%d'%(args.shortName,numNode,numEdge)
print('end:', txt)
ax.text(-0.05,1.05,txt,transform=ax.transAxes,color='b')

'''
i=0 # dump all nodes
for n in G:
    print(i,n); i+=1
'''

#edge_labels = nx.get_edge_attributes(G,'e')
#print('EL',edge_labels)
#nx.draw_networkx_edge_labels(G, pos,labels = edge_labels) # edge attribute+ID
#nx.draw_networkx_edge_labels(G, pos, edge_labels)  # only edgeID
print('plot.show')
plt.show()
