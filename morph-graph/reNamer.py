#!/usr/bin/env python
import sys
import csv

#............................
def read_one_csv(fname,delim=','):
    print('read_one_csv:',fname)
    tabL=[]
    with open(fname) as csvfile:
        drd = csv.DictReader(csvfile, delimiter=delim)
        print('see %d columns'%len(drd.fieldnames),drd.fieldnames)
        for row in drd:
            tabL.append(row)

    print('got %d rows \n'%(len(tabL)))
    #print('LAST:',row)
    return tabL,drd.fieldnames

#=================================
#=================================
#  M A I N 
#=================================
#=================================
tabProp,_=read_one_csv('props.txt')
propL=[]
for row in tabProp:
    x=row['roy_prop_name']
    #print(x)
    propL.append(x[:-14])
    #break
print(propL)

tabRoy,keyL=read_one_csv('../web-generator/2019_master3.csv','\t')

cnt={'inp':0,'prod1':0,'prod2':0}
cellL=[]
# add prod-info to each Roy record
for row in tabRoy:
    #print('got',row)
    cnt['inp']+=1
    bbpN=row['bbp_name']
    shortN=row['short_name']
    nPr=int(row['num_prob'])
    aaa=row['bbp_name'][:-5]
    aaa=aaa.replace('int','')
    #print('check aaa',aaa)
    assert aaa in propL
    
    #if nPr==67: continue
    cnt['prod1']+=1
    #print(shortN)
    print('./rdMorph2Graph.py -s %s -y %s_1_props.yaml'%(shortN,aaa))
    
    continue
    outPath='meta/'+shortN
    print('mkdir '+outPath)
    print('cp -rp ~/prjn/neuronBBP-data_67pr/%s/meta.cellSpike.yaml %s'%(shortN,outPath))

    continue
    ok88

    cellL.append(shortN)
    #- - -  PUBLISHING
    #print('# <p><hr> <p> %s &nbsp; &nbsp; &nbsp; %s <br> <img src="%s/cellSpike_predict_f109.png"> <hr>'%(shortN,bbpN,shortN)) ;    continue
    
    #- - - - -  REPACKING
    #print('./postproc_oneSimu.sh %s %s %s '%(shortN,bbpN,simJid)) ; continue

    #- - - - -  multi-formatting
    # ./postproc_oneSimu.sh bbp109 L5_BP_dSTUT214_1 27301979,27308458,27246954 17  4K
    #print('./postproc_oneSimu.sh %s %s %s 67  6K'%(shortN,bbpN,','.join(simJidL)))
    
    #- - - - -  multi-training

    #if cnt['inp']>=50 : break
    continue
    # ---- PREDICTING
    outP=out0+shortN
    print('mkdir ',outP)        
    print(' srun -n1 ./predict_CellSpike.py --design 100165_693 -n 5000 --noXterm --venue poster --seedModel /global/cscratch1/sd/balewski/neuronBBP/%s/out/   --dataPath /global/cscratch1/sd/balewski/neuronBBP-packed1/%s/packed17pr/ --outPath %s'%(mlJid,shortN,outP) )
    print('wait\n')
    print('chmod a+rx -R ',outP)
    

    break
print('\ncnt',cnt, len(cellL))

#print(' '.join(cellL))

#<img src="cellSpike_predict_f109.png">


# srun -n1 ./predict_CellSpike.py --design 100165_693 -n 5000 --noXterm --venue poster --seedModel /global/cscratch1/sd/balewski/neuronBBP/354782/7/out/   --dataPath /global/cscratch1/sd/balewski/neuronBBP-play/bbp004/packed17pr/

