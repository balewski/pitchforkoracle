#!/usr/bin/env python
""" 
produce training data
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from pprint import pprint
import sys,os
import networkx as nx
sys.path.append(os.path.abspath("../"))
from Util_IOfunc import read_yaml
import copy

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("-s", "--shortName",help="bbpNNN name for output graph", default='bbp153')
    parser.add_argument("-y", "--yamlName",help="from Roy, graph info from NEURON", default='props_L5_TTPC1_cADpyr.yaml')

    parser.add_argument("-o", "--outPath", default='nxdata/',help="output path for plots and tables")
    parser.add_argument("-i", "--inpPath", default='/global/cfs/cdirs/m2043/roybens/cell_props_06_29_20/',help="input path for roy's files")

    
    # old: /global/cfs/cdirs/m2043/roybens/cell_props/
    args = parser.parse_args()
    #args.prjName='cellSpike'
    # on my laptop VM
    args.inpPath='cell_props_06_29_20/'

    
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    for xx in [ args.inpPath, args.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            exit(99)

    return args

#...!...!..................
def clipName(txt):
    xL=txt.split('[')
    #print('www',txt,'\n',xL)
    idx=xL[1][:-1]
    name='%s_%d'%(xL[0],int(idx))
    
    return name

#...!...!..................
def compute_pos(G):
    from networkx.drawing.nx_agraph import graphviz_layout
    prog='dot'
    #prog='twopi'
    #prog='circo'
    #prog='neato'
    pos=graphviz_layout(G,prog=prog)
    return pos

#...!...!..................
def verify_probes(probeV,probeR):
    iOff=0
    print('roy L=%d, vaya L=%d'%(len(probeR),len(probeV)),probeV[0])

    #for i,v in enumerate(probeR):  print(i,v)
    # check uniquness, create another list of the duplicates
    l=probeR
    dupS=set([x for x in l if l.count(x) > 1])
    if len(dupS)>0:
        print('Roy has %d duplicates'%len(dupS),dupS)
        bad_dup
    nSkip=0
    # drop missing Vyassa probes
    probeR2=copy.copy(probeR)
    i=0
    for k in probeR2:
        a=k.split('.')[1]
        a2=clipName(a)
        #print(i,a2,probeV.index(a2),probeV[i])
        #assert a2==probeV[i]
        #i+=1
        if a2 in probeV: continue
        nSkip+=1
        probeR.pop(k)
    print('after roy L=%d, nSkip=%d'%(len(probeR),nSkip))

    for i,k in enumerate(rec_points):
        a=k.split('.')[1]
        b=probeL[i+iOff]
        a2=clipName(a)
        #print(i,'roy vyassa:',a2,b, iOff)
        if   a2!=b:
            iOff-=1
            continue
        assert a2==b
    print('done , iOff=',iOff)
        

#=================================
#=================================
#  M A I N
#=================================
#=================================
args=get_parser()

#metaF='/global/homes/b/balewski/prjn/neuronBBP-data_67pr/'+args.shortName+'/meta.cellSpike.yaml'
metaF='old/neuronBBP-morphGraph/meta/'+args.shortName+'/meta.cellSpike.yaml'
metaD=read_yaml(metaF)
probeL=metaD['rawInfo']['probeName'] 

G=nx.DiGraph()

inpF=args.inpPath+'/'+args.yamlName
grfF=args.outPath+'/'+args.shortName+'.nx.yaml'

blob=read_yaml(inpF)
#pprint(blob)
rec_points=blob.pop('rec_points')
rec_distances=blob.pop('rec_distances')
segmL=sorted(blob)
print(inpF, 'numSegm=',len(segmL),'numDist=',len(rec_distances),len(rec_points))

print('probe count:', len(rec_points) , len(probeL))
#assert len(rec_points) == len(probeL)
probeL[0]=probeL[0]+'_0'  # for consistency w/ Roy
verify_probes(probeL,rec_points)

probeInfo={}
for i,[a,d] in enumerate(rec_distances):    
    segmN=a.split('.')[1]
    #print(i,a,d)
    #if 'soma' in segmN: bbb
    segmN=clipName(segmN)
    probeInfo[segmN]=[i,float('%.1f'%d)]
    #print(i,segmN,d)
                 
armL=['axon','apic','dend','soma']
cnt={'inp':0}
for x in armL: cnt[x]=0

for i,k in enumerate(segmL):
    rec=blob[k]
    #print('M:ii',i,k)
    segmN=k.split('.')[1]
    cnt['inp']+=1
    for x in armL: 
        if x in segmN: cnt[x]+=1

    segmN=clipName(segmN)

    if 'Children' in rec: 
        childL=rec['Children']
        # remove full cell name
        childL=[ x.split('.')[1] for x in childL]
        childL=[ clipName(x) for x in childL]
    else:
        childL=[]
    assert segmN not in childL    
    
    #pprint(rec);
    #print(segmN,featD)    
    parentN=rec['Parent']
    if parentN!='None':
        parentN=parentN.split('.')[1].split('(')[0]
        parentN=clipName(parentN)
    else:
        parentN=None
        
    # node features
    fCM=int(rec['CM'])
    fD=float('%.2f'%rec['Diameter'])
    fL=float('%.1f'%rec['Length'])
    fS=int(rec['NSeg'])

    if segmN in probeInfo:
        idx,d2s=probeInfo[segmN]
    else:
        idx=d2s=-1
        
    # increment graph content
    G.add_node(segmN,CM=fCM, Diameter=fD,Length=fL,NSeg=fS,probeIdx=idx,dist2soma=d2s)
    if parentN!=None: G.add_edge(parentN,segmN)
    for x in childL:
        G.add_edge(segmN,x)

xy_pos=compute_pos(G)
cellFullName=k.split('.')[0]
print('cellFullName=',cellFullName,inpF)
outD={'nxgraph':G,'xy_pos':xy_pos}
outD['probeInfo']=probeInfo
outD['probeName']=probeL
nx.write_yaml(outD,grfF)
print('counter',cnt,'len(G)=',len(G),'save=',grfF)


