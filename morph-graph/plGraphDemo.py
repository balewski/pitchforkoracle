#!/usr/bin/env python
# example reading morphology graph for 1 cell
import networkx as nx
import ruamel.yaml  as yaml
import matplotlib.pyplot as plt

#inpPath='/global/cfs/cdirs/m2043/balewski/neuronBBP-morphGraph/nxdata/'
inpPath='./nxdata/'
shortName='bbp004'
grfF=inpPath+shortName+'.nx.yaml'

print('read graph:',grfF)
with open(grfF, 'r') as ymlFd :
    bulk=yaml.load( ymlFd, Loader=yaml.CLoader)
    G=bulk['nxgraph']
    xy_pos=bulk['xy_pos']
    probeInfo=bulk['probeInfo']
    
numNode= G.number_of_nodes()
print('G numNode=',numNode,'edges:',G.number_of_edges())

print('\nsome node properties:')
i=0
for n in G:
    print(n,G[n],'attr=',G.nodes[n])
    if n in probeInfo:
        print('  is a probe, idx,dist2soma:',probeInfo[n])
    i+=1
    if i>10 : break


print('\nsome edges:')
i=0
for e in G.edges():
    print(e)
    i+=1
    if i>10 : break
    

print('just plotting')
nx.draw(G,pos=xy_pos,node_size=10, with_labels=True)
plt.show()

# Nore, the plot will look scrambled, you need to use better tool to see neuron structure, but this code is hard to install on Cori. It is easy on a laptop.
#see below

from networkx.drawing.nx_agraph import graphviz_layout
pos=graphviz_layout(H,prog='dot')
nx.draw(G,node_size=10,pos=pos)
