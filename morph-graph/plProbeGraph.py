#!/usr/bin/env python

''' 
plot probes location on the graph
'''

import sys

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import sys,os
#from networkx.drawing.nx_agraph import graphviz_layout
import ruamel.yaml  as yaml
#sys.path.append(os.path.abspath("../"))
#from Util_IOfunc import read_yaml


import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("-s", "--shortName",help="bbpNNN name for output graph", default='bbp004')
    parser.add_argument("-o", "--outPath", default='out/',help="output path for plots and tables")
    parser.add_argument("-i", "--inpPath", default='/global/homes/b/balewski/prjn/neuronBBP-morphGraph/',help="input path for NX-yaml files")
    parser.add_argument("-k", "--numProbe", default=11,type=int,help="number of probes")
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,
        help="disable X-term for batch mode")
    args = parser.parse_args()

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    for xx in [ args.inpPath, args.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            exit(99)

    return args


#=================================
#=================================
#  M A I N
#=================================
#=================================
args=get_parser()
grfF=args.inpPath+args.shortName+'.nx.yaml'

print('read graph:',grfF)
with open(grfF, 'r') as ymlFd :
    bulk=yaml.load( ymlFd, Loader=yaml.CLoader)
    G=bulk['nxgraph']
    xy_pos=bulk['xy_pos']
    probeInfo=bulk['probeInfo']
    probeL=bulk['probeName']

assert args.numProbe<=len(probeL)

probeL=probeL[:args.numProbe]
print('considred probes:',probeL)

#------ graph partition in to : apic,axon and everything else
numNode= G.number_of_nodes()
print('G numNode=',numNode,'edges:',G.number_of_edges())
print('\nG any arms:',len(G['soma_0']))

# for now only deal with axon and apic separately
arms=["axon_0"]
if "apic_0" in G: arms.append('apic_0')
armsG={}
for arm in arms:
    H=nx.ego_graph(G,arm ,  radius=1000)
    G.remove_edge('soma_0',arm)
    armsG[arm]=H
    numEdge=H.number_of_edges()
    print('arm=%s edges=%d'%(arm,numEdge))
    #break
H=nx.ego_graph(G,'soma_0',  radius=1000)
numEdge=H.number_of_edges()
print('arm=soma_1 edges=%d'%(numEdge))
nx.relabel_nodes(H,{'soma_0':'soma_dend'}, copy=False)
assert 'soma_0' not in H
armsG['soma_dend']=H
arms.append('soma_dend')
xy_pos['soma_dend']=xy_pos['soma_0']
prog='dot'

figId=0
sumFound=0
dist_thr=50

for arm in arms:
    figId+=1
    fig=plt.figure(figId,facecolor='white', figsize=(5,8))
    ax = plt.subplot(1,1,1)
    node_size=2
    H=armsG[arm]
    #pos=graphviz_layout(H,prog=prog)
    nx.draw(H, with_labels=False,pos=xy_pos,node_size=node_size)
    x,y=xy_pos[arm]
    plt.text(x,y+50,s=arm, bbox=dict(facecolor='red', alpha=0.5),horizontalalignment='center')

    # paint nodes from list
    probNL=[] 
    for nn in probeL:
        if nn in H: probNL.append(nn)

    print('\n',arm,'found probNL:',len(probNL))
    for nn in probNL:
        print(nn,probeInfo[nn])
    sumFound+=len(probNL)
    nx.draw_networkx_nodes(H, xy_pos, nodelist=probNL,
                           node_color='r',node_size=node_size*20, alpha=0.8)
    txt="num probe=%d\nfound:%d"%(args.numProbe,len(probNL))
    ax.text(-0.05,-0.05,txt,transform=ax.transAxes,color='b')
    ax.text(-0.05,1.05,args.shortName,transform=ax.transAxes,color='b')
    #break
print('qqq',len(probeL),sumFound)
#assert len(probeL)==1+sumFound
plt.show()

