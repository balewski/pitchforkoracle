# pitchforkOracle

Hodgkin-Huxley  models represent the biophysical characteristic of cell membranes.

*) formater is reading traces for many stimuli for common training

*) trainer:
- is capable of hype-param tunning
- epochs are computed from requested --trainTime (sec)

*) predictor:

./predict_CellHH.py --seedModel /global/cscratch1/sd/balewski/cellHH2b/9519992-4/out_45/ --seedWeights same

= = = = = =  Major code updates = = = 
ver 2f 
- change data IO from yaml to HD5
- drop '-1.5' when rescaling func in format.py
- change loss from MAE to MSE 
 

ver 2g
-  accept hd5 raw data format (was yaml)

ver 2h
- change input from independent traces per stimuli to RGB-like stacking of traces from different stimuli for fixed set of params
- re-define valid frames : if one of stimuli produces valid trace keep all traces
- model: drop 2nd input, save only func_min in Aux
- drop stimId from training params

ver 2i 
INPUT -> [ CONV -> RELU -> POOL]*3 -> [FC -> DROP->RELU]*3 -> FC

ver 2j
*Reduce CNN kernel from 7 to 3
*Drop weights limit constraint for CNN
INPUT -> [ CONV -> RELU -> CONV -> RELU -> POOL]*3 -> [FC -> DROP->RELU]*3 -> FC

ver 2k
 - rescale U’s to range <1, (drop U4 exceptional treatment)

= = = = = = = = = =

ver 3
 - split data on 10 k-folds, use 6 for the training
  -Us (true) params, normalize to range [-1,1] , linear transform
  -Traces (voltages) , 
  -- Add white noise of 0.2 mV (in data-format stage)
  -- Normalize to [0.05,0.9] w/ nonlinear transform : log(V+v0)/fact
  - Drop frames if any of Us is below -3.5, accept U in [-3.5,+4]
    frame loss of 25%;  0.94^5=0.72
  - Require now 1 good-QA traces for valid frame 

Freez ML model to be:
INPUT -> [ CONV -> RELU -> CONV -> RELU -> POOL]*4 -> [FC -> DROP->RELU]*4 -> FC

With the following dimensions:
Input: 5
1D CNN layers:  [8, 16, 24, 32]  
FC layers: [25, 25, 25, 25]
Optimizer : ADAM
Loss : MSE
