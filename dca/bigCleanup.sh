#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;  #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

#cellL="   bbp001  bbp002  bbp003  bbp004  bbp005 bbp006 bbp007 bbp008 bbp009 bbp010 bbp011 bbp012 bbp013"
cellL="  bbp014  bbp017 bbp018 bbp019 bbp020 bbp023 bbp024 bbp025 bbp026 bbp027 bbp028 bbp031 bbp032 bbp034 bbp035 bbp036 bbp037 bbp038 bbp040 bbp041 bbp042 bbp043 bbp045 bbp046 bbp047 bbp048 bbp049 bbp050 bbp051 bbp052 bbp053 bbp054 bbp055 bbp056 bbp057 bbp060 bbp061 bbp062 bbp063 bbp066 bbp067 bbp068 bbp069 bbp070 bbp071 bbp072 bbp075 bbp076 bbp077 bbp078 bbp079 bbp080 bbp081 bbp082 bbp083 bbp084 bbp086 bbp087 bbp088 bbp089 bbp090 bbp091 bbp092 bbp093 bbp094 bbp095 bbp096 bbp097 bbp098 bbp099 bbp100 bbp101 bbp102 bbp106 bbp107 bbp108 bbp109 bbp111 bbp112 bbp113 bbp114 bbp115 bbp118 bbp119 bbp120 bbp121 bbp122 bbp124 bbp125 bbp126 bbp127 bbp128 bbp129 bbp132 bbp133 bbp134 bbp135 bbp136 bbp139 bbp140 bbp141 bbp142 bbp143 bbp144 bbp145 bbp146 bbp147 bbp148 bbp149 bbp150 bbp151 bbp152 bbp153 bbp154 bbp155 bbp156 bbp159 bbp160 bbp161 bbp162 bbp164 bbp165 bbp166 bbp167 bbp168 bbp171 bbp172 bbp173 bbp174 bbp175 bbp176 bbp179 bbp180 bbp181 bbp182 bbp185 bbp186 bbp187 bbp188 bbp189 bbp192 bbp193 bbp194 bbp195 bbp196 bbp197 bbp198 bbp199 bbp200 bbp201 bbp202 bbp203 bbp204 bbp205 bbp206 bbp207 "


inpPath0=/global/cscratch1/sd/balewski/neuron-DCA_one_2020
outPath0=${inpPath0}_sum

skipPath0=/global/cscratch1/sd/balewski/neuron-DCA_one_2020_partial
k=0
ns=0
for cellN in $cellL; do
    inpPath=${inpPath0}/$cellN
    nJob=` ls $inpPath|wc -w`
    echo $k work on  cell: $cellN  nJob=$nJob
    txt=''
    okJob=0
    for entry in `ls $inpPath`; do
	#echo $entry
	arrPath=${inpPath}/$entry
        nArr=` ls $arrPath|wc -w`
	echo nArr=$nArr  jid=$entry
	#break
	mSum=-1
	nBad=0
	for iarr in `ls $arrPath `; do
	    sumPath=$arrPath/${iarr}/out
	    nSum=`ls $sumPath/*sum.yaml| wc -w `
	    #echo iarr=$iarr nSum=$nSum
	    if [ $mSum -lt 0 ] ; then
		mSum=$nSum
	    fi
	    if [ $mSum -ne $nSum ] ; then
		echo mismatch $entry for $cellN   $nArr  sum  $mSum -ne $nSum $sumPath/'*sum.yaml'
		ns=$[ $ns + 1 ]
		nBad=$[ $nBad + 1]
	    fi
	done
	echo end  jid=$entry nBad=$nBad $cellN
	if [ $nBad -gt 0 ]; then
	    skipPath=${skipPath0}/$cellN
	    mkdir -p $skipPath
	    mv  $arrPath $skipPath
	    continue # skip this job
	fi
	txt+=' '${entry}/0-9
	okJob=$[ $okJob + 1 ]
    done
    if [ $okJob -lt 1 ] ; then
	echo no jobs for cell $cellN skip
	continue
    fi
	
    #continue
    # data agregation starts
    outPath=${outPath0}/$cellN/
    mkdir -p $outPath
    logF=${outPath}/log.txt

    echo process  -N $cellN -s $txt
    time (./sumDca4Score.py --outPath $outPath -N $cellN -s $txt -X  >& $logF)
    grep '\#sum' $logF
    #echo ./sumDca4Score.py --outPath $outPath -N $cellN -s $txt -X 
    k=$[ $k + 1 ]
    #break
done
echo done $k jobs  , nSkip=$ns

exit

# - - - - - - - - - - - - - - - --  -
excitatoty only
cellL=" bbp054  bbp098  bbp102  bbp152  bbp153  bbp154  bbp155  bbp156  bbp176  bbp205  bbp206  bbp207 "

all cells
inpPath=/global/cscratch1/sd/roybens/neuron-DCA_one_2020a/

SKIP
	    ls $arrPath
	    skipPath=${skipPath0}/$cellN
	    mkdir -p $skipPath
	    mv  $arrPath $skipPath
skipPath0=/global/cscratch1/sd/balewski/neuron-DCA_one_2020_skip


	if [ $mArr -lt 0 ] ; then
	    mArr=$nArr
	else
	    if [ $mArr -ne $nArr ] ; then
		echo mismatch $entry for $cellN   $mArr  $nArr 
		ns=$[ $ns + 1 ]
	    fi
	fi
