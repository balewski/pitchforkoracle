#!/usr/bin/env python3
import sys,os
'''
compact Z+DCA  for production
https://github.com/BouchardLab/DynamicalComponentsAnalysis
Jesse: should behave similarly to a scikit-learn model (fit, transform, model.coef_).

See details at:
https://github.com/BouchardLab/DynamicalComponentsAnalysis/blob/master/dca/dca.py

module load tensorflow/gpu-2.2.0-py37 
export HDF5_USE_FILE_LOCKING=FALSE
export PATH=/global/homes/b/balewski/.local/cori/gpu-tensorflow2.2.0-py37/bin:$PATH

Answer, Cori login node
generate: nFrames=10,nTbin=333,nFeat=5
X (10, 333, 5)
est cov ...
done 0.0 sec
fit d=3 ...
d=3 score=0.187  time =217.9 sec

'''

import numpy as np
import  time
from dca import DynamicalComponentsAnalysis as DCA

def noise_dataset(nFrames=1,nTbin=333,nFeat=10):
    print('generate: nFrames=%d,nTbin=%d,nFeat=%d'%(nFrames,nTbin,nFeat))
    X = np.random.randn(nFrames,nTbin,nFeat)  # swap last 2 indices
    return X

def main():
    
    #nFrames=10 ; nFeat=7 ;  nTbin=333; T=10; d=3 # runs reasonable
    nFrames=100 ; nFeat=67 ;  nTbin=1600; T=16 # the NEURON data
    
    X = noise_dataset(nFrames,nTbin,nFeat)
    print('X',X.shape,'T=',T)

    model = DCA( T=T, block_toeplitz=True, n_init=5)
    #model.fit(X) # two steps at once

    print('est cov ...') ; T0 = time.time()
    model.estimate_data_statistics(X)
    print('done %.1f sec'%(time.time()-T0))
    T0 = time.time()
    for d in range(1,nFeat,5):
        print('fit DCA.d=%d ...'%d)  ;  T1 = time.time()
        model.fit_projection(d=d)
        print('d=%d score=%.3f  time =%.1f sec'%(d,model.score(),time.time()-T1))

    print(' total time =%.1f sec'%(time.time()-T0))
       
    #  different types of output from DCA 
    X2=model.transform(X)
    print('X2',X2.shape)
    print('model attr:',model.__dict__.keys())
    coef=model.coef_
    print('coef :',coef.shape)
    print('coef dump:',coef)
    
    
main()

