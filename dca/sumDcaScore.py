#!/usr/bin/env python
""" 
extract DCA score from sum-files, filter by cell name
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import numpy as np
import  time
import sys,os
sys.path.append(os.path.abspath("../"))
from Util_IOfunc import write_yaml, read_yaml
from Plotter_Backbone import Plotter_Backbone

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")

    parser.add_argument("-s", "--slurmDir", nargs='+',help="path to inputs, a l blank separated list,expands 1234/10-13", default=['32462211/0'])


    parser.add_argument("-o", "--outPath",
                        default='out2/',help="output path for plots and tables")

    parser.add_argument("-N", "--cellName", type=str, default='bbp102', help="cell shortName")
    args = parser.parse_args()
    args.sourcePath='/global/cscratch1/sd/balewski/neuron-DCA4/'
    args.prjName='dcaScore'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args



def expandList(kL):
    kkL=[]
    for x in kL:
        if '-' not in x:
            kkL.append(x) ; continue
        [a,b]=x.split('/')
        print('ab',a,b)
        xL=b.split('-')
        for i in range(int(xL[0]),int(xL[1])+1):
            kkL.append('%s/%d'%(a,i))
    print(kL,'  to ',kkL)
    return kkL

    nK=len(kL)
    assert nK>0



# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
def locateSummaryData(src_dir,prefix,sumD,verb=1):
    src_dir+='/out/'  # for  multi task per job data
    for xx in [ src_dir]:
        if os.path.exists(xx): continue
        print('Aborting on start, missing  dir:',xx)
        exit(1)

    if verb>0: print('use src_dir:',src_dir,'prefix:',prefix)
    jobL=os.listdir(src_dir)
    print('locateSummaryData got %d potential jobs, e.g.:'%len(jobL), jobL[0])
    #print('sub-dirs',jobL)

    # at this point the list of dirs is fixed, now picking of sum.yaml

    nok=0
    for jobId in jobL:
        if prefix  not in jobId : continue
        sumF=src_dir+'/%s/cellDca_%s_777.sum.yaml'%(jobId,prefix)
        try:
            dcaD=read_yaml(sumF,verb=1)
        except:
            print( ' -->missing/failed')
            continue
        nok+=1
        assert prefix==dcaD['cellName']
        dcaScore=dcaD['dcaScore']
        numOutProbe=dcaD['numOutProbe']
        windowLength=dcaD['windowLength']
        #print('zz',dcaD)
        sumD['score'][windowLength][numOutProbe]=dcaScore

        sumD['numFrame']=dcaD['numFrame']
    return nok        
#............................
#............................
#............................
class Plotter_PredictInfoM(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)
        self.cellName=args.cellName
        self.timeBin_msec=0.125  # for 8kHz data        
                
#...!...!..................
    def dcaScore(self,dataD,tit1='',figId=5):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(5,4.5))
        ax = self.plt.subplot(1,1,1)

        '''
        colT={10:'brown',20:'black',40:'fuchsia',150:'red'}
        symT={10:'v',20:'o',40:'D',150:'s'}
        msT={10:10,20:10,40:8,150:9}
        '''
        colT={16:'brown',32:'black',64:'fuchsia'}
        symT={16:'v',32:'o',64:'D'}
        msT={16:8,32:8,64:6}
        
        for winT in dataD['winL']:
            xV=[]
            yV=[]
            recD=dataD['score'][winT]

            for x in sorted(recD):
                xV.append(x)
                yV.append(recD[x])

            lab='%.0f ms [%d bins]'%(winT*self.timeBin_msec,winT)
            ax.plot(xV, yV, alpha=0.7, label=lab,color=colT[winT],marker=symT[winT], markersize=msT[winT])
        tit='cell %s, numFrame %d'%(dataD['cellName'],dataD['numFrame'])
        ax.set(title=tit,xlabel='DCA d (aka output probes)',ylabel='DCA score (aka P.I.)')
        ax.grid()
        ax.legend(loc='upper left',title='DCA T window ')


#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":

    args=get_parser()
    slurmDir=expandList(args.slurmDir)

    sumD={}
    #wL=[10,20,40]  # testA  bbp102
    wL=[64,32,16]  # testB bbp153
    sumD['score']={ T:{} for T in wL}
    sumD['cellName']=args.cellName
    sumD['winL']=wL

    # grid sampling
    nok=0
    for slurmJid in args.slurmDir:
        nok+=locateSummaryData(args.sourcePath+slurmJid, args.cellName,sumD)

    print('nOK=',nok)
    if nok<=0: exit(0)
    for T in wL:
        print(T ,' end  len',len(sumD['score'][T]))
    
    plot=Plotter_PredictInfoM(args)
    plot.dcaScore(sumD)
    
    plot.display_all(args.cellName)
