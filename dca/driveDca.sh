#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

echo 'D:DCA-driver start on '`hostname`' '`date`
taskList=$1
cellName=$2
numFrame=$3

procIdx=${SLURM_PROCID} 
arrIdx=${SLURM_ARRAY_TASK_ID}
nprocspn=${SLURM_NTASKS_PER_NODE}

echo D:my procIdx=$procIdx "pwd: "`pwd`  cellName=$cellName
echo D:RANK_`hostname` $SLURM_PROCID arrIdx=$arrIdx nprocspn=$nprocspn
taskIdx=$[ $nprocspn * $arrIdx + $procIdx ]
echo D:askIdx=$taskIdx 

# this is for handling task-list
maxTask=`cat $taskList |wc -l`
if [ $taskIdx -ge $maxTask ]; then
    echo "D:rank $taskIdx above maxTask=$maxTask, idle ..."; exit 0
fi
line=`head -n $[ $taskIdx +1 ] $taskList |tail -n 1`
myName=${cellName}

echo D:myName=${myName}  dcaArgs=$line
outDir=out/${taskIdx}_$myName
logN=${taskIdx}_${myName}

mkdir -p $outDir
pwd

if [ $SLURM_LOCALID -eq 0 ] ; then
    ( sleep 180; echo; echo "M:TTTTTTTTT_3m";  date; hostname; free -g; top ibn1)&
fi


shardZero=$(( ${taskIdx} % 20  ))
# stagger starts on each node to ease the cold-start IO
sleep $(( 2 + ${SLURM_LOCALID}*4 ))
echo D:start DAC ${taskIdx} at $outDir=$outDir shardZero=$shardZero

python -u ./rdFrames2Dca_8kHz.py --cellName $cellName --numFrame $numFrame --numInpProbe 67 $line --outPath $outDir  --shardZero $shardZero >&log.$logN

echo D:DCA-done-`date`

