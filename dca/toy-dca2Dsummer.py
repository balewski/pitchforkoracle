#!/usr/bin/env python3
import sys,os
'''
compact Z+DCA  for production
https://github.com/BouchardLab/DynamicalComponentsAnalysis
Jesse: should behave similarly to a scikit-learn model (fit, transform, model.coef_).

See details at:
https://github.com/BouchardLab/DynamicalComponentsAnalysis/blob/master/dca/dca.py

module load tensorflow/gpu-2.1.0-py37
export PATH=/global/homes/b/balewski/.local/cori/gpu-tensorflow2.1.0-py37/bin:$PATH


Answer, Cori logn node
generate: nTbin=160,nFeat=7
X (160, 7) T= 6
est cov ...
done 0.0 sec
fit DCA.d=5 ...
d=5 score=5.017  time =104.6 sec
 total time =104.6 sec


'''
import  time
import numpy as np
from dca import DynamicalComponentsAnalysis as DCA

def noise_dataset(nTbin=333,nFeat=10):
    print('generate: nTbin=%d,nFeat=%d'%(nTbin,nFeat))
    X = np.random.randn(nTbin,nFeat)  
    return X

def main():
    nFeat=7 ; nTbin=160 ; T=6 ; d=5  # works quickly , 2 minutes on Haswell
    #nFeat=67 ; nTbin=1600 ; T=64 ; d=5  # fit_projection is stuck
    
    X = noise_dataset(nTbin,nFeat)
    print('X',X.shape, 'T=',T)
    
    model = DCA( T=T, block_toeplitz=True, n_init=5)

    print('est cov ...') ; T0 = time.time()
    #model.estimate_data_statistics(X)
    model.estimate_cross_covariance(X)
    print('done %.1f sec'%(time.time()-T0))
    T0 = time.time()
    #for d in range(1,nFeat,5):
    print('fit DCA.d=%d ...'%d)  ;  T1 = time.time()
    model.fit_projection(d=d)
    print('d=%d score=%.3f  time =%.1f sec'%(d,model.score(),time.time()-T1))

    print(' total time =%.1f sec'%(time.time()-T0))
    
    
main()

