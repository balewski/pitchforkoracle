#!/usr/bin/env python3
import sys,os
'''

Paper: 
https://papers.nips.cc/paper/9574-unsupervised-discovery-of-temporal-structure-in-noisy-data-with-dynamical-components-analysis.pdf

https://github.com/BouchardLab/DynamicalComponentsAnalysis
Jesse: should behave similarly to a scikit-learn model (fit, transform, model.coef_).

For the X =(nSamp, nTimeBin, nFeat), d<nFeat,  T << nTimeBin.
Q: what is the dimension of cov matrix?
A:The covariance has size  (2*T*nFeat)^2


module load tensorflow/gpu-2.2.0-py37 
export HDF5_USE_FILE_LOCKING=FALSE
export PATH=/global/homes/b/balewski/.local/cori/gpu-tensorflow2.2.0-py37/bin:$PATH


Quick test: 
   ./rd40kHz_2_Dca8kHz.py   --numOutProbe 3 --windowLength 10 --cellName bbp153

takes ~2 min on cori-login node
'''

import numpy as np
import time
import torch
from dca import DynamicalComponentsAnalysis as DCA

sys.path.append(os.path.abspath("/global/homes/b/balewski/pitchforkOracle/toolbox/"))
from Util_IOfunc import write_yaml, read_yaml, write_data_hdf5, read_data_hdf5
from Plotter_Backbone import Plotter_Backbone
from matplotlib import cm as cmap

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")
    parser.add_argument("--shardId", default=1, type=int, help="number of data shards to be used")
    parser.add_argument("--numFrame", default=400, type=int, help="number of frames, or all is -1")
    parser.add_argument("--numOutProbe", default=3, type=int, help="number of output (DCA-transformed) probes")
    parser.add_argument('-T',"--windowLength", default=16, type=int, help="number of consecutive time-bins from full time-line")
    
    parser.add_argument("-o","--outPath",
                        default='out/',help="output path for plots and tables")

    parser.add_argument("-N", "--cellName", type=str, default='bbp153',
                        help="cell shortName")
    parser.add_argument("-j","--jobId", default=None,
        help="optional, aux info to be stored w/ summary")

    args = parser.parse_args()
    args.dataPath='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr/'
    args.prjName='cellDca'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
#............................
#............................
class Plotter_Entropy(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)
        
#...!...!..................
    def VoltsSoma(self,F1,figId=100, stim=[], titL=['','']):
        tit1,tit2=titL
        nrow,ncol=4,2
        figId=self.smart_append(figId)
        # Remove horizontal space between axes
        fig,axA = self.plt.subplots(nrow, ncol, sharex=True, sharey=True,num=figId,facecolor='white', figsize=(16,8))
        #fig.subplots_adjust(,hspace=0,wspace=0) ??

        #print('eee',F1.shape)
        mxFr=F1.shape[0]
        nFr=nrow*ncol        
        frL=[ i for i in range(nFr)]
        #print('prL',prL)

        nBin=F1.shape[1]
        maxX=nBin
        xLab='time bins'; yLab='ampl (mV)'
        binsX=np.linspace(0,maxX,nBin)

        for ifr in range(nFr):
            frId=frL[ifr]
            ax=axA[ifr%nrow,ifr//nrow]
            V1=F1[frId]
            # np.random.shuffle(V1) 
            ax.plot(binsX,V1,label='fr%d'%(frId),c='b')
            if len(stim)>0:
                ax.plot(stim*10, label='stim',color='black', linestyle='--',linewidth=0.5)
            ax.grid(linestyle=':')
            ax.legend(loc='upper right', title='probe  idx:name')
            if ifr//nrow==0: ax.set_ylabel(yLab)
            if ifr%nrow==3: ax.set_xlabel(xLab)
            if ifr==0: ax.set_title(tit1)
            #if ifr==nrow: ax.set_title(tit2)

#...!...!..................
def saveSummary(args,score,covTime,fitTime,numTimeBin,dcaCoef,inpF):

    argL=list(args.__dict__.keys())
    for k in ['verb', 'noXterm', 'prjName']: argL.remove(k)
    print(argL)
    
    sumRec={ k:getattr(args,k) for k in argL}
    sumRec['dcaScore']=float(score)
    sumRec['covTime']=covTime
    sumRec['fitTime']=fitTime
    sumRec['numTimeBin']=numTimeBin
    sumRec['inpFile']=inpF
    print(sumRec)
    sumRec['dcaCoef']=np.array(dcaCoef) # this is too much to print
    
    jid=args.shardId
    outF=args.prjName+'_%s_%s.sum.yaml'%(args.cellName,jid)
    write_yaml(sumRec, args.outPath+'/'+outF)

def rebin_data3D(X,nReb):
        nS,tBin,nF=X.shape
        #print('X3D',X.shape,'nReb=',nReb)
        assert tBin==8000 # tested only for original data
        assert tBin%nReb==0
        a=X.reshape(nS,tBin//nReb,nReb,nF)
        b=np.sum(a,axis=2)/nReb
        #print('X2',a.shape,b.shape,b.dtype)
        return b


    
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
plot=Plotter_Entropy(args)
nReb=5

shortN=args.cellName
inpF='%s/%s.cellSpike.data_%d.h5'%(shortN,shortN,args.shardId)
inpD=read_data_hdf5(args.dataPath+inpF)

numInpProbe=inpD['frames'].shape[2]
if numInpProbe< args.numOutProbe:
    print('data shape does not allow for d=%d, ABORT'%args.numOutProbe)
    exit(1)

X=inpD['frames'][:args.numFrame]
#print('before X',X.shape)
X=rebin_data3D(X,nReb)
print('after X',X.shape)

T0 = time.time()
model = DCA(T=args.windowLength, block_toeplitz=False, n_init=5, dtype=torch.float32)
#model.fit(X)  # two steps at once
print('est cov ...')
model.estimate_data_statistics(X)
T1 = time.time()
delT1=(T1-T0)/60.
print('   time=%.1f(min) '%(delT1))
print('fit ... d=',args.numOutProbe)
model.fit_projection(d=args.numOutProbe)
T2 = time.time()
delT2=(T2-T1)/60.
print('   time=%.2f  (min) '%(delT2))

score=model.score()
print('score',score)

print("#,%s,%d,%d,%d,%d,%.2f,%s,%.1f,%.1f,%.1f"%(shortN,args.numFrame, numInpProbe,args.numOutProbe,args.windowLength,score,args.jobId,delT1,delT2,delT1+delT2)) 
saveSummary(args,score,delT1*60,delT2*60,X.shape[1],model.coef_,inpF)
# add jobId
