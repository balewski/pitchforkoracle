#!/usr/bin/env python
""" 
extract DCA score from sum-files, filter by cell name
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import numpy as np
import  time
import sys,os
sys.path.append(os.path.abspath("../"))
from toolbox.Util_IOfunc import write_yaml, read_yaml, read_one_csv
from toolbox.Plotter_Backbone import Plotter_Backbone
from pprint import pprint

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")

    parser.add_argument("-N", "--cellName", nargs='+',help="list of bbpNNN, blank separated", default=[])
    parser.add_argument('--venue', dest='formatVenue', choices=['prod','poster'], default='prod',help=" output quality/arangement")

    parser.add_argument("-o", "--outPath",
                        default='out/',help="output path for plots and tables")

    args = parser.parse_args()
    args.prjName='dcaScore'
    
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#...!...!..................
def extract_one_cell(rawT,name):
    print('extract for',name)

    i=0
    data={}
    for rec in rawT:
        #print('rr',rec)
        if name != rec['cellName']: continue
        #print(i,rec)
        d=int(rec[' dca.d'])
        pi=rec[' PI']
        #print(d,pi)
        if d not in data: data[d]=[]
        data[d].append(float(pi))
        i+=1
    print('found %d val for %s , num d=%d'%(i,name,len(data)))
    #pprint(data)
    for d in data: data[d]=np.array(data[d])
    return data
      
#............................
#............................
#............................
class Plotter_PredictInfoM(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)
        self.cellName=args.cellName
        self.timeBin_msec=0.125  # for 8kHz data        

#...!...!..................
    def dcaScore(self,dataD,plDD,tit1='',figId=5):
        cellL=sorted(dataD)
        
        figId=self.smart_append(figId)
        if args.formatVenue=='poster':
            xySize=(5,4.5)
            cellL=['bbp156','bbp176','bbp205','bbp206','bbp207',  # L6 
                   'bbp152','bbp153','bbp154','bbp155',  #L5
                   'bbp098','bbp102',  #L4
                   'bbp054'] #L23
        else: xySize=(8,6)
      
        fig=self.plt.figure(figId,facecolor='white', figsize=xySize)  #(9,6.5)
        ax = self.plt.subplot(1,1,1)

        xbinFact=5 # regulats width of violin
               
        print('pl cL',cellL)
        cL10 = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']  # 10 distinct colors
        cli=-1

        nameD={'bbp054':('#D35400','L23'), 'bbp098':('#9B59B6','L4'), 'bbp102':('#6C3483',None), 'bbp152':('#16A085','L5'), 'bbp153':('#28B463',None),'bbp154':('#229954',None),'bbp155':('#28B463',None),'bbp156':('#21618C','L6'),'bbp176':('#3498DB',None), 'bbp205':('#5499C7',None), 'bbp206':('#2874A6',None), 'bbp207':('#5DADE2',None)}
        
        for cellN in cellL:
            xV=[]; yV=[]; eV=[]; ydV=[] # for violin
            dca_dL=sorted(dataD[cellN])

            for d in dca_dL:
                scoreV=dataD[cellN][d]
                ydV.append(scoreV)
                xV.append(d/xbinFact)
                yV.append(np.mean(scoreV))
                eV.append(np.std(scoreV))

            #
            yV=np.array(yV)
            eV=np.array(eV)
            
            if 0: # change to relative P.I.
                maxPI=max(yV)
                print('cell %s maxPI=%.1f'%(cellN,maxPI))
                yV/=maxPI; eV/=maxPI
                ydV=[ yd/maxPI for yd in ydV ]
                        
            emV=eV/np.sqrt(eV.shape[0]-1)
            
            lab='%s:%d'%(cellN,scoreV.shape[0])
            msi=9 ; cli+=1
            symi='$%s$'%(int('0')+cli)
            if cli<10: coli=cL10[cli]
            
            if args.formatVenue=='poster':
                symi='.'
                coli,lab=nameD[cellN]

            if 1 :
                viop=ax.violinplot(ydV, positions=xV,showmeans=False, showmedians=False, showextrema=False )
                for pc in viop['bodies']:
                    pc.set_facecolor(coli)

            ax.errorbar(xV, yV, yerr=emV,linewidth=1.5,marker=symi,markersize=msi,label=lab,color=coli,linestyle=':')
        winTbin=plDD['windowLength']
        tit1='T=%.0f ms [%d bins]'%(winTbin*self.timeBin_msec,winTbin)
        tit=tit1+', numFrames:%d'%(plDD['numFrame'])
        ytit='Predictive Information (bits)'
      
        if args.formatVenue=='poster':
             ax.legend()
        else:               
            ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
               ncol=6, mode="expand", borderaxespad=0., title=plDD['legTitle'])

        #txtSize=16
        if args.formatVenue!='poster':
            ax.text(0.05,0.95,tit,transform=ax.transAxes)
        if 'altY' in dataD: ytit=dataD['altY']
        
        ax.set_xlabel('DCA Dimensionality')#, fontsize = txtSize)
        ax.set_ylabel(ytit)#, fontsize = txtSize)
        
        # change font size for x axis
        #ax.xaxis.get_xticklabels().set_fontsize(40)
        # We change the fontsize of minor ticks label 
        ax.tick_params(axis='both', which='major')#, labelsize=txtSize)
       
        if args.formatVenue!='poster':
            ax.grid()
        
        ax.set_xlim(0,)
        ax.set_ylim(0,)
        #ax.set_xscale('log')

        # restore physical 'd' labels
        xticP=[]; xticL=[]
        for d in range(0,75,5):
            xticP.append(d/xbinFact)
            if d%10==0:
                xticL.append(str(d))
            else:
                xticL.append('')
        ax.set_xticks(xticP)
        ax.set_xticklabels(xticL)

   

#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":

    args=get_parser()

    inpF="/global/cfs/cdirs/m2043/balewski/neuronBBP-misc-8kHz/pred-info-169cell-800frames/pred-info-800frames-8kHz-T16_169cell.csv"

    rawT,_=read_one_csv(inpF)

    cellL=['bbp156','bbp176','bbp205','bbp206','bbp207',  # L6 
                   'bbp152','bbp153','bbp154','bbp155',  #L5
                   'bbp098','bbp102',  #L4
                   'bbp054'] #L23
    if len(args.cellName) >0: cellL=args.cellName

    dataD={}
    for name in cellL:
        dataD[name]=extract_one_cell(rawT,name)

    plDD={}
    plDD['windowLength']=16

    plDD['numFrame']=800
    plDD['legTitle']='multi-frame DCA'
    plot=Plotter_PredictInfoM(args)
    plot.dcaScore(dataD,plDD)
    
    plot.display_all('oct14C', png=0)
  
