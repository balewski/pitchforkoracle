#!/usr/bin/env python
""" 
extract DCA score from sum-files, filter by cell name
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import numpy as np
import  time
import sys,os
sys.path.append(os.path.abspath("../"))
from Util_IOfunc import write_yaml, read_yaml
from Plotter_Backbone import Plotter_Backbone

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")

    parser.add_argument("-s", "--slurmDir", nargs='+',help="path to inputs, a l blank separated list,expands 1234/10-13", default=['32462211/0'])


    parser.add_argument("-o", "--outPath",
                        default='out2/',help="output path for plots and tables")

    parser.add_argument("-N", "--cellName", type=str, default='bbp102', help="cell shortName")
    args = parser.parse_args()
    args.sourcePath='/global/cscratch1/sd/balewski/neuron-DCA4/'
    args.prjName='dcaScore'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args



def expandList(kL):
    kkL=[]
    for x in kL:
        if '-' not in x:
            kkL.append(x) ; continue
        [a,b]=x.split('/')
        print('ab',a,b)
        xL=b.split('-')
        for i in range(int(xL[0]),int(xL[1])+1):
            kkL.append('%s/%d'%(a,i))
    print(kL,'  to ',kkL)
    return kkL

    nK=len(kL)
    assert nK>0



# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
def locateSummaryData(src_dir,cellL,sumD,verb=1):
    src_dir+='/out/'  # for  multi task per job data
    for xx in [ src_dir]:
        if os.path.exists(xx): continue
        print('Aborting on start, missing  dir:',xx)
        exit(1)

    if verb>0: print('use src_dir:',src_dir,'prefix:',cellL)
    jobL=os.listdir(src_dir)
    print('locateSummaryData got %d potential jobs:'%len(jobL))
    print('sub-dirs',jobL)

    # at this point the list of dirs is fixed, now picking of sum.yaml

    nok=0
    for jobId in jobL:
        sumF=None
        for cellN in cellL:
            if cellN  not in jobId : continue
            sumF=src_dir+jobId
            #print('ee',cellN  not in jobId ,cellN  , jobId,sumF )
            break
        if sumF==None: continue
        #print('oo',sumF, cellN)
        try:
            dcaD=read_yaml(sumF,verb=1)
        except:
            print( ' -->missing/failed')
            continue
        nok+=1
        
        #print(cellN,'zz',dcaD)
        assert cellN==dcaD['cellName']
        dcaScore=dcaD['dcaScore']
        
        #dcaScore=dcaD['covTime']/60. ; sumD['altY']='DCA cov-time/task (min)'
        #dcaScore=dcaD['fitTime']/60. ; sumD['altY']='DCA fit-time/task (min)'
        #dcaScore=(dcaD['fitTime']+dcaD['covTime'])/60.; sumD['altY']='DCA tot time/task (min)'
        
        numOutProbe=dcaD['numOutProbe']
        windowLength=dcaD['windowLength']
        assert  windowLength==sumD['windowLength']
        

        sumD['score'][cellN][numOutProbe].append(dcaScore)

        sumD['numFrame']=dcaD['numFrame']
    return nok        
#............................
#............................
#............................
class Plotter_PredictInfoM(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)
        self.cellName=args.cellName
        self.timeBin_msec=0.125  # for 8kHz data        
                
#...!...!..................
    def dcaScore(self,dataD,tit1='',figId=5):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(5,4.5))
        ax = self.plt.subplot(1,1,1)

        xbinFact=5 # regulats width of violin
        colT={'bbp102':('grey','v',10),'bbp153':('black','o',10), 'bbp019':('pink','D',8),'bbp027':('red','s',9)}

        morfT={'bbp019':'inhb smpl 019','bbp027':'inhb cmplx 027','bbp102':'exct smpl 102','bbp153':'exct cmplx 153'}

        maxS={'bbp019':70.08717, 'bbp027':42.51421, 'bbp102':62.6771,'bbp153':180.7887}
        
        
        cellL=sorted(dataD['score'])
        print('pl cL',cellL)

        for cellN in cellL:
            xV=[]
            yV=[]
            eV=[]
            ydV=[] # for violin

            for d in dataD['dca_dL']:
                scoreL=dataD['score'][cellN][d]
                if len(scoreL)<=0: continue
                scoreV=np.array(scoreL)
                #scoreV/=maxS[cellN] # renormalize to max(cell)

                ydV.append(scoreV)
                xV.append(d/xbinFact)
                yV.append(np.mean(scoreV))
                eV.append(np.std(scoreV))
                #print('zzz',cellN,d,np.mean(scoreV))
            #
            yV=np.array(yV)
            eV=np.array(eV)

            emV=eV/np.sqrt(eV.shape[0]-1)
            
            lab=morfT[cellN]
            coli,symi,msi=colT[cellN]
            msi*=0.5

            if 1 :
                viop=ax.violinplot(ydV, positions=xV,showmeans=False, showmedians=False, showextrema=False )
                for pc in viop['bodies']:
                    pc.set_facecolor(coli)

            ax.errorbar(xV, yV, yerr=emV,linewidth=1.5,marker=symi,markersize=msi,label=lab,color=coli)
            #ax.plot(xV, yV,linewidth=1.5,marker=symi,markersize=msi,label=lab,color=coli)
            #ax.plot(xV, yV+eV,linewidth=1.0,marker=None,color=coli,ls='--')
            #ax.plot(xV, yV-eV,linewidth=1.,marker=None,color=coli,ls='--')
        winTbin=dataD['windowLength']
        tit1='T=%.0f ms [%d bins]'%(winTbin*self.timeBin_msec,winTbin)
        tit=tit1+', numFrames:%d, shards:%d'%(dataD['numFrame'],scoreV.shape[0])
        ytit='DCA P.I. (aka score)'

        ax.legend(loc='upper left',title='BBP cell idx')
        #ax.legend(loc='lower right',title='BBP cell idx'); ytit='DCA relative P.I.'; ax.axhline(0.8, color='green', linestyle=':', linewidth=1.); ax.axhline(0.9, color='green', linestyle=':', linewidth=1.); ax.axhline(1.0, color='green', linestyle='-', linewidth=1.)

        
        if 'altY' in dataD: ytit=dataD['altY']
        ax.set(title=tit,xlabel='DCA d (aka num output probes)',ylabel=ytit)
        #ax.grid()
        
        ax.set_xlim(0,)
        ax.set_ylim(0,)
        #ax.set_xscale('log')

        # restore physical 'd' labels
        xticP=[]; xticL=[]
        for d in range(0,75,5):
            xticP.append(d/xbinFact)
            if d%10==0:
                xticL.append(str(d))
            else:
                xticL.append('')
        ax.set_xticks(xticP)
        ax.set_xticklabels(xticL)


#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":

    args=get_parser()
    slurmDir=expandList(args.slurmDir)

    sumD={}
    cellL=['bbp019','bbp027','bbp102','bbp153']
    dca_dL=[1,5,10,15,20,23,25,30,35,40,45,50,55,67]
    sumD['score']={c:{ d:[] for d in dca_dL} for c in cellL}
    #sumD['windowLength']=16 # 32888663/0-29
    sumD['windowLength']=64 # 32935962/0-29
    
    sumD['cellName']=args.cellName
    sumD['dca_dL']=dca_dL

    # grid sampling
    nok=0
    for slurmJid in slurmDir:
        nok+=locateSummaryData(args.sourcePath+slurmJid, cellL,sumD)

    print('nOK=',nok)
    #print('M:all scores',sumD['score'])

    if nok<=0: exit(0)
    for cellN in cellL:
        n2=0
        for  d in dca_dL:
            n1=len(sumD['score'][cellN][d])
            print(cellN,d ,'=d, end  len',n1)
            n2+=n1
        if n2==0: sumD['score'].pop(cellN)
    plot=Plotter_PredictInfoM(args)
    plot.dcaScore(sumD)
    
    plot.display_all(args.cellName,png=1)
