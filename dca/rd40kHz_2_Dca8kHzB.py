#!/usr/bin/env python3
import sys,os
'''
Can do 1 sample or many samples DCA
Switch: --numFrame==1

Paper: 
https://papers.nips.cc/paper/9574-unsupervised-discovery-of-temporal-structure-in-noisy-data-with-dynamical-components-analysis.pdf

https://github.com/BouchardLab/DynamicalComponentsAnalysis
Jesse: should behave similarly to a scikit-learn model (fit, transform, model.coef_).

For the X =(nSamp, nTimeBin, nFeat), d<nFeat,  T << nTimeBin.
Q: what is the dimension of cov matrix?
A:The covariance has size  (2*T*nFeat)^2


module load tensorflow/gpu-2.2.0-py37 
export HDF5_USE_FILE_LOCKING=FALSE
export PATH=/global/homes/b/balewski/.local/cori/gpu-tensorflow2.2.0-py37/bin:$PATH

As Roy:
export PATH=/global/homes/r/roybens/.local/cori/gpu-tensorflow2.2.0-py37/bin:$PATH

Quick test: 
400 frames:
   ./rd40kHz_2_Dca8kHzB.py   --numOutProbe 3 --windowLength 10 --cellName bbp153

1 frame after skipping 3:
  ./rd40kHz_2_Dca8kHzB.py  --numOutProbe 3 --windowLength 10 --cellName bbp153 --numFrame 1 --skipFrame 7

takes ~2 min on cori-login node
'''

import numpy as np
import time
import torch
from dca import DynamicalComponentsAnalysis as DCA

sys.path.append(os.path.abspath("/global/homes/b/balewski/pitchforkOracle/toolbox/"))
#sys.path.append(os.path.abspath("/global/homes/r/roybens/janTest/pitchforkOracle/toolbox/"))
from Util_IOfunc import write_yaml, read_yaml, write_data_hdf5, read_data_hdf5

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--shardId", default=7, type=int, help="data shard to be used")
    parser.add_argument("--numFrame", default=400, type=int, help="number of frames, or all is -1")
    parser.add_argument("--skipFrame", default=0, type=int, help="number of initial frames to skip")

 
    parser.add_argument("--numOutProbe", default=3, type=int, help="number of output (DCA-transformed) probes")
    parser.add_argument('-T',"--windowLength", default=16, type=int, help="number of consecutive time-bins from full time-line")
    
    parser.add_argument("-o","--outPath",
                        default='out/',help="output path for plots and tables")

    parser.add_argument("-N", "--cellName", type=str, default='bbp153',
                        help="cell shortName")
    parser.add_argument("-j","--jobId", default=None,
        help="optional, aux info to be stored w/ summary")

    args = parser.parse_args()
    args.dataPath='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr/'
    args.prjName='cellDca'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#...!...!..................
def saveSummary(args,score,covTime,fitTime,numTimeBin,dcaCoef,inpF,P,jid):

    argL=list(args.__dict__.keys())
    for k in ['verb',  'prjName']: argL.remove(k)
    print(argL)
    
    sumRec={ k:getattr(args,k) for k in argL}
    sumRec['dcaScore']=float(score)
    sumRec['covTime']=covTime
    sumRec['fitTime']=fitTime
    sumRec['numTimeBin']=numTimeBin
    sumRec['inpFile']=inpF
    sumRec['phys_par']=P
    print(sumRec)
    sumRec['dcaCoef']=np.array(dcaCoef) # this is too much to print
    
    outF=args.prjName+'_%s_%s.sum.yaml'%(args.cellName,jid)
    write_yaml(sumRec, args.outPath+'/'+outF)

def rebin_data3D(X,nReb):
        nS,tBin,nF=X.shape
        #print('X3D',X.shape,'nReb=',nReb)
        assert tBin==8000 # tested only for original data
        assert tBin%nReb==0
        a=X.reshape(nS,tBin//nReb,nReb,nF)
        b=np.sum(a,axis=2)/nReb
        #print('X2',a.shape,b.shape,b.dtype)
        return b


    
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

nReb=5

shortN=args.cellName
inpF='%s/%s.cellSpike.data_%d.h5'%(shortN,shortN,args.shardId)
inpD=read_data_hdf5(args.dataPath+inpF)

numInpProbe=inpD['frames'].shape[2]
if numInpProbe< args.numOutProbe:
    print('data shape does not allow for d=%d, ABORT'%args.numOutProbe)
    exit(1)
fr0=args.skipFrame
fr1=fr0+args.numFrame
X=inpD['frames'][fr0:fr1]
P=[999.] # used only for 1-frame case
jid=args.shardId 
#print('before X',X.shape)
X=rebin_data3D(X,nReb)
if X.shape[0]==1:
    X=np.squeeze(X)
    P=np.squeeze( inpD['phys_par'][fr0:fr1] )
    jid='s%d_f%d'%(args.shardId,args.skipFrame)
print('after X',X.shape,'P=',P)

T0 = time.time()
model = DCA(T=args.windowLength, block_toeplitz=False, n_init=5, dtype=torch.float32)
#model.fit(X)  # two steps at once
print('est cov ...')
model.estimate_data_statistics(X)
T1 = time.time()
delT1=(T1-T0)/60.
print('   time=%.1f(min) '%(delT1))
print('fit ... d=',args.numOutProbe)
model.fit_projection(d=args.numOutProbe)
T2 = time.time()
delT2=(T2-T1)/60.
print('   time=%.2f  (min) '%(delT2))

score=model.score()
print('score',score)

print("#,%s,%d,%d,%d,%d,%.2f,%s,%.1f,%.1f,%.1f,%.3f"%(shortN,args.numFrame, numInpProbe,args.numOutProbe,args.windowLength,score,args.jobId,delT1,delT2,delT1+delT2,P[0])) 
saveSummary(args,score,delT1*60,delT2*60,X.shape[1],model.coef_,inpF,P,jid)
# add jobId
