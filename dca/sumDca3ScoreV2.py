#!/usr/bin/env python
""" 
extract DCA score from sum-files, filter by cell name
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import numpy as np
import  time
import sys,os
sys.path.append(os.path.abspath("../"))
from toolbox.Util_IOfunc import write_yaml, read_yaml, read_one_csv
from toolbox.Plotter_Backbone import Plotter_Backbone
from sumDca4ScoreV2 import extract_one_cell

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")
  
    parser.add_argument("-o", "--outPath",
                        default='out/',help="output path for plots and tables")

    parser.add_argument('--venue', dest='formatVenue', choices=['prod','poster'], default='prod',help=" output quality/arangement")

    parser.add_argument( "-r", dest='doRelative',
        action='store_true', default=False,help="enable relative plot")


    args = parser.parse_args()
 
    args.prjName='dcaScore'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
#............................
#............................
class Plotter_PredictInfoM(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)
        
        self.timeBin_msec=0.125  # for 8kHz data        
                
#...!...!..................
    def dcaScore(self,dataD,plDD,tit1='',figId=5):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(5,4.5))
        ax = self.plt.subplot(1,1,1)

        xbinFact=5 # regulats width of violin
        colT={'bbp102':('grey','v',10),'bbp153':('black','o',10), 'bbp019':('pink','D',8),'bbp027':('red','s',9)}

       # morfT={'bbp019':'inhb smpl 019','bbp027':'inhb cmplx 027','bbp102':'exct smpl 102','bbp153':'exct cmplx 153'}

        morfT={'bbp019':'inhb smpl','bbp027':'inhb cmplx','bbp102':'exct smpl','bbp153':'exct cmplx'}
                
        cellL=sorted(dataD)
        print('pl cL',cellL)

        if plDD['doRelative']: # find max avr per cell
            maxAvr={}
            for cellN in cellL:
                tmp=[]
                dca_dL=sorted(dataD[cellN])
                for d in dca_dL:
                    scoreV=dataD[cellN][d]
                    tmp.append(np.mean(scoreV))
                maxAvr[cellN]=max(tmp)
                
        for cellN in cellL:
            xV=[]
            yV=[]
            eV=[]
            ydV=[] # for violin
            dca_dL=sorted(dataD[cellN])
            for d in dca_dL:
                scoreV=dataD[cellN][d]
                if plDD['doRelative']:
                    scoreV/= maxAvr[cellN]

                ydV.append(scoreV)
                xV.append(d/xbinFact)
                yV.append(np.mean(scoreV))
                eV.append(np.std(scoreV))
                #print('zzz',cellN,d,np.mean(scoreV))
            #
            yV=np.array(yV)
            eV=np.array(eV)

            emV=eV/np.sqrt(eV.shape[0]-1)
            
            lab=morfT[cellN]
            coli,symi,msi=colT[cellN]
            msi*=0.5

            if 1 :
                viop=ax.violinplot(ydV, positions=xV,showmeans=False, showmedians=False, showextrema=False )
                for pc in viop['bodies']:
                    pc.set_facecolor(coli)

            ax.errorbar(xV, yV, yerr=emV,linewidth=1.5,marker=symi,markersize=msi,label=lab,color=coli)
        winTbin=plDD['windowLength']
        tit1='T=%.0f ms [%d bins]'%(winTbin*self.timeBin_msec,winTbin)
        tit=tit1+', numFrames:%d, shards:%d'%(plDD['numFrame'],scoreV.shape[0])
        

        
        #ax.legend(loc='lower right',title='BBP cell idx'); ytit='DCA relative P.I.';
        if plDD['doRelative']:
            ax.axhline(0.8, color='green', linestyle=':', linewidth=1.)
            ax.axhline(0.9, color='green', linestyle=':', linewidth=1.)
            ax.axhline(1.0, color='green', linestyle='-', linewidth=1.)

        
        if 'altY' in dataD: ytit=dataD['altY']
        
        ax.set_xlabel('DCA Dimensionality')
        if plDD['doRelative']:
            ax.set_ylabel('Relative Predictive Information')
            ax.legend(loc='lower right')
        else:
            ax.set_ylabel('Predictive Information (bits)')
        
        if args.formatVenue!='poster':
            ax.grid()
            winTbin=plDD['windowLength']
            tit1='T=%.0f ms [%d bins]'%(winTbin*self.timeBin_msec,winTbin)
            tit=tit1+', numFrames:%d, shards:%d'%(plDD['numFrame'],scoreV.shape[0])
            ax.set(title=tit)
        
        ax.set_xlim(0,)
        ax.set_ylim(0,)
        #ax.set_xscale('log')

        # restore physical 'd' labels
        xticP=[]; xticL=[]
        for d in range(0,75,5):
            xticP.append(d/xbinFact)
            if d%10==0:
                xticL.append(str(d))
            else:
                xticL.append('')
        ax.set_xticks(xticP)
        ax.set_xticklabels(xticL)


#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":

    args=get_parser()
    

    inpF="/global/cfs/cdirs/m2043/balewski/neuronBBP-misc-8kHz/pred-info-169cell-800frames/pred-info-800frames-8kHz-T16_169cell.csv"

    rawT,_=read_one_csv(inpF)

    sumD={}
    cellL=['bbp019','bbp027','bbp102','bbp153']

    dataD={}
    for name in cellL:
        dataD[name]=extract_one_cell(rawT,name)

    plDD={}
    plDD['windowLength']=16
    plDD['numFrame']=800
    plDD['doRelative']=args.doRelative
    plot=Plotter_PredictInfoM(args)
    plot.dcaScore(dataD,plDD)
    
    plot.display_all('oct14A%d'%args.doRelative,png=0)
