#!/usr/bin/env python3
import sys,os
'''
compact Z+DCA  for production
https://github.com/BouchardLab/DynamicalComponentsAnalysis
Jesse: should behave similarly to a scikit-learn model (fit, transform, model.coef_).

See details at:
https://github.com/BouchardLab/DynamicalComponentsAnalysis/blob/master/dca/dca.py

module load tensorflow/gpu-2.2.0-py37 
export HDF5_USE_FILE_LOCKING=FALSE
export PATH=/global/homes/b/balewski/.local/cori/gpu-tensorflow2.2.0-py37/bin:$PATH

Answer, Cori login node 
enerate: nTbin=1600,nFeat=67
X (1600, 67) T= 16
est cov ...
done 1.0 sec
fit DCA.d=16 ...
d=16 score=16.470  time =251.4 sec

Full Haswell
generate: nTbin=1600,nFeat=67
X (1600, 67) T= 16
est cov ...
done 0.7 sec

d=1 score=2.330  time =24.5 sec
d=2 score=5.094  time =15.6 sec
d=8 score=14.612  time =49.0 sec
d=16 score=15.076  time =87.4 sec
d=32 score=17.098  time =128.8 sec
d=64 score=19.772  time =151.4 sec
d=67 score=23.898  time =113.9 sec

'''
import  time
import numpy as np
import torch
from dca import DynamicalComponentsAnalysis as DCA

def noise_dataset(nTbin=333,nFeat=10):
    print('generate: nTbin=%d,nFeat=%d'%(nTbin,nFeat))
    X = np.random.randn(nTbin,nFeat)  
    return X

def main():
    #nFeat=7 ; nTbin=160 ; T=6 ; d=5  # works quickly , 2 minutes on Haswell
    nTbin=1600 ; nFeat=67 ; T=16 ; d=1  # fit_projection is stuck
    
    X = noise_dataset(nTbin,nFeat)
    print('X',X.shape, 'T=',T)
    X[:, :d] = np.cumsum(X[:, :d], axis=0) # give d dimensions some structure

    model = DCA(T=T, block_toeplitz=False, n_init=5, dtype=torch.float32)
    print('est cov ...') ; T0 = time.time()
    model.estimate_data_statistics(X)
    print('done %.1f sec'%(time.time()-T0))
    T0 = time.time()
    print('fit DCA.d=%d ...'%d)  ;  T1 = time.time()
    model.fit_projection(d=d)
    print('d=%d score=%.3f  time =%.1f sec'%(d,model.score(),time.time()-T1))

    
    
main()

