#!/usr/bin/env python
""" 
extract PredInfo curves from summary files, filter by cell name
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import numpy as np
import  time
import sys,os
sys.path.append(os.path.abspath("../"))
from Util_IOfunc import write_yaml, read_yaml
from Plotter_Backbone import Plotter_Backbone

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")

    parser.add_argument("-i", "--inpDir", nargs='+',help="path to inputs, all blank separated list", default=['outPI'])

    parser.add_argument("-o", "--outPath",
                        default='out2/',help="output path for plots and tables")

    parser.add_argument("-N", "--cellName", type=str, default='bbp153', help="cell shortName")
    args = parser.parse_args()
    #args.sourcePath='/global/cscratch1/sd/balewski/neuronBBP-pca2/'
    args.prjName='persInfoM'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
def locateSummaryData(src_dir,prefix,verb=1):
    #src_dir+='/out/'  # for  trainMulti data
    for xx in [ src_dir]:
        if os.path.exists(xx): continue
        print('Aborting on start, missing  dir:',xx)
        exit(1)

    if verb>0: print('use src_dir:',src_dir,'prefix:',prefix)

    jobL=os.listdir(src_dir)
    print('locateSummaryData got %d potential jobs, e.g.:'%len(jobL), jobL[0])
    #print('sub-dirs',jobL)

    # at this point the list of dirs is fixed, now picking of sum.yaml
    outD={'probeName':[],'yA':[],'eA':[]}
    
    for jobId in jobL:
        if 'yaml' not in jobId : continue
        if prefix  not in jobId : continue
        sumF=src_dir+'/'+jobId
        sumD=read_yaml(sumF,verb=1)

        assert prefix==sumD['cellName']
        probeIdx=sumD['probeIdx']
        probeName=sumD['probe_name']
        T1D=sumD['T1D']
        timeBin_msec=sumD['timeBin_msec']
        xbinA, yA, eA=sumD['avr pred info']
        #print('rr',jobId,xbinA.shape,probeName)
        if 'tA' not in outD:
            outD['tA']=xbinA
        else:
            assert outD['tA'].shape ==xbinA.shape
        outD['probeName'].append(probeName)
        outD['yA'].append(yA)
        outD['eA'].append(eA)
            
    print(' end yA len',len(outD['yA']))
        
    return outD
            
#............................
#............................
#............................
class Plotter_PredictInfoM(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)
        self.cellName=args.cellName
        
        
                
#...!...!..................
    def predInfo(self,dataD,tit1='',figId=5):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(5,4.5))
        ax = self.plt.subplot(1,1,1)
        nameA=dataD['probeName']
        yA=dataD['yA']
        eA=dataD['eA']
        xbinA=dataD['tA']
        print('xbin A',xbinA.shape)
        cL7=['r', 'b', 'g', 'c', 'm', 'y', 'k']  # 7 distinct colors
        mL7=['D','*','^','o','x','h','>']
        nT=len(nameA)
        assert nT<=len(cL7)

        probeL=['soma','axon','dend','apic']

        for i,prn in enumerate(probeL):
            k0=-1
            #print('ii',i,prn)
            for k,probN in enumerate(nameA):
                if prn in probN:
                    k0=k
                    break
            if k0<0: continue    
            #print('kk',k0,probN)
            probN=probN.replace('dend','basal_dend')
            probN=probN.replace('apic','apic_dend')
            y=yA[k0]
            e=eA[k0]
            ax.errorbar(xbinA, y, yerr=e,linewidth=1.5,marker=mL7[i],markersize=6.,label=probN,color=cL7[i])

        #ax.set_ylim(0,14)
        xLab='window size T (ms)  '
        ax.set(title='cell '+self.cellName,xlabel=xLab,ylabel='Predictive Information')
        ax.grid()
        ax.legend(loc='upper left',title='probe name')


#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":

    args=get_parser()

    sumD={}
    # grid sampling
    for slurmJid in args.inpDir:
        sumD1=locateSummaryData(slurmJid, args.cellName)
        sumD.update(sumD1)

    plot=Plotter_PredictInfoM(args)
    plot.predInfo(sumD)
    
    plot.display_all(args.cellName)
