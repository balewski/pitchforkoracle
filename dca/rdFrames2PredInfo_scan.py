#!/usr/bin/env python3
'''
This code computes predictive info for all probes and fixed T-window


Paper: 
https://papers.nips.cc/paper/9574-unsupervised-discovery-of-temporal-structure-in-noisy-data-with-dynamical-components-analysis.pdf

https://github.com/BouchardLab/DynamicalComponentsAnalysis
Jesse: should behave similarly to a scikit-learn model (fit, transform, model.coef_).

module load tensorflow/gpu-2.1.0-py37
export HDF5_USE_FILE_LOCKING=FALSE 
export PATH=/global/homes/b/balewski/.local/cori/gpu-tensorflow2.1.0-py37/bin:$PATH

Quick test: 
  ./rdFrames2PredInfo.py  --numShard 3 --windowLength 50

'''

import numpy as np
import time
from dca.cov_util import calc_pi_from_data
import sys,os
sys.path.append(os.path.abspath("../"))
from Util_IOfunc import write_yaml, read_yaml, write_data_hdf5, read_data_hdf5,build_name_hash
from Plotter_Backbone import Plotter_Backbone

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")
    parser.add_argument("--numShard", default=3, type=int, help="number of data shards to be used")

    parser.add_argument("--shardZero", default=0, type=int, help="0-th shard")
    parser.add_argument('-nf',"--numFrame", default=500, type=int, help="number of frames, or all is -1")
    
    parser.add_argument("--maxProbeIdx", default=2, type=int, help="number of input probes")
    parser.add_argument('-T',"--windowLengthBin", default=16, type=int, help="T-window, in bins @ kHz")
    
    parser.add_argument("-o","--outPath",
                        default='out/',help="output path for plots and tables")

    parser.add_argument("-N", "--cellName", type=str, default='bbp153', help="cell shortName")

    args = parser.parse_args()
    args.dataPath='/global/cfs/cdirs/m2043/balewski/neuronBBP-pack8kHz/probe_67pr8kHz/'
    args.timeBin_msec=0.025*5
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
#............................
#............................
class Plotter_PredictInfo(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)
        self.timeBin_msec=args.timeBin_msec
        
                
#...!...!..................
    def predInfo(self,dataLL,tit='',figId=5):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(12,4))
        ax = self.plt.subplot(1,1,1)

        data=np.array(dataLL)
        nPr=data.shape[1]
        xbinA=[i for i in range(nPr)]

        print('xbin A',xbinA,)
        yA=[]; eA=[]
        for i in range(nPr):
            yA.append(data[:,i].mean())
            eA.append(data[:,i].std())

        ax.errorbar(xbinA, yA, yerr=eA, fmt='o',linewidth=1.5,markersize=3.)

        xLab='probe index'
        ax.set(title=tit,xlabel=xLab,ylabel='pred.info.')
        ax.grid()
        #ax.set_xscale('log')
        return xbinA, yA, eA

#...!...!..................
def saveSummary(args,resLL,runTime,metaD):
    argL=list(args.__dict__.keys())
    for k in ['verb', 'noXterm', 'prjName','maxProbeIdx']: argL.remove(k)
    print(argL)
    
    sumRec={ k:getattr(args,k) for k in argL}
    sumRec['predInfo2D']=resLL
    sumRec['runTime']=runTime
    #print(sumRec)
    
    sumRec.update(metaD)
    outF=sumRec['short_name']+'.sum.yaml'
    write_yaml(sumRec, args.outPath+'/'+outF)


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
nameSuf='Tbin%d'%args.windowLengthBin
sumD=build_name_hash('cellPredInfo_scan_%s'%args.cellName,nameSuf)

args.prjName=sumD['short_name']
plot=Plotter_PredictInfo(args)

shortN=args.cellName

startT0 = time.time()

inpF='%s/meta.cellSpike_67pr8kHz.yaml'%(shortN)
metaD=read_yaml(args.dataPath+inpF)
probeL=metaD['rawInfo']['probeName']

maxPidx=min(len(probeL),args.maxProbeIdx)
probeL=probeL[:maxPidx]
print('loop over %d probes'%maxPidx)
sumD['probe_name']=probeL
sumD['windowLength_ms']=float(args.windowLengthBin*args.timeBin_msec)

resLL=[] # each record is a different shard
T0 = time.time()
for si in range(args.numShard):
    shardId=si+args.shardZero
    inpF='%s/%s.cellSpike_67pr8kHz.data_%d.h5'%(shortN,shortN,shardId)
    inpD=read_data_hdf5(args.dataPath+inpF)#,verb=(si==0))
    assert args.numFrame<= inpD['frames'].shape[0]

    rec=[]
    for pidx in range(maxPidx):
        X=inpD['frames'][:args.numFrame,:,pidx:pidx+1]
        X=X/150. # now unist are volts
        #print('X',X.shape,'maxT=',args.maxTLength)
        prInf=calc_pi_from_data(X, 2*args.windowLengthBin)
        if pidx%20==0: print('done pidx=%d, PI=%.1f shard=%d'%(pidx,prInf,si))
        rec.append(float(prInf))
    resLL.append(rec)
    delT=(time.time()-T0)
delT1=time.time()-startT0
print('elaT=%.1f min'%(delT1/60.))
tit='cell=%s  window T=%.0fms  [1ms = %d tBins]  nFrame=%d'%(shortN,sumD['windowLength_ms'],1./args.timeBin_msec,args.numFrame)
tit+='   nShard=%d'%args.numShard
xbinA, yA, eA=plot.predInfo(resLL,tit)
sumD['avr pred info per probe']=np.array([xbinA, yA, eA])
saveSummary(args,resLL,delT1,sumD)

plot.display_all(shortN)


