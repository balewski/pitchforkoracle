#!/usr/bin/env python
""" 
extract DCA score from sum-files, filter by cell name
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import numpy as np
import  time
from pprint import pprint
import sys,os
sys.path.append(os.path.abspath("../toolbox"))
from Util_IOfunc import write_yaml, read_yaml
from Plotter_Backbone import Plotter_Backbone

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")

    parser.add_argument("-s", "--slurmDir", nargs='+',help="path to inputs, a l blank separated list,expands 1234/10-13", default=['37363270/0-7'])


    parser.add_argument("-o", "--outPath",
                        default='out/',help="output path for plots and tables")

    parser.add_argument("-N", "--cellName", type=str, default=None, help="(optional) cell shortName, e.g. bbp153")
    
    parser.add_argument('--venue', dest='formatVenue', choices=['prod','poster'], default='prod',help=" output quality/arangement")
    args = parser.parse_args()
    args.prjName='dcaScore'
    args.sourcePath='/global/cscratch1/sd/balewski/neuron-DCA_2020/'
    #args.sourcePath='/global/cscratch1/sd/balewski/neuron-DCA_one_2020/'
    #args.sourcePath='/global/cscratch1/sd/roybens/neuron-DCA_one_2020/'

    if args.cellName!=None:
        args.sourcePath+=args.cellName+'/'
        args.prjName='pi1frame_'+args.cellName
 

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#...!...!..................
def expandList(kL):
    kkL=[]
    for x in kL:
        if '-' not in x:
            kkL.append(x) ; continue
        [a,b]=x.split('/')
        print('ab',a,b)
        xL=b.split('-')
        for i in range(int(xL[0]),int(xL[1])+1):
            kkL.append('%s/%d'%(a,i))
    print(kL,'  to ',kkL)
    return kkL

    nK=len(kL)
    assert nK>0


#...!...!..................
def process1FramePI(sumD):
    rawDataPath='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr/'
    for cellN in sumD['score']:
        cellD=sumD['score'][cellN]
        print('cellN:',cellN,sorted(cellD))
        phyX=sumD['phys_par'][cellN]
        phyX=np.array(phyX)
        print('P1PI: phyX:',phyX.shape)
        sumD['phys_par'][cellN]= phyX # now data are 2D np-array
        #pprint(cellD)
        
        # sanity check
        for y in cellD:
            print('ccc d=',y,len(cellD[y]),phyX.shape[0])
            assert len(cellD[y])==phyX.shape[0]
            cellD[y]=np.array(cellD[y])
        
        numSamp=phyX.shape[0]
        sumD['num_samp']=numSamp  # it should be called just once, not for all cells.
        # add coonductances names
        inpF='%s/%s/meta.cellSpike.yaml'%(rawDataPath,cellN)
        blob=read_yaml(inpF,verb=1)
        sumD[cellN+'_phys_range']=blob['dataInfo']['physRange']
        for y in ['stimName','bbpName']:
            sumD[y]=blob['rawInfo'][y]


#...!...!..................
def prep1Framelot(inpD,ip=0):
    assert len(inpD['score'])==1
    x=args.cellName
    parInfo=inpD[x+'_phys_range'][ip]
    print('parInfo=',parInfo)
    parName=parInfo[0]
    ph_base=np.sqrt(parInfo[1]*parInfo[2])

    cellD=inpD['score'][x]
    physV=inpD['phys_par'][x][:,ip]
    ph0=min(physV);     ph1=max(physV)

    #print('phy rng',ph0,ph1,ph_base,'count=',physV.shape)
    # define thresholds
    phr=physV/ph_base
    #print('phr=',phr)
    isLow= phr<0.2
    isHigh=phr>4.
    isMid=(phr>0.8) *( phr<2.)
    print('count: low,mid,high=',sum(isLow),sum(isMid),sum(isHigh),'inp=',physV.shape)
    #print('lll',phr[isMid])
    core='%s_phy%d'%(x,ip)
    outD={core+'_low':{},core+'_mid':{},core+'_high':{} }
    for y in cellD:
        assert len(cellD[y])==phr.shape[0]
        outD[core+'_low'][y-0.4]=cellD[y][isLow]
        outD[core+'_mid'][y]=cellD[y][isMid]
        outD[core+'_high'][y+0.4]=cellD[y][isHigh]
        
    return {'score':outD,'windowLength':inpD['windowLength'],'numFrame':1,'legTitle':'slice: '+parName}
        
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
#...!...!..................
def locateSummaryData(src_dir,sumD,verb=1):
    src_dir+='/out/'  # for  multi task per job data
    for xx in [ src_dir]:
        if os.path.exists(xx): continue
        print('Aborting on start, missing  dir:',xx)
        exit(1)

    if verb>0: print('use src_dir:',src_dir)#,'prefix:',cellL)
    jobL=sorted(os.listdir(src_dir))
    print('locateSummaryData got %d potential jobs:'%len(jobL))
    print('sub-dirs',jobL)
    
    # at this point the list of dirs is fixed, now picking of sum.yaml

    nok=0
    for jobId in jobL:
        sumF=src_dir+jobId
        #print('oo',sumF)
        
        try:
            blob=read_yaml(sumF,verb=1)
        except:
            print( ' -->missing/failed')
            continue
        nok+=1
        
        #print(cellN,'zz',dcaD)
        cellN=blob['cellName']
        dcaScore=blob['dcaScore']

        #dcaScore=dcaD['covTime']/60. ; sumD['altY']='DCA cov-time/task (min)'
        #dcaScore=dcaD['fitTime']/60. ; sumD['altY']='DCA fit-time/task (min)'
        #dcaScore=(dcaD['fitTime']+dcaD['covTime'])/60.; sumD['altY']='DCA tot time/task (min)'
        
        numOutProbe=blob['numOutProbe']
        assert  sumD['windowLength']==blob['windowLength']
        assert sumD['numFrame']==blob['numFrame']

        #print('ppp',blob['phys_par'])
        
        if cellN not in sumD['score']:
            sumD['score'][cellN]={}
            sumD['phys_par'][cellN]=[]
        if numOutProbe not in sumD['score'][cellN]:
            sumD['score'][cellN][numOutProbe]=[]

        if args.cellName!=None and numOutProbe==1:
            sumD['phys_par'][cellN].append(blob['phys_par'])
        sumD['score'][cellN][numOutProbe].append(dcaScore)
    
    return nok

#............................
#............................
#............................
class Plotter_PredictInfoM(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)
        #self.cellName='dcaDec2020'
        self.timeBin_msec=0.125  # for 8kHz data        
                
#...!...!..................
    def dcaScore(self,dataD,tit1='',figId=5):
        cellL=sorted(dataD['score'])
        
        figId=self.smart_append(figId)
        if args.formatVenue=='poster':
            xySize=(7,4.8)
            cellL=['bbp156','bbp176','bbp205','bbp206','bbp207',  # L6 
                   'bbp152','bbp153','bbp154','bbp155',  #L5
                   'bbp098','bbp102',  #L4
                   'bbp054'] #L23
        else: xySize=(8,6)
      
        fig=self.plt.figure(figId,facecolor='white', figsize=xySize)  #(9,6.5)
        ax = self.plt.subplot(1,1,1)

        xbinFact=5 # regulats width of violin

 
               
        print('pl cL',cellL)
        cL10 = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']  # 10 distinct colors
        cli=-1

        nameD={'bbp054':('#D35400','L23'), 'bbp098':('#9B59B6','L4'), 'bbp102':('#6C3483',None), 'bbp152':('#16A085','L5'), 'bbp153':('#28B463',None),'bbp154':('#229954',None),'bbp155':('#28B463',None),'bbp156':('#21618C','L6'),'bbp176':('#3498DB',None), 'bbp205':('#5499C7',None), 'bbp206':('#2874A6',None), 'bbp207':('#5DADE2',None)}
        
        for cellN in cellL:
            xV=[]; yV=[]; eV=[]; ydV=[] # for violin
            dca_dL=sorted(dataD['score'][cellN])

            for d in dca_dL:
                scoreL=dataD['score'][cellN][d]
                if len(scoreL)<=0: continue
                scoreV=np.array(scoreL)

                ydV.append(scoreV)
                xV.append(d/xbinFact)
                yV.append(np.mean(scoreV))
                eV.append(np.std(scoreV))

            #
            yV=np.array(yV)
            eV=np.array(eV)
            
            if 0: # change to relative P.I.
                maxPI=max(yV)
                print('cell %s maxPI=%.1f'%(cellN,maxPI))
                yV/=maxPI; eV/=maxPI
                ydV=[ yd/maxPI for yd in ydV ]
            
            
            emV=eV/np.sqrt(eV.shape[0]-1)
            
            lab='%s:%d'%(cellN,scoreV.shape[0])
            msi=9 ; cli+=1
            symi='$%s$'%(int('0')+cli)
            if cli<10: coli=cL10[cli]
            
            if args.formatVenue=='poster':
                symi='.'
                coli,lab=nameD[cellN]

            if 1 :
                viop=ax.violinplot(ydV, positions=xV,showmeans=False, showmedians=False, showextrema=False )
                for pc in viop['bodies']:
                    pc.set_facecolor(coli)

            ax.errorbar(xV, yV, yerr=emV,linewidth=1.5,marker=symi,markersize=msi,label=lab,color=coli,linestyle=':')
        winTbin=dataD['windowLength']
        tit1='T=%.0f ms [%d bins]'%(winTbin*self.timeBin_msec,winTbin)
        tit=tit1+', numFrames:%d'%(dataD['numFrame'])
        ytit='Predictive Information (bits)'
        #ytit='DCA relative P.I.'#a

        if args.formatVenue=='poster':
             ax.legend()
        else:               
            ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
               ncol=6, mode="expand", borderaxespad=0., title=dataD['legTitle'])

        txtSize=16
        if args.formatVenue!='poster':
            ax.text(0.05,0.95,tit,transform=ax.transAxes)
        if 'altY' in dataD: ytit=dataD['altY']
        
        ax.set_xlabel('DCA Dimensionality', fontsize = txtSize)
        ax.set_ylabel(ytit, fontsize = txtSize)
        
        # change font size for x axis
        #ax.xaxis.get_xticklabels().set_fontsize(40)
        # We change the fontsize of minor ticks label 
        ax.tick_params(axis='both', which='major', labelsize=txtSize)
        #ax.tick_params(axis='both', which='minor', labelsize=8)

        if args.formatVenue!='poster':
            ax.grid()
        
        ax.set_xlim(0,)
        ax.set_ylim(0,)
        #ax.set_xscale('log')

        # restore physical 'd' labels
        xticP=[]; xticL=[]
        for d in range(0,75,5):
            xticP.append(d/xbinFact)
            if d%10==0:
                xticL.append(str(d))
            else:
                xticL.append('')
        ax.set_xticks(xticP)
        ax.set_xticklabels(xticL)


#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":

    args=get_parser()
    slurmDir=expandList(args.slurmDir)

    sumD={}
    # ... all scores will be attached here as 2D dict: [cell][dca.d]
    sumD['score']={}
    sumD['phys_par']={}
    sumD['windowLength']=16 
    sumD['numFrame']=800
    if args.cellName!=None:  sumD['numFrame']=1

    # read data frm jobs
    nok=0
    for slurmJid in slurmDir:
        nok+=locateSummaryData(args.sourcePath+slurmJid,sumD)

    print('M:nOK=',nok)
    if args.cellName!=None:  # add 1-frame data post-processing
        process1FramePI(sumD)
        

    if nok<=0: exit(0)
    cellL=sorted(sumD['score'])
    for cellN in cellL:
        n2=0
        dca_dL=sorted(sumD['score'][cellN])
        for  d in dca_dL:
            n1=len(sumD['score'][cellN][d])
            print(cellN,d ,'=d, end  len',n1)
            n2+=n1
        if n2==0: sumD['score'].pop(cellN)

    outF=args.prjName+'.yaml'
    write_yaml(sumD,args.outPath+outF)

    if args.cellName!=None: 
        sumD['legTitle']='%s %s'%(args.cellName,sumD['bbpName'])
        xx=sumD[args.cellName+'_phys_range']
        print('#sumDca4,%s,%s,%d,%d'%(args.cellName,sumD['bbpName'],len(xx),sumD['num_samp']))
    else:
        sumD['legTitle']='multi-frame DCA'

    plot=Plotter_PredictInfoM(args)
    plot.dcaScore(sumD)


    if args.cellName!=None:  # 1frame  plot per range of phys-par0

        sumD2=prep1Framelot(sumD,ip=0) # index of phys param to sort the data
        plot.dcaScore(sumD2)
    
    plot.display_all('2021Feb11',png=0)
