#!/usr/bin/env python
""" 
 minimal code reading per-fram dca yaml summary for 1 cell
"""

from pprint import pprint
import ruamel.yaml  as yaml
import time

#...!...!..................
def read_yaml(ymlFn,verb=1):
        if verb: print('  read  yaml:',ymlFn,end='')
        start = time.time()
        ymlFd = open(ymlFn, 'r')
        bulk=yaml.load( ymlFd, Loader=yaml.CLoader)

        ymlFd.close()
        #  
        #print('\nee2',type(bulk),'size=%d'%len(bulk))
        #print(bulk)
        if verb: print(' done  elaT=%.1f sec'%(time.time() - start))
        return bulk

#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__=="__main__":

    cellN='bbp041'    
    inpPath='/global/cscratch1/sd/balewski/neuron-DCA_one_2020_sum/'+cellN+'/'
    inpF='pi1frame_%s.yaml'%cellN

    blob=read_yaml(inpPath+inpF)
    print('inpF:',inpF,'keys:',sorted(blob))
    print('cell=',cellN,' samples=',blob['num_samp'])
    pprint(blob[cellN+'_phys_range'])
    #pprint(blob)
