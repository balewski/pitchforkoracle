#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

echo 'D:DCA-driver start on '`hostname`' '`date`
taskList=$1
numFrame=$2
cellName=$3
shardId=$4

procIdx=${SLURM_PROCID} 
arrIdx=${SLURM_ARRAY_TASK_ID}
nprocspn=${SLURM_NTASKS_PER_NODE}
taskIdx=$procIdx

echo D:my procIdx=$procIdx "pwd: "`pwd` arrIdx=$arrIdx
echo D:RANK_`hostname` $SLURM_PROCID arrIdx=$arrIdx nprocspn=$nprocspn cellName=$cellName
#taskIdx=$[ $nprocspn * $arrIdx + $procIdx ]

echo D:askIdx=$taskIdx 

# this is for handling task-list
maxTask=`cat $taskList |wc -l`
if [ $arrIdx -ge $maxTask ]; then
    echo "D:arrIdx  $arrIdx above maxTask=$maxTask, idle ..."; exit 0
fi
line=`head -n $[ $arrIdx +1 ] $taskList |tail -n 1`
myName='dca42'

echo D:myName=${myName}  dcaArgs=$line
outDir=out/${taskIdx}_$myName
outDir=out/

logN=${taskIdx}_${myName}

mkdir -p $outDir
pwd

if [ $SLURM_LOCALID -eq 0 ] ; then
    ( sleep 80; echo; echo "M:TTTTTTTTT_3m";  date; hostname; free -g; top ibn1)&
fi


skipFrame=$taskIdx
# stagger starts on each node to ease the cold-start IO
sleep $(( 1 + ${SLURM_LOCALID}*2 ))
echo D:start DCA ${taskIdx} at $outDir=$outDir skipFrame=$skipFrame

python -u ./rd40kHz_2_Dca8kHzB.py  --numFrame $numFrame $line --cellName $cellName --outPath $outDir  --skipFrame $skipFrame --shardId $shardId --jobId ${SLURM_ARRAY_JOB_ID}/${SLURM_ARRAY_TASK_ID}/${procIdx} >&log.$logN

echo D:DCA-done-`date`

