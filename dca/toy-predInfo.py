#!/usr/bin/env python3
import sys,os
'''
Jesse: should behave similarly to a scikit-learn model (fit, transform, model.coef_).

See details at:
https://github.com/BouchardLab/DynamicalComponentsAnalysis/blob/master/dca/dca.py

Remember, always start with:

module load tensorflow/gpu-2.2.0-py37 
export HDF5_USE_FILE_LOCKING=FALSE
export PATH=/global/homes/b/balewski/.local/cori/gpu-tensorflow2.2.0-py37/bin:$PATH

Answer:
./toy-predInfo.py 
generate: nFrames=500,nTbin=8000,nFeat=1
X (500, 8000, 1)
predInf= 0.0049726531013846165 delT=18.50 sec  T=200

'''

import numpy as np
import time
from dca.cov_util import calc_pi_from_data

def main():

    nFeat=1
    nFrames=500
    nBin=8000
    print('generate: nFrames=%d,nTbin=%d,nFeat=%d'%(nFrames,nBin,nFeat))
    X = np.random.randn(nFrames,nBin,nFeat) 
    print('X',X.shape)
    
    T=200

    # Note that the argument in calc_pi_from_data should be 2 * T if T is the DCA parameter you are using.
    T0 = time.time()
    prInf=calc_pi_from_data(X, 2*T)
    delT=(time.time()-T0)

    print('predInf=',prInf,'delT=%.2f sec  T=%d'%(delT,T))
    
    
main()

