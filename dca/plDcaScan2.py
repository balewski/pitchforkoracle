#!/usr/bin/env python
""" 
 make plot from per-step data stored as CSV file
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np
import  time
import csv
from pprint import pprint

import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("-t", "--csvName",help="input per-step data recored during the training", default='bbp153-explore4.csv')


    parser.add_argument("-o", "--outPath",
                        default='out/',help="output path for plots and tables")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    args = parser.parse_args()
    args.prjName='plSpeed'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args



#...!...!..................
def read_one_csv(fname,delim=','):
    print('read_one_csv:',fname)
    tabL=[]
    with open(fname) as csvfile:
        drd = csv.DictReader(csvfile, delimiter=delim)
        print('see %d columns'%len(drd.fieldnames),drd.fieldnames)
        for row in drd:
            tabL.append(row)

        print('got %d rows \n'%(len(tabL)))
    #print('LAST:',row)
    return tabL,drd.fieldnames

#...!...!..................
def myPlot1(yDD,dL,wL,tit='',figId=10):
    nrow,ncol=1,1
    #  grid is (yN,xN) - y=0 is at the top,  so dumm
    plt.figure(figId,facecolor='white', figsize=(6,9))

    colT={20:'brown',50:'black',100:'fuchsia',150:'red'}
    symT={20:'v',50:'o',100:'D',150:'s'}
    msT={20:10,50:10,100:8,150:9}
        
    ax=plt.subplot(nrow,ncol,1)

    for winT in wL:
        xV=[]
        yV=[]
        for x in dL:
            if x  not in yDD[winT]: continue
            xV.append(x)
            yV.append(yDD[winT][x])                           
        ax.plot(xV, yV, alpha=0.7, label='window T=%d'%winT,color=colT[winT],marker=symT[winT], markersize=msT[winT])

    # aux experiments
    dataL=point_exp()
    for data in dataL:
        winT=data['T']
        x=data['d']
        yV=data['score']
        xV=[x]*len(yV) #
        ax.plot(xV, yV, label='T=%d, vary data'%winT, alpha=0.5,color=colT[winT],marker=symT[winT], markersize=6)
        
        ax.grid(True)
        ax.set(xlabel='outProbes( aka d)',ylabel='score (aka pred. info.)',title=tit)
        ax.legend(loc='upper left')

    #ax.set_ylim(0,90)
    plt.tight_layout()

    
def point_exp():
    exp2={'T':50,'d':29.5,'score':[ 56.2,63.0,60.5,62.3,57.8]}
    exp5={'T':100,'d':16,'score':[ 42.3,37.3,52.8,45.8,43.3,40,40.9]}
    exp6={'T':20,'d':14,'score':[78.6,75.5,56.3,78.2,100,86.6,63.6,70.8 ]}
    return [exp6,exp2,exp5]
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
tabL,keyL=read_one_csv(args.csvName,'\t')

xV=[] # steps/k
tV=[] # time (min)
speedS=[] # steps/sec
lossV=[] # train-loss

# see 11 columns ['job_name', 'samples', 'inpProbe', 'outProbe', 'windowT', 'score', 'jid', 'covT_min', 'fitT_min', 'totT_min', 'totT_h']
#got 20 rows 

row0=tabL[0]
constD={ x:row0[x] for x in ['samples', 'inpProbe']}
print('constD:',constD)

dL=[3,5,10,15,20,25,30] # aka outProbes
wL=[20,50,100,150] # aka T-window
yDD={ x:{} for x in wL}

for row in tabL:
    #print('rr',row)
    for x in constD: assert row[x]==constD[x]
    score=float(row['score'])
    d=int(row['outProbe'])
    w=int(row['windowT'])
    yDD[w][d]=score

pprint(yDD)
myPlot1(yDD,dL,wL,tit=args.csvName, figId=11)



plt.show()
