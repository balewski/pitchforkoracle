#!/usr/bin/env python3
'''
This code computes predictive info for 1 probe


Paper: 
https://papers.nips.cc/paper/9574-unsupervised-discovery-of-temporal-structure-in-noisy-data-with-dynamical-components-analysis.pdf

https://github.com/BouchardLab/DynamicalComponentsAnalysis
Jesse: should behave similarly to a scikit-learn model (fit, transform, model.coef_).

module load tensorflow/gpu-2.1.0-py37
export HDF5_USE_FILE_LOCKING=FALSE 
export PATH=/global/homes/b/balewski/.local/cori/gpu-tensorflow2.1.0-py37/bin:$PATH

Quick test: 
  ./rdFrames2PredInfo.py  --numShard 3 --windowLength 50

'''

import numpy as np
import time
from dca.cov_util import calc_pi_from_data
import sys,os
sys.path.append(os.path.abspath("../"))
from Util_IOfunc import write_yaml, read_yaml, write_data_hdf5, read_data_hdf5,build_name_hash
from Plotter_Backbone import Plotter_Backbone

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")
    parser.add_argument("--numShard", default=3, type=int, help="number of data shards to be used")

    parser.add_argument("--shardZero", default=0, type=int, help="0-th shard")
    parser.add_argument('-nf',"--numFrame", default=500, type=int, help="number of frames, or all is -1")
    parser.add_argument("--probeIdx", default=0, type=int, help="number of input probes")
    parser.add_argument('-T',"--maxTLength", default=300, type=int, help="max T-window, in bins, after rebin")
    
    parser.add_argument("-o","--outPath",
                        default='out/',help="output path for plots and tables")

    parser.add_argument("-N", "--cellName", type=str, default='bbp153', help="cell shortName")

    args = parser.parse_args()
    args.dataPath='/global/cfs/cdirs/m2043/balewski/neuronBBP-pack8kHz/probe_67pr8kHz/'
    args.timeBin_msec=0.025*5
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
#............................
#............................
class Plotter_PredictInfo(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)
        self.timeBin_msec=args.timeBin_msec
        
#...!...!..................
    def VoltsSoma(self,F1,figId=100, stim=[], titL=['','']):
        tit1,tit2=titL
        nrow,ncol=4,2
        figId=self.smart_append(figId)
        # Remove horizontal space between axes
        fig,axA = self.plt.subplots(nrow, ncol, sharex=True, sharey=True,num=figId,facecolor='white', figsize=(16,8))

        #print('eee',F1.shape)
        mxFr=F1.shape[0]
        nFr=nrow*ncol        
        frL=[ i for i in range(nFr)]
        #print('prL',prL)

        nBin=F1.shape[1]
        maxX=nBin*self.timeBin_msec
        xLab='time(ms),  [1ms = %d tBins]'%int(1./self.timeBin_msec)
        yLab='ampl (mV)'
        binsX=np.linspace(0,maxX,nBin)

        for ifr in range(nFr):
            frId=frL[ifr]
            ax=axA[ifr%nrow,ifr//nrow]
            V1=F1[frId]
            ax.plot(binsX,V1,label='fr%d'%(frId),c='b',linewidth=0.7)
            if len(stim)>0:
                ax.plot(binsX,stim*10, label='stim',color='black', linestyle='--',linewidth=0.5)
            ax.grid(linestyle=':')
            ax.legend(loc='upper right', title='probe  idx:name')
            if ifr//nrow==0: ax.set_ylabel(yLab)
            if ifr%nrow==nrow-1: ax.set_xlabel(xLab)
            if ifr==3:
                ax.set_title(tit1)
                #ax.set_xlim(20,36)
            if ifr==0:
                ax.set_title(args.prjName)
                
#...!...!..................
    def predInfo(self,dataLL,xbinT,tit='',figId=5):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(6,4))
        ax = self.plt.subplot(1,1,1)

        data=np.array(dataLL)
        nT=data.shape[1]
        xbinA=np.array(xbinT)*self.timeBin_msec
        print('xbin T',xbinT,self.timeBin_msec)
        print('xbin A',xbinA,)
        yA=[]; eA=[]
        for i in range(nT):
            yA.append(data[:,i].mean())
            eA.append(data[:,i].std())

        ax.errorbar(xbinA, yA, yerr=eA, fmt='-o',linewidth=1.5,markersize=3.)
        # add box-plot : labels=xlab,vert=False ,labels=binsX[:-1]
        #ax.boxplot(val2D, showfliers=False,whis=(1,99)) # last is no-outliers

        xLab='window size T (ms)   [1ms = %d tBins]'%int(1./self.timeBin_msec)
        ax.set(title=tit,xlabel=xLab,ylabel='pred.info.')
        ax.grid()
        #ax.set_xscale('log')
        return xbinA, yA, eA

#...!...!..................
def saveSummary(args,resLL,TL,runTime,metaD):
    argL=list(args.__dict__.keys())
    for k in ['verb', 'noXterm', 'prjName','maxTLength']: argL.remove(k)
    print(argL)
    
    sumRec={ k:getattr(args,k) for k in argL}
    sumRec['predInfo2D']=resLL
    sumRec['T1D']=TL
    sumRec['runTime']=runTime
    #print(sumRec)
    
    sumRec.update(metaD)
    outF=sumRec['short_name']+'.sum.yaml'
    write_yaml(sumRec, args.outPath+'/'+outF)


def XXXrebin_data(X,nReb):
        nS,tBin,nF=X.shape
        print('X1',X.shape,nReb)
        assert tBin==8000 # tested only for original data
        assert tBin%nReb==0
        a=X.reshape(nS,tBin//nReb,nReb,nF)
        b=np.sum(a,axis=2)/nReb
        print('X2',a.shape,b.shape,b.dtype)
        return b.astype('int16')

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
nameSuf='%dprobe'%args.probeIdx
sumD=build_name_hash('cellPredInfo_8kHz_%s'%args.cellName,nameSuf)

args.prjName=sumD['short_name']
plot=Plotter_PredictInfo(args)

shortN=args.cellName
pidx= args.probeIdx
startT0 = time.time()

Ttempl=[1,2,5,10,20,30,40,50,60,70,100,120,150,200,250]  # unit is bins
TL=[]
for T in Ttempl:
    if T>args.maxTLength : break
    TL.append(T)
    
resLL=[]

inpF='%s/meta.cellSpike_67pr8kHz.yaml'%(shortN)
metaD=read_yaml(args.dataPath+inpF)
probeL=metaD['rawInfo']['probeName']
print('use probeIdx=%d  name=%s'%(pidx,probeL[pidx]))
sumD['probe_name']=probeL[pidx]


#print('ee',probeL)
#print('qq',probeL.index('axon_11'))
for si in range(args.numShard):
    shardId=si+args.shardZero
    inpF='%s/%s.cellSpike_67pr8kHz.data_%d.h5'%(shortN,shortN,shardId)
    inpD=read_data_hdf5(args.dataPath+inpF,verb=(si==0))
    assert args.numFrame<= inpD['frames'].shape[0]
    assert pidx<= inpD['frames'].shape[2]
    
    X=inpD['frames'][:args.numFrame,:,pidx:pidx+1]
    X=X/150. # now unist are volts
    print('X',X.shape,'maxT=',args.maxTLength)

        
    if 0:  # special case, QA of volts
        titL=['probIdx=%d'%pidx,'']
        stimD=read_yaml(args.dataPath+'/'+shortN+'/stim.chaotic_2_8kHz.yaml')
        stim=stimD['stimFunc']
        
        plot.VoltsSoma(X,titL=titL,stim=stim)
        plot.display_all(shortN)
    
    rec=[]
    T0 = time.time()
    for T in TL:
        prInf=calc_pi_from_data(X, 2*T)
        rec.append(float(prInf))
        delT=(time.time()-T0)
        print(si,'predInf=',prInf,'delT=%.2f sec  T=%d'%(delT,T))
    resLL.append(rec)

delT1=time.time()-startT0
print('elaT=%.1f min'%(delT1/60.))
tit='cell=%s  probeIdx=%d:%s  nFrame=%d'%(shortN,pidx,probeL[pidx],args.numFrame)
tit+='   nShard=%d'%args.numShard
xbinA, yA, eA=plot.predInfo(resLL,TL,tit)
sumD['avr pred info']=np.array([xbinA, yA, eA])
saveSummary(args,resLL,TL,delT1,sumD)

plot.display_all(shortN)


