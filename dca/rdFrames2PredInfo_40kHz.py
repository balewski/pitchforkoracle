#!/usr/bin/env python3
import sys,os
'''
This code computes predictive info for 1 probe
- rebins tBin 8000->1600
- optional data distortions ( --mode):
   * white noise
   * add bias
   * scaling
   * chop tBins on quaters 


Paper: 
https://papers.nips.cc/paper/9574-unsupervised-discovery-of-temporal-structure-in-noisy-data-with-dynamical-components-analysis.pdf

https://github.com/BouchardLab/DynamicalComponentsAnalysis
Jesse: should behave similarly to a scikit-learn model (fit, transform, model.coef_).

module load tensorflow/gpu-2.1.0-py37
export HDF5_USE_FILE_LOCKING=FALSE 
export PATH=/global/homes/b/balewski/.local/cori/gpu-tensorflow2.1.0-py37/bin:$PATH

Quick test: 
  ./rdFrames2PredInfo.py  --numShard 3 --windowLength 50

'''

import numpy as np
import time
from dca.cov_util import calc_pi_from_data

sys.path.append(os.path.abspath("/global/homes/b/balewski/pitchforkOracle/"))
from Util_IOfunc import write_yaml, read_yaml, write_data_hdf5, read_data_hdf5,build_name_hash
from Plotter_Backbone import Plotter_Backbone

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
        action='store_true', default=False,help="disable X-term for batch mode")
    parser.add_argument("--numShard", default=3, type=int, help="number of data shards to be used")
    parser.add_argument("--shardZero", default=0, type=int, help="0-th shard")
    parser.add_argument('-nf',"--numFrame", default=500, type=int, help="number of frames, or all is -1")
    parser.add_argument("--probeIdx", default=0, type=int, help="number of input probes")
    parser.add_argument('-T',"--windowLength", default=500, type=int, help="max T-window, in bins, after rebin")
    
    parser.add_argument("--mode",type=str, default=None,help='data modiffier')

    parser.add_argument("-o","--outPath",
                        default='out/',help="output path for plots and tables")

    parser.add_argument("-N", "--cellName", type=str, default='bbp153',
                        help="cell shortName")
    #parser.add_argument("-j","--jobId", default=None, help="optional, aux info to be stored w/ summary")

    args = parser.parse_args()
    args.dataPath='/global/cfs/cdirs/m2043/balewski/neuronBBP-data_67pr/'
    args.rebin_freq=5
    args.timeBin_msec=0.025*args.rebin_freq
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
#............................
#............................
class Plotter_PredictInfo(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)
        self.timeBin_msec=args.timeBin_msec
        
#...!...!..................
    def VoltsSoma(self,F1,figId=100, stim=[], titL=['','']):
        tit1,tit2=titL
        nrow,ncol=4,2
        figId=self.smart_append(figId)
        # Remove horizontal space between axes
        fig,axA = self.plt.subplots(nrow, ncol, sharex=True, sharey=True,num=figId,facecolor='white', figsize=(16,8))

        #print('eee',F1.shape)
        mxFr=F1.shape[0]
        nFr=nrow*ncol        
        frL=[ i for i in range(nFr)]
        #print('prL',prL)

        nBin=F1.shape[1]
        maxX=nBin*self.timeBin_msec
        xLab='time(ms),  [1ms = %d tBins]'%int(1./self.timeBin_msec)
        yLab='ampl (mV)'
        binsX=np.linspace(0,maxX,nBin)

        for ifr in range(nFr):
            frId=frL[ifr]
            ax=axA[ifr%nrow,ifr//nrow]
            V1=F1[frId]
            ax.plot(binsX,V1,label='fr%d'%(frId),c='b',linewidth=0.7)
            if len(stim)>0:
                ax.plot(binsX,stim*10, label='stim',color='black', linestyle='--',linewidth=0.5)
            ax.grid(linestyle=':')
            ax.legend(loc='upper right', title='probe  idx:name')
            if ifr//nrow==0: ax.set_ylabel(yLab)
            if ifr%nrow==nrow-1: ax.set_xlabel(xLab)
            if ifr==3:
                ax.set_title(tit1)
                #ax.set_xlim(20,36)
            if ifr==0:
                ax.set_title(args.prjName)
                
#...!...!..................
    def predInfo(self,dataLL,xbinT,tit='',figId=5):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(6,4))
        ax = self.plt.subplot(1,1,1)

        data=np.array(dataLL)
        nT=data.shape[1]
        xbinA=np.array(xbinT)*self.timeBin_msec
        print('xbin T',xbinT,self.timeBin_msec)
        print('xbin A',xbinA,)
        yA=[]; eA=[]
        for i in range(nT):
            yA.append(data[:,i].mean())
            eA.append(data[:,i].std())

        ax.errorbar(xbinA, yA, yerr=eA, fmt='-o',linewidth=1.5,markersize=3.)
        # add box-plot : labels=xlab,vert=False ,labels=binsX[:-1]
        #ax.boxplot(val2D, showfliers=False,whis=(1,99)) # last is no-outliers

        xLab='window size T (ms)   [1ms = %d tBins]'%int(1./self.timeBin_msec)
        ax.set(title=tit,xlabel=xLab,ylabel='pred.info.')
        ax.grid()
        #ax.set_xscale('log')

#...!...!..................
def saveSummary(args,resLL,TL,runTime,metaD):
    argL=list(args.__dict__.keys())
    for k in ['verb', 'noXterm', 'prjName']: argL.remove(k)
    print(argL)
    
    sumRec={ k:getattr(args,k) for k in argL}
    sumRec['predInfo2D']=resLL
    sumRec['T1D']=TL
    sumRec['runTime']=runTime
    #print(sumRec)
    
    sumRec.update(metaD)
    outF=sumRec['short_name']+'.sum.yaml'
    write_yaml(sumRec, args.outPath+'/'+outF)

def rebin_data(X,nReb):
        nS,tBin,nF=X.shape
        print('X1',X.shape,nReb)
        assert tBin==8000 # tested only for original data
        assert tBin%nReb==0
        a=X.reshape(nS,tBin//nReb,nReb,nF)
        b=np.sum(a,axis=2)/nReb
        print('X2',a.shape,b.shape,b.dtype)
        return b.astype('int16')
    
#...!...!..................
def modify_data(X,mode):
    if 'white' in mode:
        ampl=int(mode.split('_')[1])
        return X+np.random.uniform(ampl,size=X.shape) 
    
    if 'bias' in mode:
        val=float(mode.split('_')[1])
        return X+val
    
    if 'scale' in mode:
        val=float(mode.split('_')[1])
        return X*val

    if 'chop' in mode:
        nchop=4
        ichop=int(mode.split('_')[1])
        nS,tBin,nF=X.shape
        assert ichop<nchop
        tBin1=tBin//nchop
        i0=ichop*tBin1
        print('cc',i0,tBin1,X.shape)
        return X[:, i0:i0+tBin1,:]
    assert mode==None # not implemented option

def read_stimulus(path1):
    stimD=read_yaml(path1+'/stim.chaotic_2.yaml')
    print(stimD.keys())
    stim=stimD['stimFunc']
    S=stim.reshape(1,-1,1)
    S=rebin_data(S,args.rebin_freq)
    S=S.reshape(-1)
    #print('SS',S.shape)
    return S

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
sumD=build_name_hash('cellPredInfo',args.mode )
args.prjName=sumD['short_name']
plot=Plotter_PredictInfo(args)

shortN=args.cellName
pidx= args.probeIdx
startT0 = time.time()

Ttempl=[1,2,5,10,20,30,40,50,60,70,100,120,150,200,250]
TL=[]
for T in Ttempl:
    if T>args.windowLength : break
    TL.append(T)
    
resLL=[]

inpF='%s/meta.cellSpike_orig.yaml'%(shortN)
metaD=read_yaml(args.dataPath+inpF)
probeL=metaD['rawInfo']['probeName']
print('use probeIdx=%d  name=%s'%(pidx,probeL[pidx]))
sumD['probe_name']=probeL[pidx]


#print('ee',probeL)
#print('qq',probeL.index('axon_11'))
for si in range(args.numShard):
    shardId=si+args.shardZero
    inpF='%s/%s.cellSpike.data_%d.h5'%(shortN,shortN,shardId)
    inpD=read_data_hdf5(args.dataPath+inpF,verb=(si==0))
    assert args.numFrame<= inpD['frames'].shape[0]
    assert pidx<= inpD['frames'].shape[2]
    
    X=inpD['frames'][:args.numFrame,:,pidx:pidx+1]
    X=X/150. # now unist are volts
    print('X',X.shape,'maxT=',args.windowLength)

    X=rebin_data(X,args.rebin_freq)
    if args.mode!=None:
        X=modify_data(X,args.mode)
        
    if 0:
        titL=['probIdx=%d'%pidx,'']; stim=None
        #stim=read_stimulus(args.dataPath+'/'+shortN)
        
        plot.VoltsSoma(X,titL=titL,stim=stim)
        plot.display_all(shortN)
    
    rec=[]
    T0 = time.time()
    for T in TL:
        prInf=calc_pi_from_data(X, 2*T)
        rec.append(float(prInf))
        delT=(time.time()-T0)
        print(si,'predInf=',prInf,'delT=%.2f sec  T=%d'%(delT,T))
    resLL.append(rec)

delT1=time.time()-startT0
print('elaT=%.1f min'%(delT1/60.))
saveSummary(args,resLL,TL,delT1,sumD)

tit='cell=%s  probeIdx=%d:%s  nFrame=%d'%(shortN,pidx,probeL[pidx],args.numFrame)
#tit+='   nShard=%d'%args.numShard
tit+='  mode=%s '%args.mode
plot.predInfo(resLL,TL,tit)
plot.display_all(shortN)


